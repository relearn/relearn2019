# Relearn 2019

This summer, Relearn is back in the form of a curve, transversing multiple times and spaces. 
Curl yourselves this summer from, to and between Rotterdam, Brussels and Paris ! Read more about the curve.

Relearn 2019 is a curve, transversing multiple times and spaces.

These would be independently organized sessions based on the urgencies and affordances of each of the participating spaces. 
However, there would still be a common thread throughout the series, perhaps in the form of a transversal “reroam” 
but also in the form of a roaming server that is passed on to each subsequent event. 

It is possible to attend all sessions but not required, nor the goal, it is really the intention to have 
self-standing events which do not depend people joining a succession of events but still share commonalities.

<http://relearn.be/2019>

## Accessing the files 
This folder contains a selection of the files from the re-roaming server after three sessions in Rotterdam (7-9 June), Brussels (21-22 June) and Paris (6-8 September).

/var/www/

/home/ (hidden files are removed as they might contain sensitive information)

/etc/ (only the files that were changed by the group)

Pictures from Varia and Brussels can be found at /var/www/html/pictures
Pictures from Paris can be found at /var/www/html/files/pictures/ and /var/www/html/files/pictures-manetta-cristina/

The Etherdump has been running from /var/www/html/etherdump (files are in the folder "publish")

But there is a lot more to be found!

## Accessing the history
This repo contains a snapshot of the server after the session at Varia in Rotterdam (tagged as `varia`) and a snapshot after the sessions at OSP in Brussels and at La Generale in Paris (tagged as `bxl-paris`). 
You can use git history commands such as `git log`, `git diff varia..bxl-paris` to see the differences between files. You can retrieve the full content of the server after the session at Varia with `git checkout varia`. You can return to the latest version with `git checkout master`.
