Damage reporting… or not.
Relearn  Rotterdam curve 2019


- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

__PUBLISH__ 

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

In this table, some materials propose the merging of particular methodological tactics plus attitudes that have been placed in practice collectively along a plastic and elastic "we" during the past months (see references below). This bunch of sensibilities have to do with the experiences of damage, failure, brokenness, backwardness and misfitting and the material-semiotic implications that actively reporting [publishing] them have or could have in our mundane technoecologies. 

Somehow, this too-fast practice at Relearn 2019 constitutes an invitation to gather two methods together.

Method number one is “Bug Reporting”, and it is connected to how the hegemonic acceleration of contemporary technologies imposes a series of conditions that lead to the persistence of cultural forms of totalitarian innovation towards progess and agility which must  sometimes be resisted and contested. Yet those same conditions also constitute a complex of latencies [things not expressed] and absences with which we must affirmatively coexist, driven by the need for attentive, politicised presences. In a way, the persistent practice of finding “bugs” (is this yet another way to conduct research [research as in finding mistakes]?) tracks the potential to, as Donna Haraway proposes, “stay with the trouble” [nice] in a responsible, creative way. In other words, writing disobedient, collective, inventive “bug reports” can be seen as a method of pushing the probable and expanding the possible[!] [stretch and make space for other use and voices]. Discussing technological sovereignty and infrastructural self-defence initiatives is a good place to start… but maybe not enough. The first step is to systematically identify and affirmatively publish the damages that turbo-capitalism [!] renders on a daily immanent basis; it is urgent that we join forces and write “bug reports” on the system, in order to technically equip ourselves with partial and localised repair possibilities. 

[reports can be written after technical inspection, but reporting can also happen after user inspection] [where to report the bugs?]
[how to stimulate the act of reporting? not-reporting has been accepted too much]
[publishing bugs, in public? as a publishing format? to the agency that is responsible for the bug only?]
[some bugs need face to face contact to be filed, bodily contact while complaining is important]

Method numer two is called “Queering Damage” and uses analytics otherwise, to activate a read-write attitude in relation to specific experiences of damage that might be partially and situatedly reparated. “Queering Damage” invites us to wonder together about the potential of microbial, animal, plant, mineral, cosmological technoscience, to situate ourselves in these mundane and alluring scenes, to consider these labours and imagine a collective life otherwise. It asks us to ponder the possibilities and limitations of informatics and to take seriously the affective forces of nonhuman animals and machines. How might we extend queer theories that concern personal injury into more-than-human assemblages, in order to consider the damages shared by humans and nonhumans? How can we generate ways that take us beyond reparative narratives or benevolent utopianism towards more-than-human, rough and mundane life?

[Queering? Queering as in post-human extensions of our bodies.] [Us as ecosystems, not individuals]
[Queering damage as a different way of perceiving a bug situation. Complexifying the situation, looking at it as a ecosystem]
[Human as a word with strong ties to rational thinking. More-than-human as a way to also see us as emotional beings]
[Modifying the body is often still seen as a way to optimize our own systems and productivity. The idea of "mastering" ourselve, which holds connections to colonialist thinking] [This vs. making our collective life better/nicer for a collective being.]

[Quote, definition of queer by Neel Ahuja: "To recognize that life is ambiently queer is to divest from spectacular temporalities of crisis and transcendence that infuse queer theory and environmentalism alike"]

[What is a good complaint] [What are the concequences you would like to happen?]
[Attachment to a complaint, responsibility of a complain, anonymous/non-anonymous]
[Where to go with your complaint? What are the interfaces of complaining in an institution?]

[Is there an interface for complaints in Relearn? How to handle the structurelessness?]
[This is closely related to the code of conduct questions]

operation 0: Attunement
Take 1 min to attune to the immediate technical environment, find the courage, identify allies based on shared urgencies on it.

operation 1: Identify damage
Take 5 min to widen the notion of damage and identify its conditions and surroundings. Damages don't have to be personally experienced injuries: they can be observed damages or collective ones. Felt or remembered. They can be bodily damage, community damage, environmental damage, personal injury, historical damage, public damage, fictitious damage, cultural damage, file damage, structural damage, naturalized damage, computer damage, urban damage, transgenerational damage, geopolitical damage... etc. Just consider different forms of damage: pain, suffering, injury, uselessness, unrevoerability, failure, brokenness, backwardness, unreparability, homophobia, racism, ageism, ableims, specism, classism, exclusions, inclusions, path dependences…

Take 2 min to consider your extended queering attitude: non-identitary, non-essentialist, non-fixed lives, trans*feminist approaches, interesectional (what is interesected?), not-only-human, antinormative, affirmative… or other geometries of relation.

Now take 5 min to check possible (partial) reparations: perhaps there are partial hopes to repair some damages but not others, or needs to live with damage in ways that give comfort and care.  Or an decision to reject damage as a vector because it briengs with too heavy forces of revictimisation. Situatedly considering the intensity of reparation is a basic gesture of trying to organize a togetherness that counts and cares.

Analytics (discovery, interpretation, and communication of meaningful patterns in data): a post-positivist move. It is urgent to reclaim analytics in non-profit experiments, not based in optimisation nor efficiency but in the political urgency for read-write practices that don’t let go of contemporary complexity. Reclaim analytics as the practice of cutting complexity together apart!

operation 2: Reporting the damage
(use the templates on the table for this and next operation)
5 min

• What is going on?
• Which are the agents implied (alive or not, human or not, powerful or powerless)?
• Wheres (spatiality / situatedness / displacements / distribution)?
• Whens (temporality / durability / existing / extinct / repeated)?
• Semiotic-materialities: what signs and matters are at work in this assemblage?
• What is your particular/specific entanglement with the scene?
• Can a pattern be identified here? To what extent is this damage structural, or singular?

operation 3: Analytic transcript on the B-side of the report (this is an absolute experiment!)
openended duration

1. Define your field of interest. Cut apart and together an area of urgency. Set the scene, through naming the fields
ᘒ ፨ ᚼ ⚘ ◊ ᗶ

2. Declare your AE, Entangled Agents (myth, technology, ...).
Æ = pH+pNH+pIH+pF
^ + Æ (DAMAGE)

3. Divide by the axioms of αe (ethics) and αK (Kapital): [dividing as a
second cutting? Is this also a cutting apart together?]
αe
[formula]
αK

4. Where/when: Define the spacetime conditioning: (ephemeral,
eternal, cyclic, linear, continuous, eventual, historical present, memory,
extincted, dreamed, extended, distributed, dissoluted, dispersed,
concentrated, catalized, centrifugal, centripetal, located, situated,
regional, local, global, reachable, micro, meso, macro...)
ʃ T

5. Talking about relations. Analyze and notate the Æ through a
consideration of its Variable Relations (Ω repair, Δ damage, a affects,
E effects, © creative force, ≠ difference)
∫ = ^ + ∫∫ / (xΩ , x Δ , xa, x∃, x©, x≠)

6. Apply at least one Generative Operation for your analysis, to see
what it would do. Queering operations. These are tricky, they require
you to take responsibility. Define their scale:
>>+i+*+#+"+♥+ ↺ + ♙ + ♞ + ↺ + ✵ + ⚕

7. declare your QÆ = qH+qNH+qIH+qF+qd, declare the
reconfigured entanglement:
(tΩ , t Δ , ta, t∃, t©, t≠)

8. Divide by the axioms of αe (ethics) and αK (Kapital)
αe
[the rest of the formula goes here]
αK

9. Name the other end of the equation:
QÆ (QUEERING DAMAGE OR N)



- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

References:

Queering Damage, Biennal de Pensament, Barcelona, October 2018
https://queeringdamage.hangar.org

Alchorisma – Hasselt, December 2018
http://constantvzw.org/site/-Alchorisma,217-.html 

Bug Reporting Workshop, Festival Gelatina, Madrid, May 2019
https://www.lacasaencendida.es/en/talks/bug-reporting-workshop-jara-rocha-10073 

The Bitflipping Mixtape (forthcoming)
http://constantvzw.org/site/The-Bitflipping-Mixtape-launch.html 

Contact, to keep wondering together… or not: jara@riseup.net



------------------------------------------------------------------------------------------------------



Damage Report
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

• Name the damage:




•  What is going on? (brief description)




• Which are the agents implied (alive or not, human or not, powerful or powerless)? (cut a natureculture)



• Wheres (spatiality / situatedness / displacements / distribution)?



• Whens (temporality / durability / existing / extinct / repeated)?



• Semiotic-materialities: what signs and matters are at work in this assemblage?



• What is your particular/specific entanglement with the scene?



• Can a pattern be identified here? To what extent is this damage structural, or singular?








Analytics
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
Try these elements (to be used, changed, cut and extended) for a read-write of reported damages, on a blank page

indicators:

H – Human
NH – Non human
IH – Inhuman
F – Fantasies/Imaginaries

generative operations (potentials for queering; modes of intervention):

>> remediate
^ scale timespace to n
i integrate
* multiply
# speculate
" renarrate
♥ love
↺ protest
♙ resist
♞ reclaim
↻ invent
✵ flip
⚕ celebratevariable 

relations (a declaration of the relations in your scene of damage that you
are you attending to):

Ω repair
Δ damage
a affects
∃ effects
© creative force
≠ difference

axioms:

αe axiom of ethics (survival, caretaking, respect, responsability)
αK Kapital (accumulation, acquisition, growth)

fields:

ᘒ On the experience and process of damage and pain, partial
reparation, loss/disposession
፨ On queer politics and methodologies, contestation, affirmation
ᚼ On contesting anthronormativities
⚘ On bioeconomies, lively capital, accumulation
◊ On computation, analytics, bioinfotech
ᗶ On political fictions, poethics, figurations &/or wild fantasies,
opportunities

