= why (1)
Relearn Roaming Communities
:release-version: 1.0.0
:doctype: manpage
:manmanual: Relearn Roaming Server Why
:mansource: Relearn Roaming Server Why {release-version}


== NAME

why - the short story about why we propose to relearn the shell environment.

== FEMINIZING THE SHELL ENVIRONMENT

Welcome to the Relearn Roaming Server! 
Strongly inspired by the Feminist Server Manifesto, some members of the Relearn community met and discussed how this could practically manifest when interacting with the server.
Some of the questions raised during this discussion inquired into the possibilities of:
    
* Leveling the playing field of participation, either by providing alternatives for connecting with the server (other than the shell) or by fostering user comfort with trial and error through the display of encouraging messages when mistakes occur;

* Acknowledging care and maintenance work (often rendered invisible), either by directly thanking the user performing these functions or by keeping an acknowledgment log of the caretakers;

* Introducing the voice of the community behind the server, e.g. by rethinking the form of the manual file, often terse and dry, and allowing it to be rewritten to better reflect the ideals and goals of this community; 

* Rethinking user self-representation by, among others, allowing them to choose their preferred pronouns when a new user account is created;

* Facilitating server exploration by introducing ways to playfully become acquainted with the file system and the inner workings of the server, while avoiding confusion between accessibility and obfuscation/abstraction;

* Questioning persistent availability and re-introducing materiality by, for example, allowing the server to have downtime when overheated and properly signaling it to the users;

* Recontextualizing the server within its surroundings, either by mapping the wider network of dependencies or by emphasizing the "roaming" in roaming server in a diary file: Where did the server come from? Where will it go? Whom did he/she/they meet? How did that change her/him/them?

While all of these and more ideas didn't have enough time to be implemented in their entirety during the Relearn session in Rotterdam, they will hopefully inspire other creative explorations around what it means to access, maintan and care for a Feminist Server. 

By seemingly providing the server with a more active voice, the goal is, rather than anthropomorphizing the server, having this voice be a living documentation of the community around it, changing and evolving accordingly along the curve.


 // __PUBLISH__
