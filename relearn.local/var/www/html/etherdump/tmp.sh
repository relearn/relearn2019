/usr/local/bin/etherdump index input \
        publish/*.meta.json \
        --order lastedited \
        --templatepath templates \
        --title "Relearn's roaming etherdump" \
        --output index.html
