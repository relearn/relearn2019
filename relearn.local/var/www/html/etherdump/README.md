# etherdump @ Relearn's roaming server

Hosted at <http://relearn.local/etherdump>. 

This repository contains the files that we use for our [Etherdump](https://gitlab.constantvzw.org/aa/etherdump) installation.  

This installation of etherdump is running on the [publish-vs-nopublish branch](https://gitlab.constantvzw.org/decentral1se/etherdump). It list only includes pads that are tagged with the magic word `__PUBLISH__` (which is an *opt-in* keyword).

Etherdump (currently) lists pads that include the `__PUBLISH__` tag as *html*, *plain text* and *json* documents. 



