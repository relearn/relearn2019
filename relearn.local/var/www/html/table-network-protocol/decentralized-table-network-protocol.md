__PUBLISH__
__SYMLINK__ /var/www/html/table-network-protocol/decentralized-table-network-protocol.md
__LINK__ http://relearn.local/table-network-protocol/
__EXECUTED__ Friday evening during the Relearn @ Varia weekend


# Decentralized Table Network Protocol (RFC*)

<small>\* *Request For Comments*</small>

**Subject**: Decentralized Table Network Protocol (DTNP) to be used during the Rotterdam session of Relearn 2019

**Date**: June 2019

**Author**: Varia, info@varia.zone

This protocol is a rotation system protocol, written for the opening evening of Relearn 2019 in Varia. It facilitates a set of engaged group conversations that run parallel to each other. It thereby circumvents the need of a plenary one-speaking-to-all-the-others situation. With this protocol, we want to trigger a decentralized multiplex of anecdotes, energies and questions, while getting to know the complex constellation of *others* that are present in the space. 

To execute this version of the protocol, the group needs to be split into 6 smaller groups. Five tables are hosting 5 different group conversations in relation to *digital networks* and/or *publishing formats*. Each round is 20 minutes and the end of a round is marked by the sound of a yet-to-be-specified instrument.

Tables: 

⏿   Relearn's roaming server<br>
✍  publishing formats<br>
▩   other geometries<br>
Δ   queering damage bug report<br>
🗯 networks are people too<br>

|Round|Group|Table|
|-----|-----|-----|
|1    |1    |🗯 |
|     |2    |✍   |
|     |3    |▩    |
|     |4    |Δ    |
|     |5    |⏿    |
|     |6    |break|

|Round|Group|Table|
|-----|-----|-----|
|2    |1    |✍    |
|     |2    |▩    |
|     |3    |Δ    |
|     |4    |⏿    |
|     |5    |break|
|     |6    |🗯 |

|Round|Group|Table|
|-----|-----|-----|
|3    |1    |▩    |
|     |2    |Δ   |
|     |3    |⏿    |
|     |4    |break|
|     |5    |⛬    |
|     |6    |✍    |

|Round|Group|Table|
|-----|-----|-----|
|4    |1    |Δ    |
|     |2    |⏿    |
|     |3    |break|
|     |4    |🗯 |
|     |5    |✍    |
|     |6    |▩   |

|Round|Group|Table|
|-----|-----|-----|
|5    |1    |⏿    |
|     |2    |break|
|     |3    |🗯 |
|     |4    |✍    |
|     |5    |▩    |
|     |6    |Δ    |

|Round|Group|Table|
|-----|-----|-----|
|6    |1    |break|
|     |2    |🗯 |
|     |3    |✍    |
|     |4    |▩    |
|     |5    |Δ    |
|     |6    |⏿    |

