function boop {
   ( speaker-test -t sine -f $1 )& pid=$! ; sleep $2s ; kill -9 $pid
}

C=261.6
C1=277.2
D=293.7
D1=311.1
E=329.6
F=349.2
F1=370.0
G=392.0
G1=415.3
A=440.0
A1=466.2
B=493.9
C2=523.2

C22=554.3
D2=587.33
D12=622.2
E2=659.26
F2=698.46
F22=739.99
G2=783.99
G22=830.61
A2=880.00
A22=932.33
B2=987.77
C3=1046.50

boop $G 0.1
boop $A 0.1
boop $B 0.1
boop $C2 0.1
boop $D2 0.1
boop $F 0.1
boop $E 0.1
boop $D 0.1
boop $C 0.1
