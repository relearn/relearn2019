
function boop {
   ( speaker-test -t sine -f $1 )& pid=$! ; sleep $2s ; kill -9 $pid
}

boop $((500 + $RANDOM % 200 * 10)) 0.1
boop $((500 + $RANDOM % 200 * 10)) 0.05
boop $((500 + $RANDOM % 200 * 10)) 0.05
boop $((500 + $RANDOM % 200 * 10)) 0.1
boop $((500 + $RANDOM % 200 * 10)) 0.4
boop $((500 + $RANDOM % 200 * 10)) 0.1
boop $((500 + $RANDOM % 200 * 10)) 0.2
