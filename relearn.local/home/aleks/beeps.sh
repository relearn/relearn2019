
function boop {
   ( speaker-test -t sine -f $1 )& pid=$! ; sleep $2s ; kill -9 $pid
}

boop 1200 0.1
boop 600 0.05
boop 700 0.05
boop 1600 0.1
boop 500 0.4
boop 2200 0.1
boop 1200 0.2

