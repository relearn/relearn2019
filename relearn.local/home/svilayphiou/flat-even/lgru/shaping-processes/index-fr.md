Title: Shaping processes
Lang: fr
Date: 2012
Authors: Alexandre Leray; Stéphanie Vilayphiou

Ce chapitre se focalise sur la manière dont une oeuvre d'art, de design ou logicielle s'adresse au public et invite les gens à s'engager avec elle. Il suggère une manière dont les outils et les objets de design pourrait révéler leur propres mécanismes, leur généalogie, et refléter sur leur propre subjectivité et sur les dynamismes qui les ont fait naitres. Ce faisant, ces travaux offrent un cadre critique pour constament revisiter leur signification et leur fonction.

Dans *Softness*, Matthew Fuller suggère certaines méthodologies pour que les artistes échappent de l'expérience informatique centrée-utilisateur moyenne et se reconnecter avec la matérialité du média informatique. Prenant des exemples de l'art moderne et de l'art comporain des médias, il parle d'*interrogabilité*, une qualité des oeuvres d'art reflétant leur propre manière d'opérer, offrant ainsi une manière de comprendre comment ils sont faits et la possibilité de les faire évoluer.

Dans *Coordinating Collaborations*, Chris kelty explore la manière dont le mouvement du logiciel libre et/ou Open Source est "absolument préoccupé par sa propre existence en tant que public". Faisant l'aller-retour entre planing et improvisation, FLOSS allie critique et la création de modes de travail alternatifs. Les projets Linux et Apaches sont confrontés pour montrer montrer les différentes approches à la prise de décisions, et aux outils dévellopés pour supporter leurs models de collaborations respectifs.

Dans *La politique du language dans le design graphique*, le designer Aitor Méndez imagine ce que pourrait être une pratique ouverte du design graphique. Il se bat pour l'appropriation d'un design  par le public, encourageant les variations basées sur un design initial, laissant de la place pour l'improvisation là où le design à toujours été une question de planification.


* [_Coordinating Collaborations_](coordinating-collaborations.html)
* [_Softness: Interrogability; general intellect; art methodologies in software_](softness-interrogability-general-intellect-art-methodologies-in-software.html)
* [_The politics of Language in Graphic Design_](the-politics-of-language-in-graphic-design.html)
