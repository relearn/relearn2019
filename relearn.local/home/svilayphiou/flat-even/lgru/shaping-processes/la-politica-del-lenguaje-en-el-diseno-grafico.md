Title: La política del lenguaje en el diseño gráfico
Authors: Aitor Méndez
License: CC-BY
Lang: es
Date: 2012
Authors: Alexandre Leray; Stéphanie Vilayphiou


<div markdown=true class="original">
Introducción
============

Esta exposición es un acercamiento al diseño abierto desde una perspectiva poco usual, la del
lenguaje. El diseño abierto es, en gran parte, la extrapolación al ámbito del diseño de los y métodos y
objetivos del software libre y, por tanto, es comprensible que sus propuestas surjan alrededor de las
herramientas que hacen posible la colaboración o la forma en que los resultados del trabajo de diseño
pueden ser compartidos. Sin embargo, las herramientas informáticas para la producción del diseño,
el instrumental necesario para su producción industrial o los dispositivos en red para socializar los
recursos, si bien son tratados recurrentemente, no son los únicos elementos que deberían ser
considerados desde la perspectiva del diseño abierto. El lenguaje es una herramienta fundamental e
ineludible en el trabajo de diseño y sorprende que nadie, hasta donde yo sé, haya abordado esta
cuestión del lenguaje desde el ámbito del diseño abierto.

Pero, ¿cuál es esta cuestión que hay que abordar? ¿de qué tratan el software y la cultura libre? ¿Son la
transparencia, la colaboración y la reutilización fines por sí mismos? En su mayoría, las
aproximaciones y debates sobre diseño abierto parecen responder implícitamente que sí, perdiendo
de vista que transparencia, colaboración y reutilización son meras estrategias para un único fin: la
emancipación del individuo respecto de los distintos poderes que tratan de imponerle sus
condiciones de existencia. Este objetivo podría definirse también como el intento de equilibrar las
fuerzas entre las grandes estructuras de poder y los individuos —devolviendo a estos la posibilidad
de intervenir y de participar de forma efectiva en la organización de su existencia—. Esta cuestión y
no otra es la que debe guiar el espíritu del diseño abierto. Esta reclamación puede parecer obvia pero
es cada vez más necesaria en vista de la multitud de casos de apropiación o, más bien, expropiación
que el mercado ejecuta sobre las estrategias libres y abiertas. Vemos cómo el mercado emplea
estrategias libres persiguiendo sus propios fines, muy alejados del equilibrio e igualdad social
reclamados en los presupuestos iniciales del movimiento por la cultura libre. En otras palabras, el
diseño abierto debe ser considerado siempre en su dimensión política, porque la transparencia, la
colaboración y la liberación de los recursos, son estrategias que por sí mismas no garantizan en
absoluto el equilibrio ni la justicia social.

La dicotomía entre estas dos acepciones del diseño abierto —la que identifica una dimensión
política como su objeto, su razón de ser, y la que se limita a implementar un rango de estrategias que
podrían servir igualmente para favorecer la emancipación del individuo como para lo contrario,
favorecer su subordinación— es, de hecho, la misma que se da en el ámbito del software libre entre
sus dos acepciones “código abierto” y “software libre”. Sería pertinente, por tanto, dirigirse a estos
dos ámbitos como “diseño abierto” y “diseño libre”.


La reificación del sujeto
=========================

La exclusión del individuo se consigue delimitando su campo de actuación de manera que su
intervención en cualquier ámbito sea cada vez menos relevante. Esta pérdida de competencias puede
determinarse por la vía clásica de imposición autoritaria o por la más sofisticada y exitosa fórmula de
producir un tipo de subjetividad, un tipo de sujeto que tome por su propia voluntad las mismas
decisiones que tomarían los grandes conglomerados de poder, decisiones, en suma, aparentemente
autónomas pero tomadas en favor de otros intereses.

El argumento que sostengo aquí es que la pasividad necesaria para la instrumentalización del
individuo es, a menudo, producto de su reificación bajo distintas apariencias y que el lenguaje
(incluyendo el lenguaje gráfico al que me referiré luego específicamente) actúa como herramienta de
sujección en ese estado de reificación. Por ejemplo, el sujeto deviene elector ante una administración
pública, consumidor ante el mercado, espectador ante los medios, etc., adoptando e interiorizando
distintos roles en función del entorno que trata de instrumentalizarlo. Como ilustración de este
fenómeno propongo la lectura del comienzo de la novela de Philp K. Dick *Sueñan los androides con
ovejas eléctricas* que es en su conjunto una excepcional reflexión sobre el fenómeno de la reificación.

![Diapo1][Diapo1]
Diapo 1. Sueñan los androides con ovejas eléctricas

> Una alegre y suave oleada eléctrica silbada por el despertador automático del órgano de ánimos
> que tenía junto a la cama despertó a Rick Deckard. Sorprendido —siempre le sorprendía
> encontrarse despierto sin aviso previo—, emergió de la cama, se puso en pie con su pijama
> multicolor, y se desperezó. En el lecho, su esposa Iran abrió sus ojos grises nada alegres,
> parpadeó, gimió y volvió a cerrarlos.
>
> —Has puesto tu Penfield demasiado bajo —le dijo él—. Lo ajustaré y cuando te despiertes...    
> —No toques mis controles. —Su voz tenía amarga dureza—. No quiero estar despierta.
>
> Él se sentó a su lado, se inclinó sobre ella y le explicó suavemente:   
> —Precisamente de eso se trata. Si le das bastante volumen te sentirás contenta de estar
> despierta. En C sobrepasa el umbral que apaga la conciencia.
>
> Amistosamente, porque estaba bien dispuesto hacia todo el mundo —su dial estaba en D—,
> acarició el hombro pálido y desnudo de Iran.    
> —Aparta tu grosera mano de policía —dijo ella.    
> —No soy un policía. —Se sentía irritable, aunque no lo había discado.    
> —Eres peor —agregó su mujer, con los ojos todavía cerrados—. Un asesino contratado por la
> policía.    
> —En la vida he matado a un ser humano.    
>
> Su irritación había aumentado, y ya era franca hostilidad.    
> —Sólo a esos pobres replicantes —repuso Iran.    
> —He observado que jamás vacilas en gastar las bonificaciones que traigo a casa en cualquier
> cosa que atraiga momentáneamente tu atención. —Se puso de pie y se dirigió a la consola de su
> órgano de ánimos—. No ahorras para que podamos comprar una oveja de verdad, en lugar de
> esa falsa que tenemos arriba. Un mero animal eléctrico, cuando yo gano ahora lo que me ha
> costado años conseguir. —En la consola vaciló entre marcar un inhibidor talámico (que
> suprimiría su furia), o un estimulante talámico (que la incrementaría lo suficiente para triunfar
> en una discusión).    
> —Si aumentas el volumen de la ira —dijo Iran atenta, con los ojos abiertos— haré lo mismo.
> Pondré el máximo, y tendremos una pelea que reducirá a la nada todas las discusiones que
> hemos tenido hasta ahora. ¿Quieres verlo? Marca... haz la prueba —se irguió velozmente y se
> inclinó sobre la consola de su propio órgano de ánimos mientras lo miraba vivamente,
> aguardando.
>
> Él suspiró, derrotado por la amenaza.    
> —Marcaré lo que tengo programado para hoy. —Examinó su agenda del 3 de enero de 1992:
> preveía una concienzuda actitud profesional—. Si me atengo al programa —dijo
> cautelosamente—, ¿harás tú lo mismo? —Esperó; no estaba dispuesto a comprometerse
> tontamente mientras su esposa no hubiese aceptado imitarlo.    
> —Mi programa de hoy incluye una depresión culposa de seis horas —respondió Iran.    
> —¿Cómo? ¿Por qué has programado eso? —Iba contra la finalidad misma del órgano de ánimos    
> —.Ni siquiera sabía que se pudiera marcar algo semejante —dijo con tristeza.    
> —Una tarde yo estaba aquí —dijo Iran—, mirando, naturalmente, al Amigo Buster y sus
> Amigos Amistosos, que hablaba de una gran noticia que iba a dar, cuando pasaron ese anuncio
> terrible que odio, ya sabes, el del Protector Genital de Plomo Mountibank, y apagué el sonido
> por un instante. Y entonces oí los ruidos de la casa, de este edificio, y escuché los... —hizo un
> gesto.    
> —Los apartamentos vacíos —completó Rick; a veces también él escuchaba cuando debía
> suponer que dormía. Y sin embargo, en esa época un edificio de apartamentos en comunidad
> ocupado a medias tenía una situación elevada en el plan de densidad de población. En lo que
> antes de la guerra habían sido los suburbios, era posible encontrar edificios totalmente vacíos, o
> por lo menos eso había oído decir... Como la mayoría de la gente, dejó que la información le
> llegara de segunda mano; el interés no le alcanzaba para comprobarla personalmente.    
> —En ese momento —continuó Iran—, mientras el sonido del televisor estaba apagado, yo
> estaba en el ánimo 382; acababa de marcarlo. Por eso, aunque percibí intelectualmente la
> soledad, no la sentí. La primera reacción fue de gratitud por poder disponer de un órgano de
> ánimos Penfield; pero luego comprendí qué poco sano era sentir la ausencia de vida, no sólo en
> esta casa sino en todas partes, y no reaccionar... ¿Comprendes? Supongo que no. Pero antes eso
> era una señal de enfermedad mental. Lo llamaban «ausencia de respuesta afectiva adecuada».
> Entonces, dejé apagado el sonido del televisor y empecé a experimentar con el órgano de
> ánimos. Y por fin logré encontrar un modo de marcar la desesperación —su carita oscura y
> alegre mostraba satisfacción, como si hubiese conseguido algo de valor—. La he incluido dos
> veces por mes en mi programa. Me parece razonable dedicar ese tiempo a sentir la desesperanza
> de todo, de quedarse aquí, en la Tierra, cuando toda la gente lista se ha marchado, ¿no crees?    
> —Pero corres el riesgo de quedarte en un estado de ánimo como ése —objetó Rick—, sin poder
> marcar la salida. La desesperación por la realidad total puede perpetuarse a sí misma...    
> —Dejo programado un cambio automático de controles para unas horas más tarde —respondió
> suavemente su esposa—. El 481: conciencia de las múltiples posibilidades que el futuro me
> ofrece, y renovadas esperanzas de...    
> —Conozco el 481 —interrumpió él; había marcado muchas veces esa combinación, en la que
> confiaba—. Oye —dijo, sentándose en la cama y apoderándose de las manos de Iran, a la que
> atrajo a su lado—, incluso con el cambio automático es peligroso sufrir una depresión de
> cualquier naturaleza. Olvida lo que has programado y yo haré lo mismo. Marcaremos juntos un
> 104, gozaremos juntos de él, y luego tú te quedarás así mientras yo retorno a mi actitud
> profesional acostumbrada. Eso me dará ganas de subir al terrado a ver la oveja y de partir
> enseguida al despacho. Y sabré que no te quedas aquí, encerrada en ti misma, sin televisor. —
> Dejó libres los dedos largos y finos de su mujer y atravesó el espacioso apartamento hasta el
> salón, que olía suavemente a los cigarrillos de la noche anterior. Allí se inclinó para encender el
> televisor.
>
> Desde el dormitorio llegó la voz de Iran:
> —No puedo soportar la televisión antes del desayuno.    
> —Marca el 888 —respondió Rick mientras el receptor se calentaba—. Quiero ver la televisión,
> haya lo que hubiere.    
> —En este momento no quiero marcar nada —dijo Iran.    
> —Entonces marca el 3 —sugirió él.    
> —No puedo pedir un número que estimula mi corteza cerebral para que desee marcar otro. No
> quiero marcar nada, y el 3 menos aún, porque entonces tendré el deseo de marcar, y no puedo
> imaginar un deseo más descabellado. Lo único que quiero es quedarme aquí, sentada en la
> cama, y mirar el suelo —su voz se afiló con el acento de la desolación mientras dejaba de
> moverse y su alma se congelaba: el instintivo y ubicuo velo de la opresión, de una inercia casi
> absoluta, cayó sobre ella.
>
> Rick elevó el sonido del televisor, y la voz del Amigo Buster estalló e inundó la habitación.    
> —Hola, hola, amigos. Ya es hora de un breve comentario sobre la temperatura de hoy. El satélite
> Mongoose informa que la radiación será especialmente intensa hacia el mediodía y que luego
> disminuirá, de modo que quienes os aventuréis a salir...
>
> Iran apareció a su lado, arrastrando levemente su largo camisón, y apagó el televisor.    
> —Está bien, me rindo. Marcaré lo que quieras de mí. ¿Goce sexual extático? Me siento tan mal
> que hasta eso podría soportar. Al diablo. ¿Qué diferencia hay...?    
> —Yo marcaré por los dos —dijo Rick, y la condujo al dormitorio.
>
> En la consola de Iran marcó 594: reconocimiento satisfactorio de la sabiduría superior del
> marido en todos los temas. Y en la propia pidió una actitud creativa y nueva hacia su trabajo,
> aunque en verdad no la necesitaba; ésa era su actitud innata y habitual sin necesidad de
> estímulo cerebral artificial del Penfield.

En esta lectura de Philip K. Dick podemos encontrar dos cuestiones, al menos, de suma importancia
para el tema que nos ocupa, reificación y recursividad en la construcción subjetiva.

Reificación del sujeto
----------------------

Los individuos que aparecen en la escena son tratados como autómatas,
androides o robots. Aquello que más claramente los distingue como humanos, esto es, sentirse
humanos, es modulado por una máquina Penfield controladora del sentimiento que puede
programarse a voluntad. Se produce así, de forma un tanto literal, un desplazamiento desde el sujeto
hacia el objeto, puesto que ya no es el sujeto el encargado de producir subjetividad, en tanto ésta se
produce en una máquina. Una perfecta metáfora de las distintas formas de reificación que sufre el
sujeto contemporáneo, un mismo desplazamiento que aparece bajo diferente aspecto dependiendo
del contexto donde se da. El sujeto deviene "usuario" frente a los servicios, "consumidor" frente al
mercado, "elector" frente a las administraciones, etc. La radicalización última de este fenómeno se da
en el devenir del sujeto, no ya en consumidor, sino en producto mismo, desplazamiento que define
en su esencia la sociedad de consumo y que ha sido ampliamente revalidado, redes sociales
mediante, en la actualidad. Aquí, los dispositivos informáticos colectivos actúan como escaparate en
su sentido más literal: un canal de comunicación en un solo sentido (con apariencia de
multidireccional) que permite e incentiva la proyección masiva del sujeto como simulacro. Debido a
la distancia telemática que se impone entre las personas y la consecuente pérdida de matices en la
relación, la interacción con estos dispositivos mediáticos colectivos invierte el escenario ideal —esto
es, el escenario donde el individuo aportaría su singularidad al conjunto del colectivo— produciendo
una normalización del sujeto a partir de cientos de miles de constructos deshumanizados. Así, el
individuo no aporta ya su diferencia al conjunto, enriqueciéndolo, a la vez que conserva su identidad
diferenciada sino que, al contrario, se deja invadir y normalizar o modelar por una macroficción
consensuada.


Recursividad en la construcción del sujeto
------------------------------------------

Otro aspecto interesante de la escena es el momento
final donde se hace explícito el problema de la recursividad:

> "—No puedo pedir un número que estimula mi corteza cerebral para que desee marcar otro."

Aquí Iran se plantea la cuestión ¿qué deseo? y después ¿qué deseo desear? poniendo de manifiesto
que aquello que soy, aquello que me hace desear, puede ser también condicionado por lo que ya soy y
lo que ya deseo. Por otra parte, la subjetividad proyectada sobre la máquina Penfield remite de forma
metafórica a la construcción del sujeto por factores externos. Tenemos, pues, una subjetividad que se
construye en la intersección entre lo interior y lo exterior o, si se quiere, una relación recursiva
sujeto/objeto.

Finalmente, la novela lleva hasta sus últimas consecuencias la deriva sujeto-objeto, ya que su
protagonista, Deckard, tras perseguir y eliminar androides a lo largo del relato, descubre
sorprendido que él mismo es un androide, una máquina. El proceso de cosificación queda así
completado de forma literal.

Un fenómeno interesante es el hecho de que las nuevas formas de dominación que persiguen la
modulación de la voluntad son la consecuencia directa de la conquista de libertades. Si el individuo
tiene libertad de elección habrá diferentes instancias que tratarán de modular su voluntad en favor
de los intereses ajenos, y modular la voluntad es tanto como modular el deseo. El filósofo esloveno
Žižek cuenta una anécdota ilustrativa sobre la diferencia entre estas dos formas de
condicionamiento, la clásica y burda imposición autoritaria o el más sutil condicionamiento del
deseo.

![Diapo2][Diapo2]
Diapo 2. Retrato Žižek con cara de susto
Referencia anécdota Žižek
<http://www.youtube.com/watch?v=K4k95rslBVc#t=25m55s>
[ver minuto 25:55]

El lenguaje y la producción de subjetividad
===========================================

En esta competición por el control de la subjetividad el lenguaje juega un papel fundamental. La
lucha de clases ya no se desarrolla tanto en el ejercicio de la violencia física —que también, como
podemos comprobar con la proliferación de todo tipo de aparatos represores— como en el de la
guerra cultural y, en este contexto, el lenguaje opera como un arma tremendamente efectiva porque
interviene y condiciona la construcción de nuestro sistema cognitivo ayudando a determinar nuestra
comprensión de lo que ocurre especificando aquellas cosas que podemos designar. La forma de
subjetivación —me refiero a subjetividad como el punto de vista del sujeto— determina qué cosas
seremos capaces de ver y comprender y para qué otras cosas permaneceremos ciegos, implica la
asunción de escalas de valores y la promoción de conductas, implica qué aspecto tomará el sujeto y
cuáles serán sus decisiones.

Una sociedad regulada por este tipo de condicionamiento interno puede ser imaginada como una
gigantesca y descomunal granja de humanos que producen diferentes tipos de bienes —votos,
consumo, opinión, pasividad, etc.— Los individuos son producidos como ganado y su alimento es
inmaterial, está compuesto de ideas. Son alimentados con un tipo u otro de ideas para producir un
tipo u otro de bienes. Tal es la importancia de este fenómeno que da origen a una forma de
economía, la “economía de la atención” cuya mecánica parte de una simple regla: la atención es
valiosa porque es la única entrada hacia la cognición, el único camino para condicionar
internamente la voluntad del sujeto. No me refiero a doblegar la voluntad mediante la coerción
(método clásico autoritario de dominación) sino a dirigir la voluntad del individuo desde su interior
condicionando su deseo. La atención, asimismo, es un bien escaso —la capacidad de atención que
despliega la humanidad en su conjunto es limitada, finita—. La conjunción de estos dos factores —
La atención, entendida como única puerta de entrada hacia la cognición, junto con su escasez—
configuran un escenario económico-cultural enormemente complejo que tiene como campo de
batalla al propio individuo y la subjetividad como el territorio en disputa.

El tipo de subjetividad que se produce desde los diferentes ámbitos de poder, es decir, aquel que
incentiva conductas y escalas de valores beneficiosas para las jerarquías dominantes se alimenta con
ciertas estrategias lingüísticas, una de las más habituales consiste en su reducción. La sustracción o
eliminación del lenguaje y sus matices incide directamente en la producción de sentido. Sin un
lenguaje adecuado nuestra experiencia es inaccesible y opaca. Este hecho está avalado brutal y
contundentemente en la actualidad con la intensificación del uso de la neolengua Orwelliana, una
formulación del lenguaje que sustrae significación mediante la utilización de eufemismos, frases
hechas y metáforas parciales.

![Diapo3][Diapo3]
Diapo 3. Retrato de George Orwell

![Diapo4][Diapo4]
Diapo 4. Definición de Neolengua.

Hay muchos ejemplos de uso de la neolengua en la actualidad. Voy a mencionar dos casos para
ilustrar la situación. El primero trata del estratega político norteamericano Franz Luntz, cuyo trabajo
consiste en buscar formas de expresión que tengan una respuesta emocional adecuada —a los
intereses de sus clientes— eliminando las connotaciones y matices que la expresión transportaría
consigo originalmente.

![Diapo5][Diapo5]
Diapo 5. Franz Luntz

Él mismo ha definido su trabajo en relación a la neolengua de Orwell pero utiliza una acepción
propia que elimina su sentido peyorativo. Digamos que su definición de neolengua está formulada en
neolengua.

![Diapo6][Diapo6]
Diapo 6. Definición de neolengua por Luntz

Lunz es, asimismo, el artífice de la transmutación de conocidas expresiones como estas.

![Diapo7][Diapo7]
Diapo 7. Ejemplo de neolengua Luntz

El segundo ejemplo es el popular hashtag de Twitter #neolengua, con en el que se almacenan
infinidad de expresiones suavizadas y reducidas que el órgano ejecutivo del gobierno español emplea
para desactivar la potencial alerta que sus políticas despertarían en la población, políticas que, en
términos generales, repercuten en la desmantelación del estado de bienestar y los servicios sociales.

![Diapo8][Diapo8]
Diapo 8. hashtag #neolengua 1

![Diapo9][Diapo9]
Diapo 9. hashtag #neolengua 2

Así, el lenguaje por sí mismo, en tanto funciona como vehículo para la transmisión cultural, coloca a
la cultura como una efectiva herramienta al servicio de la sociedad disciplinaria. Sirvan estos
ejemplos para entender que las estrategias de control basadas en el lenguaje no son elucubraciones o
fantasías, sino que forman parte de nuestra experiencia cotidiana.

La reducción del lenguaje es una estrategia de control especialmente útil cuando este control se
quiere ejercer de forma masiva. Si bien es cierto que su efecto embrutecedor es eventualmente
valorado como un objetivo por sí mismo existe otra poderosa razón que obliga a reducir el lenguaje
en las relaciones de comunicación asimétricas, es decir, cuando un mensaje debe llegar desde un
único emisor a multitud de receptores. La mecánica es sencilla. Consideremos dos individuos A y B.
El individuo A conoce tres palabras: gato, perro y pato. El individuo B conoce otras tres: perro, gato
y pez. Por tanto, su lenguaje común estará constituido por dos palabras, perro y gato, rango que
podrán utilizar para entenderse.

![Diapo10][Diapo10]
Diapo 10. Reducción del lenguaje 1

Ahora consideremos un tercer individuo cuyo dominio del lenguaje comprende también tres
palabras, que son: gato, pato y pez.

![Diapo11][Diapo11]
Diapo 11. Reducción del lenguaje 2

La intersección de los tres rangos semánticos resulta en una única palabra: gato. La adición de un
individuo al conjunto de hablantes, por tanto, ha supuesto la reducción del lenguaje común útil para
la comunicación del grupo. Podríamos formular una regla general diciendo que “el número de
interlocutores es inversamente proporcional a la riqueza y diversidad del lenguaje común”.
Aunque cada caso particular tendrá sus estrategias particulares, hemos visto cómo en términos
generales el ejercicio de control mediante el lenguaje se apoya en dos estrategias principales: la
imposición —el lenguaje se formula desde algún ámbito de interés, ya sea social, político o
económico, sustrayendo a los individuos la posibilidad de participación en su construcción— y la
reducción —cualquier lenguaje impuesto con intención de ejercer control masivo conlleva
obligatoriamente la reducción del rango semántico común en el código de comunicación—. Es
precisamente en estos dos ámbitos, imposición y reducción, desde los que vamos a enfocar la política
del lenguaje en el diseño gráfico.


Diversidad y riqueza en el lenguaje gráfico
===========================================

La construcción colectiva del lenguaje, como hemos visto, implica un cierto grado de emancipación.
construir entre todos nuestro propio lenguaje es la forma más clara y directa de enfrentar su
dimensión política. Haciendo una analogía entre la filosofía del lenguaje y la antropología social
quisiera aplicar la idea de *no lugar* que Marc Augé propuso como aquel lugar construido de tal
manera que sus habitantes son incapaces de dejar huella y, consecuentemente, la historia —la
historia de sus habitantes— no queda reflejada y no puede ser interpretada. Son espacios mudos,
incapaces de expresar su historia, incapaces de generar huellas que permitan la articulación de una
red social, incapaces de almacenar identidades. Estos espacios construidos a la medida de los
intereses de las macroestructuras no dejan lugar para el individuo, reduciendo las condiciones de
existencia a las mismas que posee una vaca en la industria agroalimentaria intensiva. Son espacios,
en definitiva, diseñados para obtener una rápida y contundente reificación del sujeto, espacios que
contribuyen a la producción de una subjetividad industrializada tan muerta como el Roy Baty del
final de Blade Runner.

Los *no lugares* representan la ausencia de la acción colectiva frente a la imposición autoritaria y
vertical del espacio inerte. Ejemplos son las salas de espera de los aeropuertos, recepciones de
edificios, restaurantes de una multinacional de comida rápida, etc. y su opuesto serían aquellos
lugares construidos de forma colectiva, que almacenan el paso y la acción de los individuos, lugares
vivos que evolucionan ante la presencia de sus habitantes. Podemos encontrar un correlato muy
ajustado de todo ello en el ámbito del lenguaje gráfico. La imagen corporativa, el diseño publicitario
o la señalización de una autopista ostentan el mismo carácter impenetrable e impositivo y la misma
imposibilidad de ser intervenido, generar identidad, red social e historia. Podemos decir, entonces
que existe una correlación entre los no lugares en el espacio y los no lugares en el espacio de
comunicación.

Así, construir el lenguaje colectivamente implica también hacerlo en el ámbito interpersonal porque
en la medida que nos acercamos al ámbito mediático introducimos la asimetría de la comunicación
—desde un emisor a muchos receptores— ejercitando la acción impositiva y autoritaria. Es la
comunicación entre individuos la que debería guiar la construcción colectiva del lenguaje lo que
coloca al diseño gráfico en una encrucijada curiosa, ya que gran parte de su producción se desarrolla
para el ámbito mediático, especialmente si se considera la disciplina del diseño en su dimensión
mercantil: el diseño gráfico profesional se ocupa, sobre todo, de comunicación masiva. La causa se
puede encontrar rápidamente en una simple cuestión de rentabilidad. Invertir recursos en
comunicación gráfica sólo puede entenderse si el público objetivo está compuesto por un número
amplio de individuos. ¿A quién le interesa contratar un diseñador para comunicar con un grupo
pequeño o con una sola persona? Por esta razón sostengo que el diseño gráfico profesional es, desde
un punto de vista estructural, no coyuntural, una disciplina antisocial, y esto es algo que nada ni
nadie podrá cambiar.

Existen formas de diseño gráfico fuera del ámbito profesional que ilustran perfectamente la situación
a la inversa, formas que se ocupan en entablar comunicación interpersonal, no masiva, en las que el
lenguaje gráfico utilizado se enriquece sorprendentemente. Por ejemplo, una actividad que se hizo
popular en el breve periodo de tiempo en el que el CD funcionó como soporte para la música fue la
producción de portadas que algunos diseñaban para ilustrar discos copiados. Unas veces son copias
que se hacen para regalar a un amigo, otras, recopilaciones para la discoteca personal. El libro
“Gracias por la Música” recopila cientos de maravillosos ejemplos.

![Diapo12][Diapo12]
Diapo 12. Portada del libro “Gracias por la música”

En el siguiente ejemplo se puede ver una portada que incluye un nivel sarcasmo y acidez en un
comentario sobre la foto de portada para un disco de John Cage. Se trata de un meta comentario
socarrón y soez que difícilmente podemos encontrar en una producción industrial. Tampoco es
imposible encontrar este tipo de cosas en el mercado musical, pero a medida que las producciones se
pretenden distribuir masivamente el mensaje se hace monótono, repitiendo los mismos clichés y
acudiendo a lugares comunes.

![Diapo13][Diapo13]
Diapo 13. Portada de disco John Cage

Los tres ejemplos siguientes muestran el imaginario encuentro entre la diseñadora de la portada y el
autor del disco en un alarde de ingenio que no necesita más técnica que un bolígrafo o un rotulador.

![Diapo14][Diapo14]
Diapo 14. Portadas de Julia 1

![Diapo15][Diapo15]
Diapo 15. Portadas de Julia 2

![Diapo16][Diapo16]
Diapo 16. Portadas de Julia 3

Ejemplos de todo tipo pueden encontrarse en esta recopilación. Sirvan estos cuatro para ilustrar el
argumento.


El lenguaje del otro
====================

La actitud opuesta a una imposición del lenguaje consistiría en la aceptación del lenguaje de otro,
actitud que se puede llevar al extremo hablando en también ese lenguaje. Entender y aceptar el
lenguaje del otro es una actitud pasiva, hablar con el lenguaje del otro es una actitud activa.
En relación a esta estrategia, tuve la oportunidad de trabajar para el proyecto BCNova que trataba de
articular la sociedad de manteros —migrantes dedicados a la venta ambulante de CDs— para incidir
sobre los problemas de precariedad e integración inherentes al colectivo. Se editaron dos
publicaciones, una para distribución entre el propio colectivo y otra de cara al exterior. Ésta última se
diseñó utilizando un lenguaje que recogía ciertos tópicos del diseño popular urbano de actualidad
con la intención de establecer la identificación con un público que se ve a sí mismo como poseedor
de esos códigos.

![Diapo17][Diapo17]
Diapo 17. Mantazine BCNova 1

Por el contrario, el diseño para la segunda publicación, dirigida al colectivo de manteros, se planteó
en términos opuestos, utilizando su propio lenguaje, pero no de la misma forma en que la publicidad
adopta la apariencia de su público objetivo imitando su lenguaje, una piel de cordero sobre un lobo,
sino incentivando su propia participación en la construcción del lenguaje. El diseño y maquetación
fue delegado enteramente en el impresor, un migrante hindú que reprodujo los contenidos con una
sinceridad inalcanzable para cualquier diseñador profesional.

![Diapo18][Diapo18]
Diapo 18. Mantazine BCNova 2

El lenguaje gráfico y la construcción colectiva de identidad
============================================================

[texto en proceso]


[Diapo1]: /media/images/diapo1.jpg.png
[Diapo2]: /media/images/diapo2.jpg.png
[Diapo3]: /media/images/diapo3.jpg.png
[Diapo4]: /media/images/diapo4.jpg.png
[Diapo5]: /media/images/diapo5.jpg.png
[Diapo6]: /media/images/diapo6.jpg.png
[Diapo7]: /media/images/diapo7.jpg.png
[Diapo8]: /media/images/diapo8.jpg.png
[Diapo9]: /media/images/diapo9.jpg.png
[Diapo10]: /media/images/diapo10.jpg.png
[Diapo11]: /media/images/diapo11.jpg.png
[Diapo12]: /media/images/diapo12.jpg.png
[Diapo13]: /media/images/diapo13.jpg.png
[Diapo14]: /media/images/diapo14.jpg.png
[Diapo15]: /media/images/diapo15.jpg.png
[Diapo16]: /media/images/diapo16.jpg.png
[Diapo17]: /media/images/diapo17.jpg.png
[Diapo18]: /media/images/diapo18.jpg.png
</div>
