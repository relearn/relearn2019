Title: Shaping Processes
Lang: en
Date: 2012
Authors: Alexandre Leray; Stéphanie Vilayphiou

This chapter focuses on how a work—of art, design or software—addresses the public and invites people to engage with it. It suggests how tools and design objects may reveal their own machinery, their genealogy, and reflect on their own subjectivity and on the dynamics in which they have come about. By doing so these works offer a critical framework to constantly revisit their meaning and function.

In [_Softness_](softness-interrogability-general-intellect-art-methodologies-in-software.html), Matthew Fuller suggests some methodologies for artists to escape the average user-centered computer experience and connect back with the materiality of computational media. Taking examples from modern and contemporary media art, he talks about interrogability, a quality of artworks reflecting on their own way of operating, thus offering a way to understand how they are made and the possibility to make them evolve.

In [_Coordinating Collaborations_](coordinating-collaborations.html),  Chris Kelty explores how free and open source movement is "vitally concerned with its own existence as a public". Going back and forth between planning and improvisation, FLOSS allies criticism and the creation of working alternatives. Linux and Apache projects are put in contrast to show the different approaches to decision making, and the tools developed to support their respective models of collaboration.

In [_The politics of Language in Graphic Design_](the-politics-of-language-in-graphic-design.html), graphic designer Aitor Méndez imagines what could be an open design practice. He strives for the appropriation of a design by the public, encouraging variations based on an initial design, leaving space for improvisation where design has always been about planning. 


Content
-------

* [_Coordinating Collaborations_](coordinating-collaborations.html)
* [_Softness: Interrogability; general intellect; art methodologies in software_](softness-interrogability-general-intellect-art-methodologies-in-software.html)
* [_The politics of Language in Graphic Design_](the-politics-of-language-in-graphic-design.html)
