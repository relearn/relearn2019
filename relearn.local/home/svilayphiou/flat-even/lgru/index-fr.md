Title: Considering your tools
Lang: fr
Date: 2012
Authors: Alexandre Leray; Stéphanie Vilayphiou

> «C’est dans sa manière de « diverger » que l’on approche une pratique, c’est-à-dire en explorant ses frontières, cherchant quelles questions les praticiens pourraient accepter comme pertinentes, même si elles ne sont pas les leurs, quelles questions ne seraient pas jugées insultantes, les poussant à se mobiliser et à transformer la frontière en une ligne de défense dressée contre l’extérieur.»
> 
> Isabelle Stengers, «Notes pour une écologie des pratiques»

La création contemporaine depend largement des outils numériques. Loin d'être des moyens neutres en vue d'une réalisation artistique, ces outils sont en fait dogmatiques: ils portent des valeurs et sont pleins de conventions sur la façon dont les choses doivent être faites.

Il nous semble q'une plus grande prise de conscience du rôle des outils numériques est cruciale pour l'éducation des artistes et designers. Au lieu de n'être que des moyens de production, les dispositifs logiciels et matériels peuvent devenir des interlocuteurs avec qui penser, échanger, questionner voir à entrer en conflit avec. Et parce que la création (visuelle) est si étroitement liée avec le développement technologique, une plus grande prise de conscience de ces outils peut aider à spéculer sur les pratiques de demain et inventer les outils pour les soutenir.

Contrairement à d'autres champs contemporains de création, il y a peu de littérature sur ces questions dans la spère du design graphique. C'est pourquoi nous sentions qu'il était important de rassembler des textes et des exemples sur ce sujet dans un corpus aussi complet que possible: un outil pour penser aux outils.

À seulement cinq exceptions près, tous le contenu dans ce reader est disponible sous des licences qui invitent à sa réutilisation, sa distribution et sa ré-appropriation. Cela signifie que ces textes peuvent circuler librement, et être inclus dans d'autres publications numériques ou imprimés; peuvent être traduits ou utilisés de quelque autre manière que ce soit.

Contenu
=======

Cette publication contient des textes nouvellement commissionés, traduits ou ré-édités, distribués à travers 5 chapitres:

* [Des gestes discrets](des-gestes-discrets/) parle de la manière dont notre corps est informé par nos outils numériques.
* [Interfaces de lecture](interfaces-de-lecture) présente différentes approches pour la capacitation informatique.
* [La fabrication des standards](la-fabrication-des-standards) s'interesse aux processus sociaux et techniques sous-jacents à l'élaboration des normes.
* [Une myriade d'outils de composition](une-myriade-doutils-de-composition) se saisit de la question des logiciels comme objects culturels à travers le prisme de la typographie numérique.
* Finalement, [Reveler les processus](reveler-les-processus) discute des méthodologies pour des pratiques collective ouvertes et critiques.
* [Un texte](notes-pour-une-ecologie-des-pratiques.html) de la philosophe belge Isabelle Stengers et une introduction générale donnent une vision plus large sur les questions des outils et de la culture libre en relation avec les pratiques.
