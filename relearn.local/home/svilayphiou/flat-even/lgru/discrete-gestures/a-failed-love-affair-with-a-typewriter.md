License: All rights reserved
Title: A Failed Love Affair with a Typewriter
Authors: Friedrich A. Kittler 
Date: 1986
Lang: en


<div markdown=true class="original">
Taken from Friedrich A. Kittler, *Gramophone, Film, Typewriter* translated by Geoffrey Winthrop-Young and Michael Wutz, Stanford: Standford University Press, 1999 (200-208). 
Originally published in German in 1986 as *Grammophon Film Typewriter*, © 1986 Brinkmann & Bose, Berlin.

English Translation © 1999 by the Board of Trustees of the Leland Stanford Jr. University.

All rights reserved. No further use, reproduction or distribution is permitted without the prior written approval of the publisher.

* * *

“Our writing tools are also working on our thoughts,” Nietzsche wrote. [^1] “Technology *is* entrenched in our history,” Heidegger said. But the one wrote the sentence about the typewriter on a typewriter, the other described (in a magnificent old German hand) typewriters per se. That is why it was Nietzsche who initiated the transvaluation of all values with his philosophically scandalous sentence about media technology. In 1882, human beings, their thoughts, and their authorship respectively were replaced by the two sexes, the text, and blind writing equipment. As the first mechanized philosopher, Nietzsche was also the last. Typescript, according to Klapheck’s painting, was called *The Will to Power*.

Nietzsche suffered from extreme myopia, anisocoria, and migraines (to say nothing of his rumored progressive paralysis). An eye doctor in Frankfurt attested that Nietzsche’s “right eye could only perceive misshapen and distorted images” as well as “letters that were virtually beyond recognition,” whereas the left, “despite its myopia,” was in 1877 still capable of “registering normal images.” Nietzsche’s headaches therefore appeared to be “a secondary symptom,” [^2] and his attempts to philosophize with a hammer the natural consequence of “an increased stimulation of the site in the prefrontal wall of the third ventricle responsible for aggression.” [^3] Thinkers of the founding age of media naturally did not only turn from philosophy to physiology in theory; their central nervous system always preceded them.

Nietzsche himself successfully described his condition as quarter blindness, half-blindness, three-quarter blindness (it was for others to suggest mental derangement, the next step in this mathematical sequence). [^4] Reading letters (or musical notes) distorted beyond recognition became painful after twenty minutes, as did writing. Otherwise, Nietzsche would not have attributed his “telegram style”, [^5] which he developed while writing the suggestively titled *The Wanderer and His Shadow*, to his eye pain. To direct the blindness of this shadow, he had been planning to purchase a typewriter as early as 1879, the so-called “year of blindness”. [^6] It happened in 1881. Nietzsche got “in touch with its inventor, a Dane from Copenhagen.” [^7] “My dear Sister, I know Hansen’s machine quite well, Mr. Hansen has written to me twice and sent me samples, drawings, and assessments of professors from Copenhagen about it. *This* is the one I want (*not* the American one, which is too heavy).” [^8]

Since our writing tools also work on our thoughts, Nietzsche’s choice followed strict, technical data. En route between Engadine and the Riviera, he decided first for a traveling typewriter and second as the cripple that he was. At a time when “only very few owned a typewriter, when there were no sales representatives [in Germany] and machines were available only under the table,” [^9] a single man demonstrated a knowledge of engineering. (With the result that American historians of the typewriter elide Nietzsche and Hansen.) [^10]

![Figure0](/media/images/theorie.jpg)

Hans Rasmus Johann Malling Hansen (1835-90), pastor and head of the Royal Døvstummeinstitut in Copenhagen, [^11] developed his *skrivekugle*/writing ball/*sphère écrivante* out of the observation that his deaf-mute patients’ sign language was faster than handwriting. The machine “did not take into account the needs of business” [^12] but rather was meant to compensate for physiological deficiencies and to increase writing speed (which prompted the Nordic Telegraphy Co. to use “a number of writing balls for the transfer of incoming telegrams”). [^13] Fifty-four concentrically arranged key rods (no levers as yet) imprinted capital letters, numbers and signs with a color ribbon onto a relatively small sheet of paper that fast fastened cylindrically. According to Burghagen, this semispheric arrangement of the keys had the advantage of allowing “the blind, for whom this writing ball was primarily designed, to learn writing on it in a surprisingly short time. On the surface of a sphere each position is completely identifiable by its relative location… It is therefore possible to be guided solely by one’s sense of touch, which would be much more difficult in the case of flat keyboards.” [^14] That is precisely how it could have been stated in the assessments of professors from Copenhagen for a half-blind professor.

In 1865 Malling Hansen received his patent, in 1867 he started serial production of his typewriter, in 1872 the Germans (and Nietzsche?) learned of it from the *Leipziger Illustrirte Zeitung*. [^15] Finally, in 1882 the Copenhagen printing company of C. Ferslew combined typing balls and women—as a medium to offset the nuisance that “their female typesetters were significantly more preoccupied with the decoding of handwritten texts than with the actual setting of text.” [^16] McLuhan’s law that the typewriter causes “an entirely new attitude to the written and printed word” because it “fuses composition and publication” [^17] was realized for the first time. (Today, when handwritten publisher’s manuscripts are rarities, “the entire printing industry, via the Linotype, depend[s] upon the typewriter.”) [^18]

In the same year and for the same reasons, Nietzsche decided to buy. For 375 Reichsmarks (shipping not included) [^19] even a half-blind writer chased by publishers was able to produce “documents as beautiful and standardized as print.” [^20] “After a week” of typewriting practice, Nietzsche wrote, “the eyes no longer have to do their work”: [^21] *écriture automatique* had been invented, the shadow of the wanderer incarnated. In March 1882, the *Berliner Tageblatt* reported:

The well-known philosopher and writer [sic] Friedrich Nietzsche, whose failing eyesight made it necessary for him to renounce his professorship in Basel three years ago, currently lives in Genoa and—excepting the progressions of his affliction to the point of complete blindness—feels better than ever. With the help of a typewriter he has resumed his writing activities, and we can hence expect a book along the lines of his last ones. It is widely known that his new work stands in marked contrast to his first, significant writings. [^22]

> Indeed: Nietzsche, as proud as the publication of his mechanization as any philosopher, [^23] changed from arguments to aphorisms, from thoughts to puns, from rhetoric to telegram style. That is precisely what is meant by the sentence that our writing tools are also working on our thoughts. Malling Hansen’s writing ball, with its operating difficulties, made Nietzsche into a laconic. “The well-known philosopher and writer” shed his first attribute in order to merge with his second. If scholarship and thinking, especially toward the end of the nineteenth century, were allowed or made possible only after extensive reading, then it was blindness alone that “delivered” them from “the book.” [^24]

Good news from Nietzsche that coincided with all the early typewriter models. None of the models prior to Underwood’s great innovation of 1897 allowed immediate visual control over the output. In order to read the typed text, one had to lift shutters on the Remington model, whereas with Malling Hansen’s—notwithstanding other claims [^25]—the semicircular arrangement of the keys itself prevented a view of the paper. But even Underwood’s innovation did not change the fact that typewriting can and must remain a blind activity. In the precise engineering lingo of Angelo Beyerlen, the royal stenographer of Württemberg and the first typewriter dealer of the Reich: “In writing by hand, the eye must constantly watch the written line and only that. It must attend to the creation of each sign, must measure, direct, and, in short, guide the hand through each movement.” A media-technological basis of classical authorship that typewriting simply liquidates: “By contrast, after one briefly presses down on a key, the typewriter creates in the proper position on the paper a complete letter, which is not only untouched by the writer’s hand but also located in a place entirely apart from where the hands work.” With Underwood’s models, too, “the spot where the next sign to be written *occurs*”, is “precisely what… *cannot* be seen.” [^26] After a fraction of a second, the act of writing stops being an act of reading that is produced by the grace of a human subject. With the help of blind machines, people, whether blind or not, acquire a historically new proficiency: *écriture automatique*.

Loosely translated Beyerlen’s dictum that “for writing, visibility is as unnecessary today as it has always been,” [^27] an American experimental psychologist (who in 1904 measured the “Acquisition of Skill in Typewriting” and who obliged his subjects to keep typed test diaries) recorded documentary sentences like those of André Breton:

> 24<sup>th</sup> day. Hands and finger are clearly becoming more flexible and adept. The change now going on, aside from growing flexibility, is in learning to locate keys without waiting to see them. In other words, it is location by position.
> 
> 25<sup>th</sup> day. Location (muscular, etc.), letter and word associations are now in progress of automatization.
> 
> 38<sup>th</sup> day. Today I found myself not infrequently striking letters before I was conscious of seeing them. They seem to have been perfecting themselves just below the level of consciousness. [^28]

“A Funny Story About the Blind, etc.” (Beyerlen’s essay title) was also the story of the mechanized philosopher. Nietzsche’s reasons for purchasing a typewriter were very different from those of his few colleagues who wrote for entertainment purposes, such as Twain, Lindau, Amytor, Hart, Nansen, and so on. [^29] They all counted on increased speed and textual mass production; the half-blind, by contrast, turned from philosophy to literature, from rereading to a pure, blind, and intransitive act of writing. That is why his Malling Hansen typed the motto of all modern, highbrow literature: “Finally, when the eyes prevent me from *learning* anything—and I have almost reached that point! I will still be able to craft verse.” [^30]

1889 is generally considered the year zero of typewriter literature, that barely researched mass of documents, the year in which Conan Doyle first published *A Case of Identity*. Back then, Sherlock Holmes managed to prove his claim that the typed love letters (including the signature) received by one of London’s first and ostensibly myopic typists were the work of her criminal stepfather engaging in marriage fraud. A machine-produced trick of anonymization that prompted Holmes, seventeen years prior to the professionals in the police, to write a monograph entitled *On the Typewriter and Its Relation to Crime.* [^31]

Our esteem for Doyle notwithstanding, it is nonetheless an optical-philological pleasure to show that typewriting literature began in 1882—with a poem by Friedrich Nietzsche that could well be titled *On the Typewriter and Its Relation to Writing*.

In these typed, that is, literally forged or crafted, verses, three moments of writing coincide: the equipment, the thing, and the agent. An author, however, does not appear because he remains on the fringes of the verse: as the addressed reader, who would “utilize” the “delicate” [^32] writing ball known as Nietzsche in all its ambiguity. Our writing tool not only works on our thoughts, it “is a thing like me.” Mechanized and automatic writing refuses the phallocentrism of classical pens. The fate of the philosopher utilized by his fine fingers was not authorship but feminization. Thus Nietzsche took his place next to the young Christian women of Remington and the typesetters of Malling Hansen in Copenhagen.

But that happiness was not to last long. The human writing ball spent two winter months in Genoa to test and repair its new and easily malfunctioning favorite toy, to utilize and compose upon it. Then the spring on the Riviera, with its downpours, put an end to it. “The damned writing,” Nietzsche wrote, self-referentially as always, “the typewriter has been *unusable* since my last card; for the weather is dreary and cloudy, that is, humid: then each time the ribbon is also *wet* and *sticky*, so that every key gets stuck, and the writing cannot be seen *at all*. If you think about it!!” [^33]

* * *
<figure>
    <img src="/media/images/hansen.jpg" alt="Figure 1" />
    <figcaption>
    A facsimile Nietzsche’s Mailing Hansen poem, February-March 1882 <br />
    THE WRITTING BALL IS A THING LIKE ME: MADE OF / IRON / YET EASEALY TWISTED ON JOURNEYS. / PATIENCE AND TACT ARE REQUIRED IN ABUNDANCE, / AS WELL AS FINE FINGERS, TO USE US
    </figcaption>
</figure>

And so it was rain in Genoa that started and stopped modern writing—a writing that is solely the materiality of its medium. “A letter, a litter,” a piece of writing, a piece of dirt, Joyce mocked. Nietzsche’s typewriter, or the dream of fusing literary production with literary reproduction, instead fused again with blindness, invisibility, and random noise, the irreducible background of technological media. Finally, letters on the page looked like the ones on the right retina.

But Nietzsche did not surrender. In one of his last typewritten letters he addressed media-technological complements and/or human substitution: the phonograph and the secretary. “This machine,” he observed in another equation of writing equipment with writer, “is as delicate as a little dog and causes a lot of trouble—and provides some entertainment. Now all my friends have to do is to invent a reading machine: otherwise I will fall behind myself and won’t be able to supply myself with sufficient intellectual nourishment. Or, rather: I need a young person who is intelligent and knowledgeable enough to *work* with me. I would even consider a two-year-long marriage for that purpose.” [^34]

With the collapse of his machine, Nietzsche became a man again. But only to undermine the classical notion of love. As with men since time immemorial and women only recently, “a young person” and a “two-year-long marriage” are equally suitable to continue the failed love affair with a typewriter.

And so it happened. Nietzsche’s friend Paul Rée, who had already transported the Malling Hansen to Genoa, was also searching for its human replacement: somebody who could “aid” Nietzsche “in his philosophical studies with all kinds of writing, copying, and excerpting.” [^35] But instead of presenting an intelligent young man, he presented a rather notorious young lady who, “on her path of scholarly *production*,” required a “teacher”: [^36] Lou von Salomé.

[^1]: Friedrich Nietzsche, letter toward the end of February, in F. Nietzsche *Briefwechsel: Kritische Gesamtausgabe*, G. Colli and M. Montinari (eds), Berlin, 1975 – 84, pt. 3, 1: 172.
[^2]: Dr Eiser, 1877, quoted in Joachim Fuchs, “Friedrich Nietzsches Augenleiden” *in Münchener Medizinische Wochenschrift 120*, 1978: 632.
[^3]: Ibid., 633.
[^4]: After an observation by Martin Stingelin of Basel.
[^5]: Friedrich Nietzsche, letter of November 5, 1879, in idem 1975–84, pt. 2, 5: 461
[^6]: Friedrich Nietzsche, letter of August 14, 1822, in idem 1975–1984, pt. 2, 5: 435.
[^7]: Friedrich Nietzsche, letter of August 14, 1822, in idem 1975–84, pt. 3, 1: 113.
[^8]: Friedrich Nietzsche, letter of December 5, 1881, in idem 1975–84, pt. 3, 1: 146
[^9]: Otto Burghagen, *Die Schreibmaschine. Illustrierte Beschreibung aller gangbaren Schreibmaschinen nebst gründlicher Anleitung zum Arbeiten auf sämlichen Systemen, Hamburg*, 1898: 6.
[^10]: Apparently infected, Nietzsche’s biographer corrects his hero (saying that “the typewriter was ‘invented,’ that is, developed, 10 years earlier [sic] in America”). To top it off, he even writes “Hansun” instead of “Hansen” (Kurtz Paul Janz, *Friedrich Nietzsche: Biographie*, 3 vols., Munich, 1978–79, 2: 81, 95)
[^11]: The following data are taken from Camillus Nyrop, *“Malling Hansen” in Dansk Biografisk Leksikon*, Povl Engelstoft, vol. 18, Copenhagen, 1938: 265–267.
[^12]: Burghagen 1898, 6.
[^13]: See Rolf Stümpel (ed.), *Vom Sekretär zur Sekretärin : Eine Austellung zur Geschichte der Schreibsmaschine und ihrer Bedeutung für den Beruf der Frau im Burö*, Gutenberg Museum, Mayence, 1985: 22. There were even writing balls with a Morse-code hookup (See Ludwig Brauner, *Die Schreibmaschine in teknischer kultureller und wissenschaftlicher Bedeutung*, Sammlung geimeinnütziger Voträge, Deutscher Verein zur Verbreitung gemeinnütziger Kenntnisse in Prag, 1925: 35–36).
[^14]: Burghagen 1898, 120.
[^15]: See Ernest Martin, *Die Schreibmashine und ihre Entwicklungsgeschichte*, 2nd edition Papenheim, 1949: 571.
[^16]: Stümpel 1985, 8.
[^17]: See Marshall McLuhan, *Understanding Media*, New York, 1964: 260.
[^18]: Bruce Bliven jr, *The Wonderful Writing Machine*, New York, 1954: 132.
[^19]: Nietzsche, letter of August 20–21, 1881, in idem 1975-84, pt. 3, 1: 117 (possible pb ds le français)
[^20]: Burghagen 1898, 120 (referring to Malling Hansen’s typewriter).
[^21]: See Nietzsche, letter of August 20–21, 1881, in idem 1975–84, pt.3, 1: 117.
[^22]: *Berliner Tageblatt*, March 1882.
[^23]: See Nietzsche, letter of March 17, 1882, in idem 1975–84, pt. 3, 1: 180. “I enjoyed a report of the *Berliner Tageblatt* about my existence in Genoa—even the typewriter was mentioned.” The mechanized philosopher clipped the news item.
[^24]: Friedrich Nietzsche, Ecce homo, in *On the Genalogy of Morals and Ecce Homo*, W. Kaufmann et R.J. Hollingdale, New York, 1967: 287.
[^25]: See, for example, Werner von Eye, *Kurzgefasste Geschichte der Schreibmaschine und des maschinensschreibens*, Berlin, 1958: 20.
[^26]: Angelo Beyerlen quoted in Richard Herbertz, *“Zur Psychologie des maschinenschreibens” , in Zeitschrift für angewandte psychologie 2 (1909)*, 559.
[^27]: Beyerlen 1909, 362.
[^28]: Edgar Swift, “The Acquisition of Skill in Type-Writing: A Contribution to the Psychology of Learning” *in The Psychological Bulletin I* (1904), 299, 300, 302. Also see the self-observation in the novel by Christa Anita Brück, *Schicksale hinter Schreibmaschinen*, Berlin (1930: 238): “Here I sit, day by day,… typing freight letters, freight letters, freight letters. After three days it turned into purely mechanical work, the dim interactions between eyes and fingers, in which consciousness does not actively participate.”
[^29]: This list of early typewriting authors is taken from Burghagen 1898, 22.
[^30]: Nietzsche, letter of April 1, 1882, in idem 1975–84, pt.3, 1: 188.
[^31]: See sir Arthur Conan Doyle, *The Complete Sherlock Holmes*, Christopher Morley (ed.), Garden City, New York, 1930: 199.
[^32]: Nietzsche, letter of March 17, 1882, in idem 1975–84, pt.3, 1: 180.
[^33]: Nietzsche, letter of March 27, 1882, in idem 1975–84, pt.3, 1: 188.
[^34]: Nietzsche, letter of August 17, 1882, in idem 1975–1984, pt.3, 1: 180; on the “reading machine,” see Nietzsche, letter of December 21, 1881, in idem 1975–84, pt. 3, 1: 151.
[^35]: Förster-Nietzsche, in Friedrich Nietzsche, *Briefwechsel*, Elisabeth Förster –Nietzsche and Peter Gast (eds), Berlin, 1902-1909, pt. 5, 2: 488.
[^36]: Nietzsche, letter of June 18, 1882, in idem 1975–84, pt.3, 1: 206.


An extract of Friedrich Kittler’s *Gramophone, Film, Typewriter* (1986) chosen and introduced by Patricia Falguières

> First published in German edition in Berlin in 1986, *Gramophone, Film, Typewriter* by Friedrich Kittler has become a legendary book. Next to *Du mode d’existence des objets techniques* by Gilbert Simondon (Paris 1969) or Avital Ronell’s *Telephone Book* (Lincoln 1989), *Gramophone…* stands out as a rare example of a successful philosophical foray into the field of technology and recording devices, undertaken at the end of the XXth Century. For *Gramophone…,* Kittler proceeds from a radical reading of Derrida – the Derrida of *Grammatology* (1967) –, and takes as a literal starting point of this journey the idea that our tools work on our thoughts: no recording technology can be neutral, each one originates from, as much as it has an influence on a trail of affects which need to find their place within the history of thought. Kittler traces an impressive philosophical route across the history of recording media leading us from the laboratory of Thomas Edison in Menlo Park (where he invented the gramophone), to the headquarters of General Ludendorff and the trenches of WW1; from the postcards sent by Kafka to Felice Bauer, to the deciphering of the Enigma Code by Turing during WW2…
> 
> Paying careful attention to the materiality of technical processes, like few philosophers before him, Kittler revives and enriches some pioneering approaches to technology which were first developed in Weimar’s Germany. Approaches that, with the exception of the work of Walter Benjamin, have since been completely forgotten, (let’s think of the amnesia into which the theory of film editing developed by Bela Balazs has fallen, for example). With an attention to the physical presence of ‘situated’ objects and mechanisms but also a sense of dramatization that grants them a remarkable historical and poetic depth, Kittler touches upon something of the fascination that characterizes the avant-garde of the twenties and thirties, when Dziga Vertov, Moholy-Nagy or Fritz Lang turned the gramophone, the editing table or the typewriter into key protagonists of film, photography or books. Just remember, at the end of *The Testament of Dr. Mabuse* (1933), the discovery of this *deus ex machina* suddenly revealed behind a curtain of illusions, the mechanical voice of this diabolic band leader: the gramophone.
 
Patricia Falguières
</div>
