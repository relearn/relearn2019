Type: article
Authors: George Francis
Title: Essay on Drawing Palettes
Date: 2010
Original: http://new.math.uiuc.edu/netgeom/advice/palettes.html
License: all rights reserved, courtesy of the rights holder
Lang: en


<div markdown=true class="original">

![macpaint](/media/images/macpaint-palette.png)

History
=======

Geometers and artists are likely to face many different computer applications with drawing tool kits. And each has a different *palette* (a.k.a. drawing toolbar) reflecting a different philosophy of what constitutes a minimal set of drawing tools. Possibly arrogance, but more likely avoiding copyright infringement, these palettes not only use different though similar icons, but tools are missing or present in a wholly illogical manner.

All, however, have their ancestry firmly rooted in Bill Atkinson’s justly famous *MacPaint* application which helped launch the original Macintosh computer (google “Atkinson MacPaint”). Each of its twenty tools were intelligently thought out, very useful, and most were patented. As a result, the descendant palettes left out entire subsets of valuable tools, sometimes substituting new ones. For instance, I have never again found the *lasso* (top left), and only Microsoft’s *Paint* and Apple’s *iPaint* still have the *fat-bits* (on MacPaint toolbar, and represented thereafter by a magnifying glass in palettes that have it there.)

Of course, MacPaint was monochrome and all modern palettes are polychrome. The descendants soon bifurcated into ever more sophisticated and therefore expensive applications that evolved into the gold-standard, *Adobe Photoshop*, and devolved into ever more impoverished freebies, like Paint and iPaint.

Some MacPaint tools
-------------------

The *lasso* shrank to the image it sourrounded and this could be moved. The *hand* pushed stuff around, the *letter A* permitted the insertion of typed text, the *paint bucket* filled regions with textures, the *spray can* created interesting textures, the *paint brush* provided a variety of line styles, the *pencil* was for free hand drawing, the *line* introduced the socalled *rubber-band cursor* for drawing lines and polygons, and the *eraser* did what it says. These were followed by a number of shape drawing tools, including the *spline* for composing curves.

Along with the word *palette*, we shall here use the original names but in a generic and informal manner. Not every fat-bits tool since works the way it did in MacPaint. The freehand drawing tool, the *pencil*, also differs in a special way across the different palettes. When one draws a free-hand curve, especially with a clumsy tool like a mouse, the hand is apt to jitter, and so does the line drawn. With any new palette you encounter, try to execute your signature. If it comes out looking half-way decent, then the pencil is being *splined*. That means, your *stroke* is sampled and only smooth interpolations between some of the pixels you visit are drawn. This is an expensive feature and most palettes nowadays skip it. The result is a tool that cannot really be used for free hand drawing. It’s not that the artist suffers from some kind of palsy.

Paint and iPaint
================

![paint](/media/images/paint-palette.png)
![ipaint](/media/images/ipaint-palette.png)

These are mentioned here because *Paint* is most likely on your PC, and iPaint is what we use on the macs in the labs. Paint has a palette not dissimilar enough from MacPaint, so that the above description suffices for you to start experimenting with it. I need a good Paint tutorial, so if you’re inclined to write one, do it, and send it to me.

*iPaint* came as something of a surprise. It has only half as many tools as MacPaint. And some act quite differently from those in Paint. The fat-bits tool is in the menus. There is no lasso. The pencil has a thickness slider, and you can select the ink color. The scissors are most useful for cutting and pasting (moving stuff around). But the spline tool takes some getting used to, and it helps to know how Bezier splines work.

Both Paint and iPaint have undo/redo editing abilities, which is essential when our using it and invariably make mistakes. Sadly, the *Whiteboard* in *Elluminate* does not have the abilty to erase mistakes easily. The reason for this will become clearer below.

As with all descendants to draw a square, you need to eyeball a rectangle carefully. Circles are special ellipses. Most surprisingly, circles don’t have a center or radius. You have to guess.

If it matters how your figures look, use proper geometry drawing tools, many of which are also free. We shall discuss two below.

Word and Whiteboard
-------------------

![word](/media/images/word-palette.png)
![whiteboard](/media/images/whiteboard-palette.png)

I mentioned, these are included here because you are undoubtedly familiar with the palette in *Microsoft Word* on the left. I don’t use Word when I can help it, and you shouldn’t use it in my geometry courses either, for reasons explained elsewhere.

The palette from the *Whiteboard* in *Elluminate* has a rudimentary palette (on the right) which is no match to those of Paint and iPaint. A careful instruction on how to use this palette is given elsewhere. I’ll only describe some interesting and potentially fatal features.

For one, there is no intuitively obvious undo/redo feature in the Whiteboard. This is a huge omission which makes it almost unusable. The free-hand drawing tool, here represented by a pen, not a pencil, is not splined. You cannot sign your name. For another, there is no eraser. It is said that a philosopy professor is cheaper than a mathematics professor at the University. Both require only a pad and pencil. But the philosopher doesn’t require an eraser.

Here is the non-intuitive reason for this situation. The Whiteboard (and MS Word) use vector graphics rather than bitmap graphics. Most briefly, in bitmap graphics, you color in the tiny squares known as pixels. When you use the line-drawing tool, a Bresenham algorithm based on your initial and final position calculates the intermediate pixels to make an apparently smooth straight line. Like on a piece of paper, you keep filling in pixels with various colors. When you’ve made a mistake, or dislike some part of the screen, you can “erase it” by matching a certain portion of pixels back to the background color.

In vector graphics, the line is also drawn using the Bresenham algorithm. But the figure on the screen can re-addressed with a picking tool, and edited. For example, it can be deleted. The document stores only the data needed for the Bresenham line to be drawn. So, this so-called graphics object can easily be moved to a new location, resized, etc etc. Historically, Apple’s MacPaint used bitmap graphics, and MacDraw used vector graphics. Adobe’s Photoshop is a descendant of MacPaint, and Illustrator is a descendant of MacDraw. We shall return to this issue below.

Since we do use the Whiteboard for Elluminate sessions, a few more words are in order here. If you *hover* your mouse over an icon in the palette of the Whiteboard, a text window appears telling you what the tool does. Experimentation is advisable. The *pen* and *highlighter* are useful. So is the text entry, but a socalled *text editor* to its right is too difficult to be useful. Use it to move text fragments around, but don’t try to correct mispellings. If you do want to correct a mispelling, choose the object and then use the inline editor. Sometimes it is easiest to chose, delete and rewrite a short piece of text.

The open a filled circle/ellipse and open a filled square/rectangle works like everywhere else. The line drawing tool is a rubber-band curser. You can insert a bitmapped image (.png, .bmp, .jpg), but it won’t let you load non image files, like .pdfs. Moreover, you can’t edit such an image-object. You can try to draw into it, but not make your graffiti stick to the artwork. The clipart is what it says. Cutesey predrawn images.

Curiously, the Whiteboard uses an advanced feature more proper to advanced tools, like Photoshop, namely so-called *layers*. This permits a level of interactivity of several users which is *not* typical of blackboard, or real whiteboards. This makes its use non-intuitive.


Geometry Constructions
======================

Applications that can do geometricaly correct constructions necessarily have more intelligently designed palettes or they could not be used for their intended purposes. The gold-standard of these is Nicholas Jackiw’s *Geometer’s Sketchpad*. There are many more. Here we restrict our discussion to Ilya Baran’s open source *KSEG* (first) and Michael Hvidstens fully non-Euclidean *Geometry Explorer* (GEX second), the two we use in our courses.

Both applications, by the way, have excellent documentation. Baran’s is short and sweet and expects the user to do some intelligent experimentations. Hvidsten offers a 200 page manual which can serve as a course in geometry all by itself. So here we compare only some superificical and profound aspects of their palettes.

Baran says he designed KSEG because he couldn’t afford to buy a copy of Geometer’s Sketchpad. I’ve been using KSEG in courses that do not require the non-Euclidean plane. For courses that do, GEX is unique in that it has many more of the models for non-Euclidean, and for Euclidean geometry than any other program out there. Thus each is most optimally adapted to its purpose. Joel Castellano’s *nonEuclid* is also a good construction program for non-Euclidean geometry.

KSEG
----

![kseg](/media/images/kseg-palette.png)

Designed for the two-button mouse, KSEG creates new points with the right mouse, and that is where geometry begins, with points! Everything else is a construction. Two points determine a line segment, a ray (watch the order specified) or an (infinite) line. The lines dutifully continue past the borders of the window as you change the scale. Suppose you specify three points (hold the shift key as you select three already constructed points), what do they specifiy geometrically speaking? The options that are available. Thus you can click the segment (you get a polygon), the lines (you get a tri-lateral), the arc (you get an arc). But why not circle. Because you should construct your circle from more primitive constructions. Baran is a minimimalist, in many ways.

GEX
---

![gex](/media/images/gex-palette.png)

The palette here is more self-consciously geometrical. Hvidsten is a geometer. He segregates the tools into *create*, *construct* and *transform*. He is not a minimalist, and offers shortcuts consistent with the category. It will seem that there is a redundancy here. The same figure can be drawn in many different ways. But only a constructed figure holds together when you *wiggle* it. That is, when the initial conditions, the created objects are moved around, does the rest of the figure follow or fall apart? In the latter is the case, you merely drew a figure, as you might have done in any of the above drawing programs, but you didn’t construct it!

Conclusion
==========

This has been the briefest, and also very subjective presentations of some common palettes. Now is the time to experiment!

Epilogue
========

So, what do you do when you have to use a less than optimal drawing palette? You do the best you can. And it helps to experiment. Because the vector graphics palette of Elluminate is very useful for being part of Elluminate we shall start with it.

Let us (provisionally) call a palette “geometrical” if it is possible to make the constructions possible with ruler and compass (and straightedge and protractor). Let’s consider Euclid’s postulates. The rubber-band line drawing tool is adequate for connecting two points (Postulate 1). But to extend the line in both directions (Postulate 2) is another matter. If you don’t want to redesign the figure going from the back to the front, you’ll need to extend the line by sight. It should be possible to “group” the original line and the prolongation so that they move together.

To draw a circle, given the center and a point (Postulate 3) is another matter entirely. Because the Bresenham algorithm for drawing a circle (or an ellipse) is independent of its center, you’ll need to use some ingenuity. I find that drawing a circle first, then then moving it so as to be centered approximately at the middle works well enough.

Angles and distances are not possible, it would seem. But a ruler and straightedge construction should be possible. The auxillary constructions needed could subsequently be deleted, one the figure is done. But moving or deleting a component remains a constant hazard. It might be a interesting project to determine just what constructions you do want to make. And the it would be series of exercises to actually implement them in the Whiteboard.


12 June 10, revised on 22 February 11
</article>
