Title: Des gestes discrets
Lang: fr
Date: 2012
Authors: Alexandre Leray; Stéphanie Vilayphiou
Translation: True
Slug: discrete_gestures

Des gestes discrets se concentre sur la manière dont les corps et les dispositifs s'informent en s'influencent mutuellement. Nous pensons qu'il est important de parler de la manière dont les outils numériques représentent les gestes physiques, et d'étudier comment l'acte de "rendre discret" affecte nos expériences.

Dans un système numérique, les valeurs ne peuvent être représentés que par des nombres. Ces nombres sont compris dans une plage fixe -- la résolution -- et ces valeurs sont dîtes "discrètes". Un gest physique est capturé par un capteur, dont les signaux analogiques sont filtrés par la logique des composants électriques puis transformés par une pile dense de logiciels. Lorsqu'on dessine à l'écran, il n'y a pas d'inscription immédiate sur le support/medium, au lieu de ça l'ordinateur performe les valeurs discrètes déjà contenus dans la mémoire de l'ordinateur.

La représentation d'un geste dans in outil numérique est le résultat de la traduction entre bits et chair, une boucle de rétroaction qui implique à la fois sentir et agir. Plutôt que de conceptualiser le logiciel et le materiel comme un "outil" --utilisé pour effectuer une action -- les textes de ce chapitre sont vus sous l'angle d'un "instrument" -- utilisé pour mesurer et sentir. où se situe la sensualité dans la relation Homme/machine? Jusqu'à quel point les ordinateurs aiguisent-ils les sens de quelqu'un? Au delà de la simulation du physique, peut-on sentir les mathematiques et les algorithmes au travail?

Les textes *Technique* de Gerrit Noordzij's et *M Formula* de William Dwiggin montrent tous les deux comment les outils médient les gestes, et comment ils participent aux traces que l'on produit. *Lessons Learned fomr Metafont* de Donald Knuth raconte l'aventure passionante de la tentative de remédier l'écriture calligraphique dans un système typographique.

*Graffity Markup Language* d'Evan Roth est un example de la traduction de gestes corporels spécifiques en un format qui permet ultérieurement à ces gestes d'être archivés et performés dans un médium completement différent ou à une autre échelle.

*Gesture of Writing* de Vilèm Flusser et *A Failed Love Affair with a Typewriter* de Friedrich Kittler montrent comment les outils peuvent faconner les corps par des contraintes physiques et culturelles.

*Essay on Drawing Palettes* de George Francis et *Drawing Curved* de Pierre Huyghebaert prennent la perspective des instruments qui agissent comme une sonde pour ressentir le monde autour de nous, qu'il soit physique ou mathématique.


* [*Technique*](technique.html)
* [*The M formula*](the-m-formula.html)
* [*Graffiti Markup Language*](graffiti-markup-language.html)
* [*Lessons Learned from Metafont*](lessons-learned-from-metafont.html)
* [*Gesture of Writing*](gesture-of-writing.html)
* [*A Failed Love Affair with a Typewriter*](a-failed-love-affair-with-a-typewriter.html)
* [*Essay on Drawing Palettes*](essay-on-drawing-palettes.html)
* [*Drawing curved*](drawing-curved.html)
