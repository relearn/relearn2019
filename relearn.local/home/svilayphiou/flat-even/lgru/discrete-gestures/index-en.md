Title: Discrete Gestures
Authors: Alexandre Leray; Stéphanie Vilayphiou
Lang: en
Date: 2012
Slug: discrete_gestures

Discrete Gestures focuses on how bodies and devices inform and influence each other. We feel it is important to talk about the way digital tools represent physical gestures, and investigate how the act of "rendering discrete" affects our experiences.

Inside a digital system, values can only be represented by numbers. These numbers fall within a fixed range—the resolution—and such values are known as “discrete”. A physical gesture is captured by a sensor, whose analogue signals are filtered through the logic of electronic components and then processed by a dense stack of software. When drawing on the screen, there is no immediate inscription on a medium, rather the computer performs discrete values already contained in the computers memory.

The representation of a gesture in a digital tool results from a translation between bits and flesh, a feedback loop that involves both sensing and acting. Rather than conceptualise software and hardware as a “tool”—used to perform an action—the texts of this chapter take the perspective of the “instrument”—used to measure and feel. Where is sensuality located in the human/machine relationship? To what extent do computers sharpen one's senses? Beyond the simulation of the physical, can one feel mathematics and algorithms at work?

Both Gerrit Noordzij’s [*Technique*](technique.html) and William Dwiggin’s [*M formula*](the-m-formula.html) show how tools mediate gestures, and how they participate in the traces one produces. Donald Knuth’s [*Lessons Learned from Metafont*](lessons-learned-from-metafont.html) tells the passionate adventure of the attempt to remediate calligraphic writing into a typographic system.

Evan Roth’s [*Graffiti Markup Language*](graffiti-markup-language.html) is an example of translating specific bodily movements into a format that subsequently allows for these gestures to be archived and performed in an altogether different medium or at another scale.

Vilèm Flusser’s [*Gesture of Writing*](gesture-of-writing.html) and Friedrich Kittler’s [*A Failed Love Affair with a Typewriter*](a-failed-love-affair-with-a-typewriter.html) show how tools can shape bodies through physical and cultural constraints.

George Francis’ [*Essay on Drawing Palettes*](essay-on-drawing-palettes.html) and Pierre Huyghebaert’s [*Drawing Curved*](drawing-curved.html) take the perspective of instruments that act as probes to experience the world around us, whether physical or mathematical.


Content
-------

* [*Technique*](technique.html)
* [*The M formula*](the-m-formula.html)
* [*Graffiti Markup Language*](graffiti-markup-language.html)
* [*Lessons Learned from Metafont*](lessons-learned-from-metafont.html)
* [*Gesture of Writing*](gesture-of-writing.html)
* [*A Failed Love Affair with a Typewriter*](a-failed-love-affair-with-a-typewriter.html)
* [*Essay on Drawing Palettes*](essay-on-drawing-palettes.html)
* [*Drawing curved*](drawing-curved.html)
