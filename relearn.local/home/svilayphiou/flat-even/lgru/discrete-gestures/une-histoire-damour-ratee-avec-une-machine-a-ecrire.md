Title: Une histoire d'amour ratée avec une machine à écrire
License: All rights reserved
Lang: fr
Date: 1986


<div markdown=true class="original">
"Nos outils d’écriture travaillent aussi nos pensées", a écrit Nietzsche [[1][19]]. "La technologie *est* retranchée dans notre histoire", a dit Heidegger. Mais l’un a écrit cette phrase à propos de la machine à écrire sur une machine à écrire quand l’autre décrivait (dans un allemand ancien magnifique) l’essence de la machine à écrire. Voilà pourquoi c’est Nietzsche qui fut à l’origine de la transvaluation des valeurs avec sa phrase philosophiquement scandaleuse sur la technologie des médias, "les hommes ne sont peut-être que des machines pensantes, écrivantes et parlantes". En 1882, aux êtres humains, à leurs pensées et à la figure de l’auteur se sont substitués terme à terme les deux sexes, le texte et l’appareillage de l’écriture à l’aveugle. Premier philosophe mécanisé, Nietzsche fut aussi le dernier. Le texte dactylographié avait pour nom, si l’on en croit la peinture de Klapheck, *Volonté de puissance* [[2][20]].

Nietzsche souffrait d’une myopie extrême, d’anisocorie [[3][21]] et de migraines (pour ne rien dire d’une possible paralysie évolutive). Un ophtalmologue de Francfort attesta que "l’œil droit [de Nietzsche] ne pouvait percevoir que des images dénaturées et difformes" ainsi que "des lettres presque impossibles à identifier", alors que le gauche, "en dépit de sa myopie" était encore capable en 1877 "de rendre compte d’images normales". Les maux de tête de Nietzsche apparurent dès lors comme "un symptôme dérivé" [[4][22]] et ses tentatives de philosopher au marteau comme la conséquence naturelle d’une "stimulation intense du site de la paroi préfrontale du troisième ventricule responsable de l’agressivité" [[5][23]]. Les penseurs de l’ère fondatrice des médias ne se sont pas contentés naturellement de passer de la philosophie à la physiologie sur un plan théorique ; leur système nerveux central les avait précédés.

Nietzsche se décrivit lui-même au fil du temps comme un quart aveugle, à moitié aveugle, puis aux trois-quarts aveugle (il revint à d’autres d’achever cette progression mathématique en suggérant l’étape suivante : le dérangement mental) [[6][24]]. Déchiffrer des lettres ou des notes de musique déformées au point de n’être plus reconnaissables lui devint insupportable au–delà de vingt minutes, écrire aussi. Autrement, Nietzsche n’aurait pas attribué le "style télégraphique", [[7][25]] qu’il développa en rédigeant le texte au titre évocateur *Le voyageur et son ombre*, à ses douleurs oculaires. Pour guider la cécité de cette ombre, il avait prévu d’acheter une machine à écrire dès 1879, année dite "de la cécité". [[8][26]]

Il le fit en 1881. Nietzsche "[prit] contact avec son inventeur, un Danois de Copenhague". [[9][27]] "Ma chère sœur, je connais la machine de Hansen assez bien, M. Hansen m’a écrit par deux fois et il m’a envoyé des échantillons, des dessins et des recommandations de professeurs de Copenhague à son sujet. C’est *celle-ci* que je veux (*pas* l’américaine, qui est trop lourde)". [[10][28]]

![Figure0][Figure0]

Dans la mesure où nos outils d’écriture travaillent aussi nos pensées, le choix de Nietzsche se fit selon des critères rigoureux et techniques. Voyageant continuellement entre l’Engadine et la Riviera, il se décida d’abord pour une machine à écrire de voyage, mais il fit aussi son choix en estropié qu’il était. À une époque où "très peu de gens seulement possédaient une machine à écrire, où il n’y avait pas de représentants de commerce [en Allemagne] et où elles se négociaient sous le manteau", [[11][29]] un homme isolé avait démontré sa maîtrise de la technologie (à tel point que les historiens américains de la machine à écrire omettent Nietzsche et Hansen.) [[12][30]]

Hans Rasmus Johann Malling Hansen (1835-1890), pasteur et directeur du Døvstummeinstitut [l’Institut des Sourds-Muets] de Copenhague, [[13][31]] développa sa *skrivekugle / writing ball / sphère écrivante* [[14][32]] après avoir observé que le langage des signes de ses patients sourds et muets était plus rapide que l’écriture à la main. La machine n’était pas destinée à "tenir compte des impératifs commerciaux" [[15][33]], mais à compenser des déficiences physiologiques et à accroître la vitesse de l’écriture (ce qui incita la Compagnie nordique du télégraphe à utiliser "de nombreuses sphères écrivantes pour la transcription des télégrammes qu’elle recevait".) [[16][34]] Cinquante-quatre touches sur tiges (mais sans levier encore) disposées concentriquement imprimaient, sur une feuille de papier relativement petite fixée à un cylindre, des lettres capitales, des chiffres et des signes au moyen d’un ruban coloré.

Selon Burghagen, cette disposition hémisphérique des touches avait l’avantage de permettre à "l’aveugle, pour qui la sphère à écrire avait été initialement conçue, d’apprendre à l’utiliser en un temps étonnamment court. Sur la surface d’une sphère, chaque position est parfaitement identifiable en fonction de sa position relative… Il est dès lors possible de se laisser guider par le seul sens du toucher, ce qui aurait été autrement plus difficile dans le cas d’un clavier plat." [[17][35]] C’est précisément en ces termes que des professeurs de Copenhague ont pu présenter la machine à un ex–professeur à moitié aveugle.

En 1865, Malling Hansen déposa un brevet, en 1867, il lança la production en série de sa machine à écrire, en 1872, les Allemands (et Nietzsche ?) en furent informés par le *Leipziger Illustrirte Zeitung*. [[18][36]] Enfin, en 1882, l’imprimerie de C. Ferslew à Copenhague associa femmes et sphères à écrire afin de remédier à cet inconvénient : "les compositrices étaient beaucoup plus préoccupées par le déchiffrement du texte manuscrit que par l’agencement du texte [imprimé] lui-même." [[19][37]] Le principe de McLuhan selon lequel la machine à écrire suscite "une attitude entièrement nouvelle à l’égard du mot écrit et imprimé" par ce qu’elle "fusionne composition et publication [[20][38]] " se réalisait pour la première fois (aujourd’hui, alors que les manuscrits vraiment rédigés à la main sont des raretés chez les éditeurs, "c’est toute l’industrie de l’impression qui, via le Linotype, repose sur la machine à écrire.") [[21][39]]

La même année et pour les mêmes raisons, Nietzsche se décida à l’achat. Pour 375 Reichmarks, frais d’envoi non compris, (Friedrich Nietzsche, lettre du 20-21 août 1881, in *Briefwechsel*, III, I, p.117.) Même un écrivain à moitié aveugle coqueluche des éditeurs était susceptible de produire "des documents aussi beaux et standardisés que des impressions". [[22][40]] "Après une semaine" d’usage de la machine à écrire, Nietzsche écrivit : "les yeux n’ont plus à faire leur travail" : [[23][41]] *l’écriture automatique* [[24][42]] venait d’être inventée, l’ombre du voyageur incarnée. En mars 1882, le *Berliner Tageblatt* annonçait :

> Le célèbre philosophe et écrivain [sic] Friedrich Nietzsche, que ses yeux malades avaient obligé à renoncer à son poste de professeur à Bâle voici trois ans, habite actuellement à Gènes et – exception faite de l’évolution de son mal vers une cécité presque complète – se porte mieux que jamais. À l’aide d’une machine à écrire, il a pu reprendre ses activités d’écriture et l’on peut donc s’attendre à un ouvrage dans la lignée des précédents. Tout le monde sait que son œuvre récente contraste résolument avec ses premiers et si remarquables travaux. [[25][43]]

En effet : Nietzsche, plus fier qu’aucun autre philosophe de la publicité faite autour de sa mécanisation, [[26][44]] passa des arguments aux aphorismes, des pensées aux mots d’esprit, de la rhétorique au style télégraphique. Et c’est précisément ce que signifie la phrase "nos outils d’écriture travaillent nos pensées". La sphère à écrire de Malling Hansen, avec ses difficultés d’utilisation, fit de Nietzsche un laconique. "Le célèbre philosophe et écrivain" se dépouilla de sa première qualité [la philosophie] pour mieux s’identifier à la seconde [la littérature]. Si la science et la pensée n’étaient accessibles (surtout vers la fin du XIXe siècle) qu’au prix de lectures infinies, alors c’était la cécité, et seulement la cécité qui pouvait "délivrer du livre ". [[27][45]]

Ces bonnes nouvelles de Nietzsche coïncidaient avec les premiers modèles de machines à écrire. Aucun des modèles qui ont précédé la grande innovation de *Underwood* en 1897 ne permettait un contrôle visuel immédiat sur le processus d’écriture. Pour lire le texte frappé, il fallait soulever les volets de la *Remington*, alors que dans le modèle de Malling Hansen – quoiqu’on ait pu en dire [[28][46]] – la disposition semi-circulaire des touches empêchait précisément d’avoir vue sur le papier. Mais même l’innovation de *Underwood* ne changea rien au fait que taper à la machine peut et doit demeurer une activité aveugle. Dans le jargon précis de l’ingénieur Angelo Beyerlen, sténographe royal du Württemberg et premier marchand de machines à écrire du Reich : "Lorsqu’on écrit à la main, l’œil doit constamment observer la ligne écrite et elle seule. Il doit assister la création de chaque signe, il doit mesurer, diriger, en bref, il doit guider la main à travers chaque mouvement". C’est le socle technologico-médiatique sur lequel est érigé la figure classique de l’auteur que la machine à écrire vient tout simplement liquider : "À l’inverse, après que l’on ait brièvement appuyé sur une touche, la machine à écrire crée à l’endroit prévu sur le papier une lettre complète, qui non seulement demeure intouchée par la main de l’écrivain mais se situe aussi en un endroit complètement distinct de celui où les mains travaillent". Avec les modèles *Underwood*, de la même façon, "le lieu où le signe d’après *arrive*" est "précisément… ce qui ne peut se voir". [[29][47]] Après une fraction de seconde, l’acte d’écrire cesse d’être cet acte de lecture qui advient par la grâce d’un sujet humain. Avec l’aide de machines aveugles, les êtres humains qu’ils soient aveugles ou non, acquièrent une compétence inédite dans l’histoire : *l’écriture automatique*.

Traduisant librement la maxime de Beyerlen selon laquelle "pour écrire, la visibilité n’a jamais été aussi inutile qu’aujourd’hui", [[30][48]] un psychologue expérimental américain (qui, en 1924, évalua "l’apprentissage de la compétence dactylographique » et qui obligeait ses patients à tenir des journaux intimes dactylographiés) nota des phrases documentaires à la manière d’André Breton :

> 24e jour. Les mains et les doigts deviennent manifestement plus souples et plus experts. Le changement qui s’opère maintenant, mise à part la souplesse croissante, réside dans l’apprentissage de la localisation des touches sans attendre de les voir. Autrement dit, il s’agit de localisation par position.
> 
> 25e jour. La localisation (musculaire, etc.), les associations de lettres et de mots tendent maintenant à l’automatisation.
> 
> 38e jour. Je me suis assez fréquemment surpris à frapper des lettres avant même de les voir consciemment. Il semble qu’elles ont atteint un niveau tout juste en deçà de celui de la conscience. [[31][49]]

"L’histoire drôle à propos de l’aveugle, etc." (c’est le titre de l’essai de Beyerlen) fut aussi celle du philosophe mécanisé. Les raisons pour lesquelles Nietzsche acheta une machine à écrire étaient très différentes de celles de ses quelques collègues qui écrivaient dans le registre du divertissement, tels que Twain, Lindau, Amytor, Hart, Nansen et d’autres. Ils espéraient quant à eux une vitesse plus grande et une production textuelle de masse ; le demi-aveugle, au contraire, passa de la philosophie à la littérature, de la relecture au geste pur, aveugle et intransitif de l’écriture. C’est pourquoi sa Malling Hansen rédigea la devise de toute la littérature intellectuelle moderne : "Finalement, lorsque mes yeux m’interdiront d’apprendre quoi que ce soit – et j’en suis presque là ! je serai encore capable de façonner un vers". [[32][50]]

1889 est généralement considérée comme l’année zéro de la littérature de machine à écrire, cette masse de documents à peine étudiés, l’année où Conan Doyle publia pour la première fois *Une affaire d’identité*. Sherlock Holmes de retour avait trouvé le moyen de prouver que les lettres d’amour dactylographiées (signature comprise) reçues par l’une des premières dactylographes de Londres, ostensiblement myope, étaient l’œuvre de son beau-père criminel impliqué dans un mariage frauduleux. Un anonymat criminel rendu possible par la machine qui incita Holmes à publier, dix-sept ans avant les professionnels de la police, une monographie intitulée *De la machine à écrire et de sa relation à la criminalité*. [[33][51]]

Malgré toute l’estime que l’on peut porter à Conan Doyle, on trouvera néanmoins un certain plaisir visuel et philologique à démontrer que la littérature de machine à écrire a commencé en 1882 – avec un poème de Friedrich Nietzsche que l’on pourrait tout à fait intituler *De la machine à écrire et de sa relation à la littérature*.

Dans ces vers frappés, c’est-à-dire littéralement forgés ou fabriqués, trois instances de l’écriture viennent à coïncider : l’appareillage, la chose et l’agent. En tout cas, l’auteur n’apparaît pas, il demeure à la lisière du vers : il est le destinataire, le lecteur supposé "utiliser" la "délicate [[34][53]] " sphère à écrire, celui que nous tenons pour Nietzsche en toute son ambiguïté. Notre outil d’écriture ne travaille pas seulement nos pensées, il est "une chose comme moi". L’écriture mécanisée et automatique réfute le phallocentrisme de la plume classique. Le destin du philosophe manipulé par ses doigts fins n’était pas le devenir auteur mais le devenir femme. Ainsi Nietzsche prit-il place aux côtés des jeunes dactylos chrétiennes de Remington et des compositrices de Malling Hansen à Copenhague.

Mais ce bonheur n’allait pas durer. La sphère à écrire humaine passa deux hivers à Gènes à tester et réparer son nouveau jouet favori si souvent détraqué, à s’en servir et composer avec lui. Puis le printemps sur la Riviera, avec ses pluies torrentielles y mit un terme. "Cette satanée écriture" écrivait Nietzsche, en se référant à lui-même comme toujours, "la machine à écrire est restée inutilisable depuis ma dernière carte ; car le temps est morne et nuageux, autrement dit, humide : et à chaque fois le ruban lui-même est *humide et collant*, de sorte que toutes les touches se coincent et l’écriture ne peut plus se voir *du tout*. Quand on y pense !!" [[35][54]]

* * *
[![][53]][53]
![Figure1][Figure1]
<figcaption>
**Fac-similé du poème *Malling Hansen* de Nietzsche, février-mars 1882.**:   « LA SPHERE A ECRIRE EST UNE CHOSE COMME MOI : FAITE / DE METAL / ET POURTANT VITE DETRAQUEE EN VOYAGE. / PATIENCE ET TACT SONT REQUIS EN ABONDANCE, / DE MEME QUE DES DOIGTS FINS, POUR NOUS UTILISER. »
</figcaption>

Et c’est donc la pluie sur Gènes qui lança et mit un terme à l’écriture moderne – une écriture réduite à la matérialité de son médium. "*A letter, a litter*", une lettre, un déchet, se moquait Joyce. La machine à écrire de Nietzsche ou le rêve de la fusion entre production et reproduction littéraires, au lieu de quoi fusionnèrent une fois encore cécité, invisibilité et bruit de fond, l’irréductible horizon des médias technologiques. En fin de compte, les lettres sur la page en arrivaient à ressembler à celles qui se formaient sur la rétine droite.

Mais Nietzsche n’abandonna pas. Dans l’une de ses dernières lettres dactylographiées, il évoque la possibilité de recourir à une prothèse technologico-médiatique et/ou à son substitut humain : le phonographe ou la secrétaire. "Cette machine", fit-il observer dans une autre configuration de l’appareillage de l’écriture et de l’écrivain, "est aussi délicate qu’un petit chien et source de beaucoup d’ennuis – elle procure aussi quelque divertissement. Maintenant, tout ce que mes amis ont à faire, c’est inventer une machine à lire : autrement je m’effondrerai et je ne serai plus capable de me procurer de nourritures intellectuelles suffisantes. Ou, plutôt : j’ai besoin d’une jeune personne intelligente et assez cultivée pour *travailler* avec moi. Je pourrais même envisager un mariage de deux années rien que pour ça." [[36][55]]

Quand sa machine s’effondra, Nietzsche redevint un homme. Mais c’était pour mieux saper la notion classique d’amour. Comme depuis toujours dans l’esprit des hommes et, depuis moins longtemps, dans l’esprit des femmes également, "une jeune personne" et "un mariage de deux années" convenaient tout à fait pour prendre la suite d’une histoire d’amour ratée avec une machine à écrire.

Et il en fut ainsi. Paul Rée, l’ami de Nietzsche qui avait déjà transporté la Malling Hansen à Gènes, se mit cette fois en quête de son substitut humain : quelqu’un qui put "aider" Nietzsche "dans ses études philosophiques par toutes sortes de travaux d’écritures, de copies et de résumés". [[37][56]] Mais au lieu de lui présenter un jeune homme intelligent, Rée lui présenta une jeune dame plutôt célèbre qui, "dans la perspective d’une *production* intellectuelle", requérait un "enseignant" : [[38][57]] Lou von Salomé.

*traduit de l’anglais par Gauthier Herrmann*

* * *

Tiré de Friedrich Kittler, *Gramophone, Film, Typewriter* (traduction de Geoffrey Winthrop-Young et Michael Wutz), Stanford, Standford University Press, 1999. pp. 200-208. 
Edition originale : Friedrich Kittler, *Grammophon Film Typewriter*, © Brinkmann & Bose, Berlin, 1986.

Tous droits réservés. Aucune autre utilisation, reproduction ou distribution est autorisée sans l’accord écrit préalable de l’éditeur.

 

[1][58]. Friedrich Nietzsche, lettre de la fin de février 1882, in F. Nietzsche *Briefwechsel : Kritische Gesamtausgabe, G. Colli et M. Montinari édit., Berlin, 1975 – 1984, III, 1, p. 172*.

[2][59]. Kittler fait allusion à un tableau de Konrad Klapheck, *La volonté de puissance*, 1959, qui figure une machine à écrire stylisée (note de l’éditeur).

[3][60]. Anisocorie : malformation oculaire caractérisée par des pupilles aux circonférences inégales *[ndt]*

[4][61]. Docteur Eiser, 1877, cité dans Joachim Fuchs, "Friedrich Nietzsches Augenleiden" in *Münchener Medizinische Wochenschrift 120*, 1978, p. 632.

[5][62]. *Ibid.*, p. 633.

[6][63]. D’après une observation de Martin Stingelin de Bâle.

[7][64]. Friedrich Nietzsche, lettre du 5 novembre 1879, in F. Nietzsche *Briefwechsel, op.cit.*, II, V, p.461.

[8][65]. Friedrich Nietzsche, lettre du 14 août 1882, *ibid*. II, V, p. 435.

[9][66]. Friedrich Nietzsche, lettre du 14 août 1882, *ibid.*, III, 1, p. 113.

[10][67]. Friedrich Nietzsche, lettre du 5 décembre 1881, *ibid.*, III, 1, p. 146.

[11][68]. Otto Burghagen, *Die Schreibmaschine. Illustrierte Beschreibung aller gangbaren Schreibmaschinen nebst gründlicher Anleitung zum Arbeiten auf sämlichen Systemen,* Hambourg, 1898, p. 6.

[12][69]. Apparemment contaminé, le biographe de Nietzsche corrige son héros en prétendant que "la machine à écrire a été “inventée”, c’est-à-dire développée, dix ans auparavant [sic] en Amérique". Pour couronner le tout, il écrit "Hansun" au lieu de "Hansen" (Kurtz Paul Janz, *Friedrich Nietzsche : Biographie*, 3 vols., Munich, 1978 – 79, II, p. 81, 95)

[13][70]. L’information suivante est tirée de Camillus Nyrop, "Malling Hansen" in *Dansk Biografisk Leksikon*, Povl Engelstoft édit., vol. 18, Copenhague, 1938, pp. 265 – 267.

[14][71]. En français dans le texte original

[15][72]. Otto Burghagen, *Die Schreibmaschine, op. cit.*, p. 6.

[16][73]. Voir Rolf Stümpel édit., *Vom Sekretär zur Sekretärin : Eine Austellung zur Geschichte der Schreibsmaschine und ihrer Bedeutung für den Beruf der Frau im Burö*, Gutenberg Museum, Mayence, 1985, p. 22. Il existait même des sphères à écrire pour le Morse (voir Ludwig Brauner, *Die Schreibmaschine in teknischer kultureller und wissenschaftlicher Bedeutung*, Sammlung geimeinnütziger Voträge, édit. Deutscher Verein zur Verbreitung gemeinnütziger Kenntnisse in Prag, Prague, 1925, p. 35-36).

[17][74]. Otto Burghagen, *Die Schreibmaschine, op. cit*., 1898, p. 120 (à propos de la machine de Hansen).

[18][75]. Voir Ernest Martin, *Die Schreibmashine und ihre Entwicklungsgeschichte*, 2e édition Papenheim, 1949, p. 571.

[19][76]. Rolf Stümpel édit., *Vom Sekretär zur Sekretärin, op. cit.*, p. 8.

[20][77]. Voir Marshall McLuhan, *Understanding Media*, New York, 1964, p. 260.

[21][78]. Bruce Bliven jr, *The Wonderful Writing Machine*, New York, 1954, p. 132.

[22][79]. Otto Burghagen, *Die Schreibmaschine, op. cit.*, p. 120 (à propos de la machine à écrire de Malling Hansen).

[23][80]. Friedrich Nietzsche, lettre du 20-21 août 1881, in *Briefwechsel*, III, I, p. 117.

[24][81]. *en français dans le texte original*

[25][82]. *Berliner Tageblatt*, mars 1882.

[26][83]. Voir Friedrich Nietzsche, lettre du 17 mars 1882, in *Briefwechsel*, III, 1, p. 180. "J’ai beaucoup aimé un article de la *Berliner Tageblatt* sur mon séjour à Gènes – même la machine à écrire y était mentionnée." Le philosophe mécanisé avait conservé l’article.

[27][84]. Friedrich Nietzsche, *"Ecce homo"*, in *On the Genalogy of Morals and Ecce Homo*, W. Kaufmann et R.J. Hollingdale édit., New York, 1967, p.287.

[28][85]. Voir par exemple Werner von Eye, *Kurzgefasste Geschichte der Schreibmaschine und des maschinensschreibens*, Berlin, 1958, p. 20.

[29][86]. Angelo Beyerlen cité dans Richard Herbertz, *"Zur Psychologie des maschinenschreibens", in Zeitschrift für angewandte psychologie 2 (1909)*, p. 559.

[30][87]. Angelo Beyerlen, *ibid*., 1909, p. 362.

[31][88]. Edgar Swift, "The Acquisition of Skill in Type-Writing : A Contribution to the Psychology of Learning" *in The Psychological Bulletin I* (1904), pp. 299, 300, 302. Voir aussi l’observation réflexive dans le roman de Christa Anita Brück, *Schicksale hinter Schreibmaschinen*, Berlin, 1930, p. 238 : "Je m’assois là, jour après jour, … à taper des lettres de fret, des lettres de fret, des lettres de fret. Après trois jours, c’était devenu un travail purement mécanique, une vague interaction entre le regard et les doigts, à laquelle la conscience ne participe pas activement."

[32][89]. Friedrich Nietzsche, lettre du 1er avril 1882, in F. Nietzsche, *Briefwechsel, op. cit.,* III, I, p. 180.

[33][90]. Voir sir Arthur Conan Doyle, *The Complete Sherlock Holmes*, Christopher Morley édit., Garden City, New York, 1930, p. 199.

[34][91]. Friedrich Nietzsche, lettre du 17 mars 1882, in F. Nietzsche *Briefwechsel*, III, I, p. 180.

[35][92]. Friedrich Nietzsche, lettre du 27 mars 1882, *ibid.,* p. 188.

[36][93]. Friedrich Nietzsche, lettre du 17 mars 1882, *ibid.,* p. 180. Sur la "machine à lire", voir Nietzsche, lettre du 21 décembre 1881, *ibid.,* p. 151.

[37][94]. Förster-Nietzsche, in Friedrich Nietzsche, *Briefwechsel*, Elisabeth Förster –Nietzsche et Peter Gast édit., Berlin, 1902-1909, V, 2, 488.

[38][95]. Friedrich Nietzsche, lettre du 18 juin 1882, in F. Nietzsche *Briefwechsel*, III, I, p. 206.

![][96]

Un extrait de Friedrich Kittler, *Gramophone, Film, Typewriter* (1986) proposé et présenté par Patricia Falguières  


  
 

![transparent][97] <a href="http://www.rosab.net/spip.php?page=imprimer&id\_article=16&lang=fr" onclick="window.open(this.href,'\_blank');return false;" onmouseover="print16.src='local/cache-gd2/354d039cd4edef9b024dd9505f916b53.gif'" onmouseout="print16.src='local/cache-gd2/2fa84969b3c882426362428d6bca0dd6.gif'" > <img src="local/cache-gd2/2fa84969b3c882426362428d6bca0dd6.gif" name="print16" alt="Imprimer cet article" /> </a> 

 

Publié en allemand, à Berlin, en 1986, *Gramophone, Film, Machine à écrire* de Friedrich Kittler est devenu un livre mythique. Aux côtés de *Du mode d’existence des objets techniques* de Gilbert Simondon (Paris 1969) ou du *Telephone Book* d’Avital Ronell (Lincoln 1989), il offre l’exemple rare, à la fin du XXe siècle, d’une incursion philosophique réussie dans le territoire de la technologie et des dispositifs d’enregistrement. Procédant d’une lecture radicale de Derrida, le Derrida de la *Grammatologie* (1967), et prenant très littéralement pour point de départ de son périple l’idée que nos outils travaillent notre pensée, qu’aucune technologie d’enregistrement ne saurait être neutre, que chacune procède, autant qu’elle le détermine, d’un sillage d’affects qui doivent trouver place dans l’histoire de la pensée, Kittler présente un éblouissant parcours philosophique à travers l’histoire des médias d’enregistrement qui nous mène du laboratoire de Thomas Edison (l’inventeur du gramophone) à Menlo Park, à l’état-major du général Ludendorff dans les tranchées de 14-18, des cartes postales envoyées par Kafka à Felice Bauer au déchiffrage du code Enigma par Turing pendant la seconde guerre mondiale…

Attentif à la matérialité des procès techniques qu’il analyse avec une précision rigoureuse comme peu de philosophes avant lui, Kittler réactive et enrichit les approches pionnières de la technologie qui avaient fleuri dans l’Allemagne de Weimar – approches généralement frappées d’amnésie aujourd’hui, celles de Walter Benjamin mises à part (pensons seulement à l’oubli de la théorie du montage cinématographique d’un auteur aussi essentiel que Bela Balazs par exemple). Kittler retrouve aussi, par son souci de la présence concrète, « en situation », des objets et des dispositifs, par son sens de la dramatisation qui leur donne une formidable épaisseur historique et poétique, quelque chose de cette fascination dont les avant-gardes des années vingt et trente, de Dziga Vertov ou Moholy–Nagy à Fritz Lang, avaient su faire le ressort de tant d’œuvres, transformant le gramophone, le banc de montage ou la machine à écrire en véritables protagonistes de leurs films, de leurs photos, de leurs livres : qu’il suffise de rappeler, à la fin du *Testament du docteur Mabuse* (1933), l’apparition de ce *deus ex machina* tout à coup dévoilé derrière son rideau d’illusion, la voix mécanique du chef de bande diabolique : le gramophone…

Patricia Falguières

 [1]: #
 [2]: local/cache-gd2/83e7858e72360862bed3892e991d5ea8.gif
 []: http://www.rosab.net/spip.php?page=rubrique&id_rubrique=9
 [4]: local/cache-gd2/b412b162e839370190478c36684f4802.gif
 [5]: local/cache-gd2/a5d1094a14bbe8541c22c47a9b80ac33.gif
 [6]: local/cache-gd2/c3862cb5d2ba1433200ba1082e003a8f.gif
 [7]: local/cache-gd2/37213819fce7f8f48fa71f956ce5e437.gif
 [8]: squelettes/rosab/img/edit/interface/logo_blanc_vect_rosab.png
 [9]: spip.php?action=cookie&url=spip.php?article47&var_lang=en
 []: http://www.rosab.net/spip.php?page=rubrique&id_rubrique=9&lang=fr
 [11]: local/cache-vignettes/L38xH16/9811670976c58d5f294fb3ea74a3de24-7f97d.png
 []: http://www.rosab.net/spip.php?page=newsletter&id_article=16
 [13]: http://www.rosab.net/spip.php?page=contact&id_article=16
 [14]: http://www.rosab.net/spip.php?page=ours&id_article=16
 []: http://www.bordeaux.fr/ebx/portals/ebx.portal?_nfpb=true&_pageLabel=pgSomRub11&classofcontent=sommaire&id=2010
 []: http://www.bordeaux.fr/ebx/portals/ebx.portal?_nfpb=true&_pageLabel=pgSomRub11&classofcontent=sommaire&id=2110
 []: http://www.bordeaux.fr/ebx/portals/ebx.portal?_nfpb=true&_pageLabel=pgSomRub11&classofcontent=sommaire&id=2911
 []: http://www.culture.fr/
 [19]: #nb1 "1. Friedrich Nietzsche, lettre de la fin de février 1882, in F. Nietzsche (...)"
 [20]: #nb2 "2. Kittler fait allusion à un tableau de Konrad Klapheck, La volonté de (...)"
 [21]: #nb3 "3. Anisocorie : malformation oculaire caractérisée par des pupilles aux (...)"
 [22]: #nb4 "4. Docteur Eiser, 1877, cité dans Joachim Fuchs, "Friedrich Nietzsches (...)"
 [23]: #nb5 "5. Ibid., p. 633."
 [24]: #nb6 "6. D'après une observation de Martin Stingelin de Bâle."
 [25]: #nb7 "7. Friedrich Nietzsche, lettre du 5 novembre 1879, in F. Nietzsche (...)"
 [26]: #nb8 "8. Friedrich Nietzsche, lettre du 14 août 1882, ibid. II, V, p. (...)"
 [27]: #nb9 "9. Friedrich Nietzsche, lettre du 14 août 1882, ibid., III, 1, p. (...)"
 [28]: #nb10 "10. Friedrich Nietzsche, lettre du 5 décembre 1881, ibid., III, 1, p. (...)"
 [29]: #nb11 "11. Otto Burghagen, Die Schreibmaschine. Illustrierte Beschreibung aller (...)"
 [30]: #nb12 "12. Apparemment contaminé, le biographe de Nietzsche corrige son héros en (...)"
 [31]: #nb13 "13. L'information suivante est tirée de Camillus Nyrop, "Malling Hansen" in (...)"
 [32]: #nb14 "14. En français dans le texte original"
 [33]: #nb15 "15. Otto Burghagen, Die Schreibmaschine, op. cit., p. 6."
 [34]: #nb16 "16. Voir Rolf Stümpel édit., Vom Sekretär zur Sekretärin : Eine Austellung zur (...)"
 [35]: #nb17 "17. Otto Burghagen, Die Schreibmaschine, op. cit., 1898, p. 120 (à propos de (...)"
 [36]: #nb18 "18. Voir Ernest Martin, Die Schreibmashine und ihre Entwicklungsgeschichte, 2e"
 [37]: #nb19 "19. Rolf Stümpel édit., Vom Sekretär zur Sekretärin, op. cit., p. (...)"
 [38]: #nb20 "20. Voir Marshall McLuhan, Understanding Media, New York, 1964, p. (...)"
 [39]: #nb21 "21. Bruce Bliven jr, The Wonderful Writing Machine, New York, 1954, p. (...)"
 [40]: #nb22 "22. Otto Burghagen, Die Schreibmaschine, op. cit., p. 120 (à propos de la (...)"
 [41]: #nb23 "23. Friedrich Nietzsche, lettre du 20-21 août 1881, in Briefwechsel, III, I, (...)"
 [42]: #nb24 "24. en français dans le texte original"
 [43]: #nb25 "25. Berliner Tageblatt, mars 1882."
 [44]: #nb26 "26. Voir Friedrich Nietzsche, lettre du 17 mars 1882, in Briefwechsel, III, (...)"
 [45]: #nb27 "27. Friedrich Nietzsche, "Ecce homo", in On the Genalogy of Morals and Ecce (...)"
 [46]: #nb28 "28. Voir par exemple Werner von Eye, Kurzgefasste Geschichte der (...)"
 [47]: #nb29 "29. Angelo Beyerlen cité dans Richard Herbertz, "Zur Psychologie des (...)"
 [48]: #nb30 "30. Angelo Beyerlen, ibid., 1909, p. 362."
 [49]: #nb31 "31. Edgar Swift, "The Acquisition of Skill in Type-Writing : A Contribution (...)"
 [50]: #nb32 "32. Friedrich Nietzsche, lettre du 1er avril 1882, in F. Nietzsche, (...)"
 [51]: #nb33 "33. Voir sir Arthur Conan Doyle, The Complete Sherlock Holmes, Christopher (...)"
 []: IMG/jpg/image-poemekittler_en-3.jpg "Fac-similé du poème <i>Malling Hansen</i> de Nietzsche, février-mars 1882."
 [53]: #nb34 "34. Friedrich Nietzsche, lettre du 17 mars 1882, in F. Nietzsche (...)"
 [54]: #nb35 "35. Friedrich Nietzsche, lettre du 27 mars 1882, ibid., p. 188."
 [55]: #nb36 "36. Friedrich Nietzsche, lettre du 17 mars 1882, ibid., p. 180. Sur la (...)"
 [56]: #nb37 "37. Förster-Nietzsche, in Friedrich Nietzsche, Briefwechsel, Elisabeth (...)"
 [57]: #nb38 "38. Friedrich Nietzsche, lettre du 18 juin 1882, in F. Nietzsche (...)"
 [58]: #nh1 "Notes 1"
 [59]: #nh2 "Notes 2"
 [60]: #nh3 "Notes 3"
 [61]: #nh4 "Notes 4"
 [62]: #nh5 "Notes 5"
 [63]: #nh6 "Notes 6"
 [64]: #nh7 "Notes 7"
 [65]: #nh8 "Notes 8"
 [66]: #nh9 "Notes 9"
 [67]: #nh10 "Notes 10"
 [68]: #nh11 "Notes 11"
 [69]: #nh12 "Notes 12"
 [70]: #nh13 "Notes 13"
 [71]: #nh14 "Notes 14"
 [72]: #nh15 "Notes 15"
 [73]: #nh16 "Notes 16"
 [74]: #nh17 "Notes 17"
 [75]: #nh18 "Notes 18"
 [76]: #nh19 "Notes 19"
 [77]: #nh20 "Notes 20"
 [78]: #nh21 "Notes 21"
 [79]: #nh22 "Notes 22"
 [80]: #nh23 "Notes 23"
 [81]: #nh24 "Notes 24"
 [82]: #nh25 "Notes 25"
 [83]: #nh26 "Notes 26"
 [84]: #nh27 "Notes 27"
 [85]: #nh28 "Notes 28"
 [86]: #nh29 "Notes 29"
 [87]: #nh30 "Notes 30"
 [88]: #nh31 "Notes 31"
 [89]: #nh32 "Notes 32"
 [90]: #nh33 "Notes 33"
 [91]: #nh34 "Notes 34"
 [92]: #nh35 "Notes 35"
 [93]: #nh36 "Notes 36"
 [94]: #nh37 "Notes 37"
 [95]: #nh38 "Notes 38"
 [96]: IMG/arton16.png
 [97]: squelettes/rosab/img/edit/interface/deplierhaut2.gif


[Figure0]: /media/images/theorie.jpg
[Figure1]: /media/images/hansen.jpg
</div>
