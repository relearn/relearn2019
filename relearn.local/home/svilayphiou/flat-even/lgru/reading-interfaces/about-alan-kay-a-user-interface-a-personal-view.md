Title: About: «A User Interface — A Personal View»
Lang: en
Date: 2012
Authors: Alexandre Leray; Stéphanie Vilayphiou

Alan Kay is one of the most important figure in the history of human/computer interaction. He and his team at Xerox PARC have conceived many tools and paradigms that shape today’s computing, including object oriented language, “windows, icons, menus, pointer” style of interaction, etc. In this text, he comes back on his own work and explains what were his motivations. 

Alan Kay’s work has been driven by his own understanding of the computer as not only a tool but a media, in which he sees the potential for social and individual transformations in a similar way that print did. But, for him, people won’t benefit from it unless computers become personal medias that everyone can read AND write. This is exactly the point of Florian Cramer except that he comes to different conclusions. 

If Kay invented the modern GUI as a proposal to enable literacy, Cramer argues for the use of the command-line over the GUI. To put it differently, Cramer takes the opposite path than Kay did. 
According to Cramer, the CLI enables a writerly approach to computing when the GUI is a “shrink-wrapped” readerly interface that “reinforce the division of user and developer”. The GUI separates the API (Application Programming Interface) and the UI (User Interface) whereas in the command-line the “UI is the API”. “The command-line interface provides functions, not applications; methods, not solutions”. 

Reading psychologists like Piaget or Bruner, Kay learned that our mind has a spectrum of cognitive facilities. As they grow up, children acquire new facilities (and lose some?), 3 stages being the *enactive* mentality (know where you are, manipulate), the *iconic* one (recognize, compare, configure, concrete) and the *symbolic* one (tie together long chains of reasoning, abstract). This is why “young children are not well equipped to do ‘standard’ symbolic mathematics until the age of 11 or 12, but that even very young children can do other kind of math”. 

Kay’s motto “Doing with images makes Symbols” expresses his wish to facilitate computer literacy acquirement by proposing a progressive approach to computing, from kinesthetic to symbolic. For Cramer, on the other hand, there is no salvation outside the symbolic approach to computing. He backs up this claim by saying that even Kay’s Squeak graphical programming environment still rely on textual programming, which seems to me a little bit unfair to Kay as the latter has never refuted its power nor tried to get rid of it. 

In the second part of his essay, Florian Cramer presents recursion as being perhaps the best aspect of CLI where there is only text. This is some kind of flat land where there is no hierarchy: commands, input, output, it’s all text. Programs can be chained and commands can even process themselves, data and commands mutating into each other. In GUIs this is not possible: photoshop can’t photoshop itself. 

There is here a critique of Object Oriented programming, a paradigm that most GUIs follow and that happens to be another Kay’s invention. For Cramer, GUI stucks the user in a predefined command set environment, but in Kay’s original plan -- before being “perverted” by Apple -- there was the possibility for the user to act on those commands by programming them themselves. 

If CLIs and programming open up new forms of creation (CLI is not text only, one can manipulate graphics, sound or moving images by text, see *Database Cinema* by Manovich for instance), it seems a little bit hazardous to discard visual direct manipulation (Cramer focuses here on poetry and literature). As Kay said, the different mindsets are complementary, and maybe one way to escape the duality of GUI/CLI is to think about ways of switching from one mode to the other. In this respect free software is perhaps more sensible compared to proprietary software. For instance Inkscape (free vector graphics authoring software) offers both the direct graphical manipulation of the object and the direct editing of the source code, plus scripting facilities. Relying on an XML-based open format, it is possible to go back and forth between the two. Since Michael Murtaugh was quoted in Cramer’s essay for his programmatic video montages, I’d like to bring another work of his. For a commission implying visualization of data coming from a cultural institution, he used Inkscape to create visuals “by hand” (by mouse). He then copied the relevant chunks to feed a loop in a php script to re-import the result back into Inkscape for further direct visual manipulations. This seems a beautiful way to take advantage of the qualities of the different approaches. 


*[CLI]: Command Line Interface
*[GUI]: Graphical User Interface
