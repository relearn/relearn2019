Type: article
Authors: Olia Lialina
Translator: Jean-François Caro
Title: Turing complete user
Date: 2012
Original: http://contemporary-home-computing.org/turing-complete-user/
License: all rights reserved, courtesy of the rights holder
Lang: fr

<div markdown=true class="original">
> « La moindre erreur peut corrompre toute la production de l’appareil. Identifier et corriger ces dysfonctionnements nécessitera généralement une intervention humaine intelligente. »

– John von Neumann, « First Draft of a Report on the EDVAC », 1945

  
> « Si vous ne pouvez pas bloguer, tweetez ! Si vous ne pouvez pas tweeter, likez ! »

– Kim Dotcom, [Mr. President][1], 2012


Invisible et très occupé
========================

Les ordinateurs sont en passe de devenir invisibles. Leur taille se réduit, et ils se cachent. Ils se tapissent sous la peau et se dissolvent dans le *cloud*. Nous observons ce phénomène comme on assiste à une éclipse solaire, mi terrifiés, mi bouleversés. Nous nous divisons en factions adverses et nous déchirons sur les avantages et les périls de l’Ubiquité. Mais quel que soit le camp que nous choisissons, nous ne pouvons qu’admettre l’importance d’un tel moment. 

Parallèlement à la disparition de l’ordinateur, quelque chose d’autre devient lui aussi invisible : l’utilisateur. Les utilisateurs disparaissent en tant que phénomène et que terme ; et soit ce développement passe inaperçu, soit on l’accepte en tant que progrès – en tant qu’évolution.

La notion d’Utilisateur Invisible est promue par d’influents designers d’interface utilisateur, en particulier Don Norman, gourou du design/intuitif et défenseur de longue date de l’informatique invisible. On peut d’ailleurs le considérer comme le père de ce concept. 

Les spécialistes du design interactif ont lu son Why Interfaces Don’t Work,
publié en 1990, dans lequel il s’interroge tout en répondant à sa propre
question : « Le vrai problème de l’interface, c’est qu’elle en est une. »
Alors que faire ? « Nous devons aider la tâche et non l’interface qui commande
cette tâche. L’ordinateur du futur devrait être invisible ! »[^1] Il aura fallu près de deux décennies, mais le futur a fini par arriver, il y a cinq ans, quand appuyer sur le bouton d’une souris a cessé d’être notre méthode d’entrée de données principale, et quand les technologies tactiles et multi-tactiles ont laissé entr’apercevoir notre nouvelle émancipation du matériel. Le confort des iProduits et les innovations dans le domaine de la réalité augmentée (devenue mobile), l’avènement des services mobiles portables, le bourgeonnement de toutes sortes de techniques de capture (des mouvements, du visage), et les avancées des technologies de projection, tout cela a contribué à effacer la frontière visible entre les dispositifs d’entrée et de sortie. Petit à petit, ces progrès transforment nos interactions avec les ordinateurs en des actions pré-informatiques ou, pour reprendre un terme cher aux designers d’interface, en des gestes et des mouvements « naturels ». Bien sûr, les ordinateurs sont toujours identifiables et localisables, mais ils ont cessé d’être des appareils devant lesquels on s’assoit. Les prédictions d’invisibilité se veulent si optimistes qu’en 2012, Apple s’est permis de reformuler la prophétie de Norman en la conjuguant au présent, et en l’associant à un produit de consommation spécifique : 

> Nous pensons que la technologie atteint son summum lorsqu’elle devient
> invisible, quand vous ne pensez qu’à ce que vous faites et non à l’appareil
> avec lequel vous le faites. […] L’iPad illustre parfaitement cette idée.
> C’est une fenêtre magique qui peut devenir tout ce que vous voulez qu’elle
> soit. […] C’est une expérience de la technologie plus intime que ce que le
> public [people] a connu jusqu’alors.[^2]

> Dans la dernière phrase, le terme « expérience » n’est pas utilisé par hasard, pas plus que ne l’est le mot « people » – « les gens », « les personnes ». 

> L’idée d’un ordinateur invisible, ou plus justement l’illusion de l’absence d’ordinateur, se voit réduite à néant si l’on continue de parler d’ « interfaces utilisateurs ». C’est pour cela que le design d’interfaces a progressivement été rebaptisé design d’expériences – expression dont l’objectif premier est de faire oublier l’existence des ordinateurs et des interfaces auprès des utilisateurs. Le design d’expériences vous laisse seul avec vos émotions à ressentir, vos objectifs à atteindre et vos tâches à effectuer. 

Ce champ est abrégé en « UXD » ; X renvoie à « expérience » et U toujours à
« User ». D’après Wikipédia, Don Norman inventa le terme UX en 1995. En 2012
cependant, les designers UX évitent de mentionner le mot en U dans leurs
articles et leurs allocutions, afin d’oublier ces boutons rudimentaires et ces
périphériques d’entrée révolus. Les utilisateurs, c’était bon pour les
interfaces. Les expériences, elles, sont destinées au PEUPLE ![^3]

En 2008, Don Norman cessa tout simplement de mentionner les utilisateurs en
tant que tels. Lors d’un événement parrainé par Adaptative Path, une compagnie
de design d’interface utilisateurs, Norman déclara : « Parmi les horribles
mots que nous employons, il y a le mot “utilisateurs”. Je pars en croisade
pour nous débarrasser de ce terme. Je préfère parler de « personnes »[^4]. Il savoura l’effet de sa formule sur l’auditoire avant d’ajouter, avec un sourire charmeur : « Nous concevons des produits pour des personnes, pas pour des utilisateurs. »

C’est un but louable, en effet, mais uniquement lorsqu’on l’envisage dans le contexte réduit du design d’interface. Ici, l’emploi du terme « personnes » souligne le besoin de suivre le paradigme centré sur l’utilisateur par opposition au paradigme favorisant l’implémentation. Dans ce contexte, l’emploi du terme « personnes » permet de rappeler aux développeurs de logiciels que l’utilisateur est un être humain dont il faut prendre compte dans les processus du design et de validation. 

Mais lorsqu’on l’interprète dans un contexte plus large, le désaveu du mot
« utilisateur » devient dangereux. L’existence de l’utilisateur est la
dernière trace qui nous rappelle la présence, qu’il soit visible ou non, de
l’ordinateur, d’un système programmé que l’on utilise. En 2012, ce fut au tour
du théoricien des nouveaux médias Lev Manovich de régler ses comptes avec le
terme. Il déclare sur son blog : « Par exemple, comment appelle-t-on une
personne qui interagit avec un média numérique ? Un utilisateur ? C’est un
mauvais terme. »[^5] 

Soit.  Je suis d’accord qu’avec toutes les grandes choses que nous permettent d’accomplir les nouveaux médias – différents modes d’initiation et de participation, les rôles multiples que l’on peut endosser – il est dommage de réduire cela à des « utilisateurs ». Cependant c’est bien de cela dont il s’agit. Blogueurs, artistes, diffuseurs de podcasts et même de trolls, tous demeurent des utilisateurs de systèmes qu’ils n’ont pas programmés. Ce sont donc tous les utilisateurs (et nous aussi). 

Nous devons protéger ce terme, car si l’on s’adresse à des personnes et non à
des utilisateurs, on éclipse l’existence de deux classes d’individus – les
développeurs et les utilisateurs. Et si nous perdons cette distinction, les
utilisateurs risquent de perdre leurs droits et la capacité de les défendre.
Le droit d’exiger de meilleurs programmes, la possibilité de choisir « aucun
des choix ci-dessus »[^6], le droit d’effacer ses fichiers, de les récupérer, d'échouer magistralement et, pour revenir au plus fondamental, le droit de voir son ordinateur.

**En d’autres termes : l’utilisateur invisible est un enjeu plus important que
l’ordinateur invisible.**

Que peut-on faire pour protéger le terme, la notion et l’existence des utilisateurs ? Quels contre-arguments puis-je avancer pour faire cesser la croisade de Norman et dissiper le scepticisme de Manovich ? Que savons-nous d’un utilisateur, si l’on oublie que c’est « un mauvais terme » ?

Nous le savons, les choses n’ont pas toujours été ainsi. Avant que les Vrais
Utilisateurs (ceux qui payent pour utiliser le système) ne deviennent des
« utilisateurs », les programmeurs et les hackers étaient fiers de se désigner
de la sorte. À leurs yeux, être un utilisateur constituait le meilleur rôle
que l’ont pouvait jouer dans le rapport avec son ordinateur.[^7] 

De plus, on aurait tort de penser que les ordinateurs et les développeurs sont
arrivés en premier, et que les utilisateurs ne sont entrés dans la danse que
plus tard. En réalité, c’est tout le contraire. À la naissance de l’ordinateur
personnel, l’utilisateur était au centre de l’attention. L’utilisateur ne
s’est pas développé en parallèle à l’ordinateur, mais avant lui. Songez à « As
We May Think » (1945) de Vannevar Bush, l’un des textes les plus influents de
la culture informatique. Bush passe plus de temps à décrire la personne
susceptible d’utiliser le Memex que le Memex en tant que tel. Il décrivait un
scientifique du futur, un surhomme. C’était à lui, l’utilisateur du Memex, et
non le Memex en tant que tel, que l’article était consacré.[^8] 

Vingt ans plus tard, Douglas Engelbart, l’inventeur du NLS, système
d’exploitation précurseur, et de l’hypertexte, évoquait son travail de
recherche sur l’enrichissement de l’intellect humain comme « amorce » – il
voulait dire par-là que les êtres humains, leur cerveau, et leur corps,
évolueraient avec la nouvelle technologie. Voici comment, dans son ouvrage
consacré à Douglas Engelbart, le sociologue français Thierry Bardini décrit
cette approche : « Engelbart ne se souciait pas uniquement de créer
l’ordinateur personnel. Il se souciait de créer de la personne qui pourrait
l’utiliser et accomplir efficacement des tâches de plus en plus
complexes. »[^9]

Et n’oublions pas le titre du célèbre texte de J. C. R. Licklider, celui qui
esquissa les principes des recherches du département Contrôle-Commande de
l’ARPA sur les systèmes en temps réel, qui donnèrent naissance à l’ordinateur
personnel/interactif – « Man-Computer Symbiosis » (1960).[^10]

Lorsque quinze ans plus tard l’ordinateur s’apprêtait à entrer sur le marché,
les développeurs tentèrent de définir son utilisateur modèle. À Xerox PARC,
Alan Kay et Adele Golberg introduisirent l’idée selon laquelle les enfants,
les artistes, les musiciens et d’autres constituaient des utilisateurs
potentiels de cette nouvelle technologie. Leur article « Personal Dynamic
Media », paru en 1977[^11], décrit d’importants principes relatifs aux
logiciels et au matériel associés à l’ordinateur personnel. Mais si nous y
voyons un texte révolutionnaire, c’est parce qu’il pose explicitement les
utilisateurs potentiels, distingués des développeurs, comme une composante
essentielle de ces technologies dynamiques. Lui aussi employé de Xerox, Tim
Mott (alias « le Père de la conception centrée utilisateur »), soumis à ses
collègues la métaphore de la secrétaire. L’image de la « dame à la machine à
écrire Royal »[^12] préfigura le design du Xerox Star, de l’Apple Lisa et d’autres postes de travail électroniques. On ne saurait donc nier que les utilisateurs existaient avant les ordinateurs, et qu’on les imaginait et les inventait – l’utilisateur est une vue de l’esprit. Consécutivement à leur construction fictionnelle, les utilisateurs continuèrent d’être imaginés et réinventés tout au long des années 1970, 1980, 1990 et durant le nouveau millénaire. Mais aussi raisonnables, courageux, futuristes ou primitifs que furent ces modèles d’utilisateurs, une constante subsiste. 

Permettez-moi d’évoquer un autre gourou de la conception orientée utilisateur, Alan Cooper. En 2007, alors que l’emploi du mot en U était toujours autorisé dans les sphères du design interactif, Cooper et ses collègues partagèrent leur secret dans « About Face, the Essentials of Interaction Design » :

> « Un designer interactif doit se dire que les utilisateurs débutants –
> surtout les débutants – sont à la fois très intelligents et très
> occupés. »[^13] 

Voilà un conseil bien sympathique (et il s’agit, soit dit en passant, de l’un
des ouvrages les plus réfléchis sur la conception d’interface) que l’on peut
grossièrement paraphraser par « hé, vous, les développeurs frontaux,
n’allez pas vous imaginer que les utilisateurs sont plus bêtes que vous, ils
sont simplement occupés ». Mais les choses ne s’arrêtent pas là. La seconde
partie de cette citation aborde un aspect vital : les utilisateurs sont des
personnes très occupées à *autre chose*. 

Alan Cooper n’a pas inventé ce paradigme, pas plus que Don Norman et sa
concentration sur la tâche au détriment de l’outil. Il est né dans les années
1970. Lorsqu’il dresse la liste des termes informatiques les plus importants
de cette époque, Ted Nelson mentionne les supposés systèmes « niveau
utilisateur », et déclare que ces derniers constituent « des systèmes conçus
pour ceux qui *ne pensent pas aux ordinateurs*, mais à la tâche ou à l’activité
pour laquelle l’ordinateur est censé les assister »[^14]. Quelques pages plus tôt, il annonçait : 


![][IMAGE1][^15]

Souvenons-nous que Ted Nelson prenait depuis toujours le parti des utilisateurs, et même des utilisateurs « naïfs » ;  l'apparente amertume de sa formule « simple utilisateur. » est finalement très positive.

La perte de contact entre les utilisateurs et leur ordinateur a débuté à Xerox PARC, avec des secrétaires, ainsi que des artistes et des musiciens. Et plus rien ne put l’arrêter. Les utilisateurs étaient considérés et adressés commercialement comme des individus dont le vrai travail, les vrais sentiments, les vrais intérêts, les vrais talents – tout ce qui importe – étaient extérieurs à leur interaction avec les ordinateurs personnels. En 2007, par exemple, lorsque la compagnie de logiciel Adobe, dont les produits règnent en maître sur ce que l’on appelle les « industries créatives », introduisit la version 3 de la  Creative Suite, elle diffusa des spots dans lesquels graphistes, vidéastes et autres vantaient ce nouveau pack. L’une d’entre elles, particulièrement intéressante, mettait en scène une webdesigner (ou une actrice campant une webdesigner) : enthousiaste, elle énumérait les capacités que lui offraient son nouveau DreamWeaver, affirmant qu’au bout du compte, « j’ai plus de temps pour faire ce qui me plaît le plus – être créative ». Le message d’Adobe était clair : moins vous pensez au code source, aux scripts, aux liens et au Web lui-même, plus serez un webdesigner créatif. Quel ramassis de mensonges. J’aimerais montrer cette vidéo à de jeunes étudiants en design à titre d’exemple, car elle illustre une profonde ignorance de l’essence de la profession.

Cette vidéo n’est plus en ligne, mais les publicités pour la  Creative Suite 6
ne sont guère différentes – on y aperçoit des concepteurs et des évangélistes
du design parler de libération, d’amélioration ou d’enrichissement de la
créativité pour la simple raison qu’il faut moins de clics pour accomplir
telle ou telle tâche.[^16]

Dans le livre *Program or Be Programmed*, Douglas Rushkoff décrit des phénomènes similaires :

> On considère le codage comme une tâche ennuyeuse, une compétence propre
> à la classe ouvrière, comme la maçonnerie, qu’on pourrait parfaitement faire
> sous-traiter dans un pays pauvre pendant que nos gosses jouent voire
> développent des jeux vidéo. On considère que que l’élaboration du scénario
> et  des personnages sont des tâches plus intéressantes, et que la
> programmation n’est qu’un travail ingrat qu’il vaudrait mieux déléguer à
> d’autres personnes, ailleurs dans le monde.[^17]


Rushkoff déclare que le codage n’est pas perçu comme une activité créative, mais il en va de même pour la manipulation de l’ordinateur en général. Celle-ci n’est pas considérée comme une tâche créative ou comme une « pensée mature ». 

Lorsqu’il décrit, dans « As We May Think », l’instrument idéal du scientifique du futur, Vannevar Bush affirme :

Il n’existe aucun substitut à la pensée mature. Mais la pensée créative et la
pensée essentiellement répétitive sont deux choses bien distinctes. Il existe,
et il peut exister pour la première de puissantes aides mécaniques.[^18]


Contrairement à cela, les utilisateurs tels que les imaginent les informaticiens, les développeurs de logiciels et les experts en utilisabilité sont ceux dont la tâche consiste à passer le moins de temps possible devant l’ordinateur, et à ne pas perdre leur temps à y songer. Ils exigent une application spécifique et isolée pour chaque « pensée répétitive » et, plus important encore, confient aux concepteurs d’applications le droit de définir les frontières entre le créatif et le répétitif, le mature et le primitif, le réel et le virtuel. 

À certaines périodes historiques, certains moments de la vie (et à de nombreuses reprises dans une seule journée !), cette approche s’avère pertinente ; il est des moments où la délégation et l’automation sont nécessaires et bienvenues. Mais à des moments où tous les aspects de la vie sont informatisés, il est impossible d’accepter l’utilisateur « occupé à autre chose » comme étant la norme. 

Considérons donc un autre modèle d’utilisateurs qui a évolué à l’insu de l’imagination des experts en utilisabilité.


![][IMAGE2]
<figcaption>
« Un savant du futur »
Illustration de « As We May Think » de Vannevar Bush, [version illustrée][2] tirée de *Life Magazine*, 1945.
</figcaption>

![][IMAGE3]
<figcaption>
Le blogueur/voyageur russe [Sergey Dolya][3], photographie de Mik Sazonov, 2012
</figcaption>




L’application générale, le « stupide » et l’universel
=====================================================

Dans « Why Interfaces Don’t Work », Don Norman critique vivement l’univers des ordinateurs et des interfaces visibles, et les utilisateurs qui y ont recours. Vers la conclusion de son texte, il suggère la source du problème :

> Nous sommes ici en partie parce que nous ne pouvons pas faire mieux avec la
> technologie actuelle, et en partie à cause d’un accident de l’histoire. Cet
> accident, c’est d’avoir adapté une technologie d’applications générales à
> des tâches extrêmement spécialisées tout en employant des outils
> généraux.[^19]


En décembre 2011, lors du 28e Chaos Communication Congress de Berlin, l’auteur
de science-fiction et journaliste Cory Doctorow donna une fabuleuse allocution
intitulée « The Coming War on General Computation »[^20]. Il y expliquait qu’il n’y avait qu’une seule manière de transformer définitivement les ordinateurs appareils dédiés, ces objets minuscules, invisibles, confortables et monofonctionnels tant loués par Don Norman : qu’ils soient remplis de logiciels espions. Il s’expliqua ainsi : 

> Aujourd’hui, certains départements marketing tiennent peu ou prou les propos
> suivants : “[…] Faites-nous un ordinateur qui ne lance pas tous les
> programmes à la fois, juste un programme consacré à une seule tâche
> spécifique, comme écouter des fichiers audio en streaming, router des
> paquets de données, ou de lire des jeux Xbox” […] Mais ce n’est pas ce que
> nous faisons lorsque nous transformons un ordinateur en appareil dédié. Nous
> ne fabriquons pas d’ordinateur qui se contente d’exécuter le programme
> « dédié » ; nous fabriquons un ordinateur capable d’exécuter tous les
> programmes, mais qui utilise une combinaison de rootkits[^21], de logiciels espions et de tatouages numériques pour empêcher l’utilisateur de reconnaître les processus activés, d’installer son propre programme, et d’annuler des processus qu’il ne souhaite pas. En d’autres termes, un appareil dédié n’est pas un ordinateur simplifié – c’est un ordinateur parfaitement fonctionnel rempli de logiciels espions dès sa sortie de l’emballage.


Par ordinateur totalement fonctionnel, Doctorow entend ordinateur généraliste,
ou, pour reprendre les termes du mathématicien américain John von Neumann dans
son « First Draft of a Report on the EDVAC » de 1945, le « système
informatique numérique polyvalent »[^22]. Dans son article, il esquissait les
principes de l’architecture de l’informatique numérique (l’ « architecture von
Neumann »), selon laquelle le matériel était séparé du logiciel, ce qui donna
naissance au concept dit de « programme stocké ». Au milieu des années 1940,
son impact fut révolutionnaire en ce sens qu’en « stockant électroniquement
les instructions, vous pouviez changer la fonction de l’ordinateur sans devoir
en changer les câblages »[^23].

Si nous n’avons plus besoin de souligner l’aspect relatif au recâblage aujourd’hui, mais l’idée qu’un seul ordinateur soit capable de tout faire demeure essentielle, tout comme le fait que c’est ce même ordinateur multifonctions qui se trouve derrière « tout », des terminaux stupides aux super-ordinateurs. 

L’intervention de Doctorow est une introduction parfaite pour qui désire se familiariser avec le sujet. Pour plus de détails sur la guerre contre l’informatique généraliste, on pourra consulter les écrits de Ted Nelson. Il fut le premier à attirer l’attention sur l’importance de la nature multifonctions de l’ordinateur. En 1974, dans son indispensable fanzine « Computer Lib », voué à expliquer le fonctionnement des ordinateurs au plus grand nombre, il écrivait en lettres capitales : 

> LES ORDINATEURS N’ONT NI NATURE NI CARACTÈRE
> Les ordinateurs, à la différence de tout autre équipement, sont parfaitement
> AVEUGLES. Et c’est la raison pour laquelle nous y avons projeté tant de
> visages différents.[^24] 


Parmi les grands textes de ce siècle figurent « The Future of the Internet and How to Stop It » (2008) de Jonathan Zittrain, et bien sûr « The Future of Ideas » de Lawrence Lessig. Ces deux auteurs se soucient davantage de l’architecture de l’Internet que de l’ordinateur en tant que tel, mais tous les deux étudient le principe de « bout à bout » qui est au cœur de l’Internet – c’est-à-dire l’absence d’intelligence (de contrôle) au sein du réseau. Le réseau demeure neutre ou « stupide », se contentant de livrer des paquets de données sans demander ce qu’ils contiennent. Il en va de même avec l’ordinateur de von Neumann – il se contente d’exécuter des programmes. 

Les travaux de Lessig, Zittrain et Doctorow expliquent avec brio pourquoi les
architectures de l’ordinateur comme du réseau ne sont ni des accidents
historiques ni « ce que désire la technologie »[^25]. Le réseau stupide et l’ordinateur généraliste sont les fruits de décisions délibérées de la part de leurs concepteurs. 

Pour Norman, les générations futures de concepteurs de logiciels et de matériel et leurs utilisateurs invisibles confrontés la technologie généraliste constituent à la fois un accident et un obstacle. Pour le reste d’entre nous, l’avènement et l’utilisation de la technologie généraliste sont au cœur des nouveaux médias, de la culture numérique et de la société de l’information (si tant est qu’une telle chose existe). Les ordinateurs généralistes et les réseaux stupides sont les valeurs charnières de notre temporalité informatique, et la force vive au gré de laquelle toutes les choses formidables et horribles qui arrivent aux individus qui travaillent et vivent avec des ordinateurs connectés. Ces décisions de design visionnaires doivent aujourd’hui être protégées, car il ne serait techniquement guère difficile de rendre les réseaux et les ordinateurs « intelligents », c’est à dire contrôlés. 

Quel est le rapport de tout cela avec les « utilisateurs » par opposition aux « gens », hormis le fait manifeste que seuls les utilisateurs qui se souviennent un minimum de leur ordinateur – au point de visionner la vidéo de Doctorow dans son intégralité – se battront pour ces valeurs ?

Je souhaite appliquer le concept de technologie généraliste aux utilisateurs
en renversant le discours et en détournant l’attention de la technologie au
profit de l’utilisateur façonné par trois décennies d’ajustements de la
    technologie généraliste à ses besoins : **l’utilisateur généraliste**.


Les utilisateurs généralistes savent écrire un article sur leur client de messagerie, mettre en page leur carte de visite sur Excel et se raser devant une webcam. Ils sont aussi capables de publier des photos en ligne sans passer par Flickr, tweeter sans Twitter, liker sans Facebook, apposer un cadre noir à une photo sans Instagram, ôter un cadre noir d’une photo sur Instagram, et même se lever à sept heures du matin sans avoir besoin d’une application « réveil à 07h00 ».

Peut-être faudrait-il les appeler utilisateurs universels, ou utilisateurs Turing-complets, en hommage à la machine universelle dite « de Turing » – la conception d’Alan Turing selon laquelle un ordinateur, avec suffisamment de temps et de mémoire, est capable d’accomplir n’importe quelle tâche logique. La vision et le travail de conception de Turing en 1936 annonçait et a sans doute influencé le « First Draft » de von Neumann ainsi que la Machine Polyvalente. 

Mais quel que soit le nom qu’on lui choisit, ce groupe désigne les utilisateurs capables d’arriver à leurs fins en dépit de la fonction principale d’une application ou d’un appareil. Ils trouveront un moyen d’arriver à leurs fins sans recourir à une application ou un programme spécialement conçus pour cela. L’utilisateur universel n’est pas un super-utilisateur, et encore moins un hacker. Il n’a rien d’exotique. 

On recense différents exemples et niveaux d’autonomie imaginés par les utilisateurs, mais nous sommes tous capables d’universalité. Il s’agit parfois d’un refus délibéré de ne pas déléguer des tâches spécifiques à un ordinateur. C’est parfois juste une habitude. La plupart du temps, il suffit un ou deux clics suffisent à dévoiler votre architecture généraliste. 

Par exemple, vous pouvez décider de ne pas utiliser Twitter et exhiber votre petit déjeuner devant le monde entier via votre site personnel. Vous pouvez utiliser LiveJournal comme un compte Twitter, vous pouvez utiliser Twitter comme un compte Twitter mais visiter les profils comme si vous visitiez une page d’accueil au lieu de les suivre.

Vous pouvez posséder deux comptes Twitter et vous connecter à l’un via Firefox, et à l’autre via Chrome. C’est la manière que j’emploie, et peu importe si je préfère m’y prendre ainsi. Peut-être que je ne connais pas cette application qui permet de gérer des comptes multiple, peut-être que je la connais mais que je ne l’aime pas, ou peut-être suis-je trop paresseuse pour l’installer. Quoi qu’il en soit, j’ai trouvé une façon de faire. Et vous aussi vous trouverez. 

La vision du monde d’un utilisateur universel (c’est une vision du monde et non un règlement ou une profession de foi) porte sur la liaison du matériel et du logiciel. Un tel comportement se situe aux antipodes de l’utilisateur « très occupé ». Ce type d’interaction rend l’utilisateur visible, et surtout visible à lui-même. Si vous souhaitez penser à cela en termes de design d’interfaces et d’UX, c’est l’expérience utilisateur ultime.

Cela signifie-t-il que l’industrie du logiciel devrait concevoir des programmes imparfaits ou se garder d’améliorer les outils existants que pour produire ce type d’expérience utilisateur ? Bien sûr que non ! Certains outils sont parfaits. Néanmoins, on pourrait repenser l’idée du programme parfait en prenant en compte le fait qu’il est employé par l’utilisateur généraliste, et en soulignant l’ambiguïté et l’implication de l’utilisateur.

Et heureusement, l’ambiguïté n’est pas si rare. Il existe des services en
ligne qui permettent aux utilisateurs d’employer ou d’ignorer certains
composants. Par exemple, les développeurs de Twitter n’ont pas pris des
mesures pour m’empêcher de parcourir les profils auxquels je ne suis pas
abonnée. Le réseau social hollandais Hyves permet à ses utilisateurs de
manipuler les images d’arrière-plan de sorte qu’ils n’ont pas besoin d’album
photo ou d’Instagram pour être satisfaits. <Blingee.com>, dont l’objectif principal est de permettre aux utilisateurs d’ajouter des paillettes sur leurs photos, autorise d’uploader toutes les images que l’on souhaite – qu’elles soient dépourvues de paillettes, et même sans animation. Le site se contente d’offrir les montages réalisés par l’utilisateur en retour.

Je peux aussi mentionner ici l’exemple extrême d’un service qui nourrit
l’universalité des utilisateurs – <myknet.org> – un réseau social dédié aux Premières nations canadiennes. Il est tellement « stupide » que les utilisateurs peuvent redéfinir la fonction de leur profil à chaque mise à jour. Aujourd’hui, il fonctionne comme un compte Twitter, hier, c’était une chaîne YouTube, et demain, ce sera peut-être une boutique en ligne. Qui se soucie de l’apparence  low-tech du site et que celui-ci paraisse avoir 17 ans de retard ? Il fonctionne !

Généralement, le WWW, excepté Facebook, est un environnement ouvert à toutes les interprétations.

Reste qu’il est ardu, à mes yeux, de trouver un site ou une application qui
convienne réellement aux utilisateurs, et qui considère leur présence comme
une partie intégrante du *workflow*. Cela peut paraître étrange, étant donné
que tout dans le Web 2.0 pousse le public à contribuer, et que le « design
émotionnel » est censé avoir pour vocation d’établir des liens personnels
entre ceux qui conçoivent les applications et ceux qui les achètent, mais je
parle de toute autre chose. Je parle d’une situation dans laquelle le
*workflow* d’une application a des vides que les utilisateurs peuvent combler, où la fluidité et l’uniformité sont interrompues, et où certains des liens finaux doivent être complétés par les utilisateurs. 

Je conclurai avec un exemple extrême, celui d’un projet anonyme (probablement celui d’un étudiant) : 

![][IMAGE4]
<figcaption>
« Google Maps + Google Video + Mashup – Claude Lelouch’s Rendezvous » :
</figcaption>


Ce projet date de 2006, au moment même de l’avènement du Web 2.0[^26], alors
que le mash-up constituait une forme artistique, culturelle et *mainstream*
très populaire. Les artistes célébraient de nouvelles convergences et un
brouillage des frontières entres différents logiciels. « Lelouch’s
Rendezvous » est un mash-up qui dispose sur la même page le célèbre
court-métrage « C’était un rendez-vous », et sa course en voiture à travers Paris, et un plan de la capitale vous permettant de suivre le véhicule dans le film et de le localiser simultanément sur Google Maps. Mais l’auteur n’est pas parvenu (ou peut-être n’a pas souhaité) à synchroniser la vidéo et le mouvement de la voiture sur la carte. Par conséquent, l’utilisateur se voit confier les instructions suivantes : « Cliquez sur “play” dans la vidéo. […] À la quatrième seconde, cliquez sur le bouton “Go !”. »

On demande à l’utilisateur de cliquer non pas sur un mais sur deux boutons ! Cela suggère que nous pouvons prendre les choses en main, que nous sommes en mesure d’effectuer une tâche au bon moment. L’auteur compte manifestement sur l’intelligence de l’utilisateur, et n’a jamais appris qu’il était « très occupé ». 

Le fait que le fichier vidéo original utilisé pour le mash-up ait été supprimé rend ce projet encore plus intéressant. Pour y participer, il vous faut chercher une autre version du film sur YouTube. J’en ai trouvé une, ce qui veut dire que vous y arriverez aussi. 

**Il n’y a rien qu’un utilisateur puisse faire qu’un autre ne puisse pas faire
avec suffisamment de temps et de respect. Les utilisateurs d’ordinateurs sont
des utilisateurs Turing-complets. **

\*\*\*  

Je ne saurais être plus d’accord avec Sherry Turkle, Douglas Rushkoff et
d’autres brillants esprits  lorsqu’ils déclarent qu’il nous faut apprendre à
programmer et à comprendre nos ordinateurs afin de ne pas être programmés et
« exiger de la transparence des autres systèmes »[^27]. Si l’approche de
l’éducation informatique à l’école passait de la manipulation d’applications
spécifiques à l’écriture d’applications, ce serait merveilleux. Mais hormis le
fait qu’une telle chose ne soit pas réaliste, j’ajouterais que ce n’est pas
non plus suffisant. Nous aurions tort de  penser devoir choisir entre
comprendre et utiliser les ordinateurs.[^28] 

Effort est nécessaire pour éduquer les utilisateurs sur eux-mêmes. Il faudrait instaurer une compréhension de ce qu’est l’utilisateur d’un « système informatique digital automatique polyvalent ».

Les utilisateurs généralistes ne sont ni un accident de l’histoire ni une
anomalie temporaire. Nous sommes les produits de la philosophie du « mieux
avec le pire » prêchée par UNIX, le principe de bout-à-bout d’Internet,
l’esprit « en construction » puis « beta » propre au Web. Tous ces motifs, qui
réclament de l’attention, de la clémence et de l’engagement ont façonné les
utilisateurs que nous sommes, et nous nous y adaptons perpétuellement,
improvisant tout en prenant le contrôle. Nous sommes les enfants de la
métaphore illusoire et maladroite du « bureau », nous savons ouvrir une porte
sans poignée.[^29]

Nous, utilisateurs généralistes – qui ne sommes ni hackers, ni « personnes » –, qui interrogeons, consciemment ou non, notre potentiel et celui de l’ordinateur, sommes les participants ultimes de la symbiose de l’homme et de l’ordinateur. S’il ne s’agit pas exactement du type de symbiose qu’envisageait Licklider, il n’en demeure pas moins bien réel. 


     Je tiens à remercier 
     Caitlin Jones pour avoir corrigé mon anglais et
     Dragan Espenschied pour avoir conçu le design de cette page. 
     Olia Lialina, octobre 2012

Appendice A : Sujets de l’interaction humain-ordinateur
=======================================================

<table id="HCI_table">
    <tbody>
        <tr>
            <th></th>
            <th>UX</th>
            <th>Web 2.0</th>
            <th>Cloud Computing</th>
            <th>*Gamification*</th>
        </tr>
        <tr>
            <th>ordinateur</th>
            <td>technologie</td>
            <td>réseau social</td>
            <td>*cloud*</td>
            <td>*epic win*</td>
        </tr>
        <tr>
            <th>interface utilisateur</th>
            <td>expérience</td>
            <td>boutton « soumettre »</td>
            <td>bouton « upload »</td>
            <td>*epic win*</td>
        </tr>
        <tr>
            <th>utilisateurs</th>
            <td>gens</td>
            <td>vous</td>
            <td>bouton « télécharger »</td>
            <td>gamers</td>
        </tr>
    </tbody>
</table>





Appendice B : 
=============

<table id="users_imagined_table">
    <tbody>
        <tr>
            <th>année</th>
            <th>source</th>
            <th>utilisateur imaginé</th>
            <th>revendication</th>
        </tr>
        <tr>
            <td>1945</td>
            <td>Vannevar Bush:
                <br> <a href="http://www.theatlantic.com/magazine/archive/1945/07/as-we-may-think/303881/?single_page=true">As we may think</a>
            </td>
            <td>
                <ul>
                    <li>Scientifique</li>
                </ul>
            </td>
            <td>
                « Nous pouvons désormais imaginer un futur chercheur dans son laboratoire. Il a les mains libres et il n’est pas ancré. Tout en se déplaçant et en observant, il photographie et commente. »
            </td>
        </tr>
        <tr>
            <td>1962</td>
            <td>Douglas&nbsp;Engelbart:
                <br> <a href="http://www.dougengelbart.org/pubs/augment-3906.html">Augmenting Human Intellect</a>
            </td>
            <td>
                <ul>
                    <li>Travailleur des connaissances</li>
                    <li>Travailleur intellectuel</li>
                    <li>Programmeur</li>
                </ul>
            </td>
            <td>« Considérez le domaine intellectuel d’une personne qui résout les problèmes créatifs […]. Ceux-ci […] pourraient très probablement contribuer, à travers des processus et des techniques spécialisés, à aider le travailleur généraliste dans le domaine intellectuel : la logique formelle – les mathématiques sous des formes variées, dont les statistiques – théorie de la décision – théorie du jeu – analyse du temps et du mouvement – recherche d’opération – théorie de la classification – théorie de la documentation – comptabilité des coûts, pour le temps, l’énergie ou l’argent – programmation dynamique – programmation informatique. »</td>
        </tr>
        <tr>
            <td>1975</td>
            <td>Tim&nbsp;Mott,
                <br>cité dans: <cite>Fumbling The Future</cite>, 1999, p.110
                <br>(<a href="http://books.google.de/books?id=ekSp-S3Uc58C&amp;lpg=PA110&amp;ots=fMmzlCFJAT&amp;pg=PA110">sur Google Books</a>)</td>
            <td>
                <ul>
                    <li>La dame à la machine à écrire Royal</li>
                </ul>
            </td>
            <td>
                « J’ai pris pour modèle une dame dans la fin de la cinquantaine, qui a publié toute sa vie, et a toujours utilisé une machine à écrire Royal. »
            </td>
        </tr>
        <tr>
            <td>1977</td>
            <td>Alan&nbsp;Kay:
                <br> <a href="http://www.newmediareader.com/book_samples/nmr-26-kay.pdf">Personal&nbsp;Dynamic&nbsp;Media</a> 
            </td>
            <td>
                <ul>
                    <li>Enfants</li>
                    <li>Artistes</li>
                    <li>Musiciens</li>Children
                </ul>
            </td>
            <td>
                « Un autre aspect intéressant était que les enfants avaient réellement besoin d’autant de pouvoir informatique que les adultes pour utiliser un système de temps partagé. […] Les enfants […] sont habitués à la peinture aux doigts, à l’aquarelle, à la télévision couleur, aux instruments de musique et aux disques. »
            </td>
        </tr>
        <tr>
            <td title="Article is covering the complete decade in research.">197x</td>
            <td>J.C.R.&nbsp;Licklider:
                <br>Some Reflections on Early History, 1988
                <br><em>in</em> Adele Goldberg (éd.), <cite>A History of Personal Workstations</cite>, 1988, p.119</td>
            <td>
                <ul>
                    <li>Utilisateurs réels</li>
                </ul>
            </td>
            <td>
« Les acheteurs d’ordinateurs, particulièrement d’ordinateurs personnels, ne consacreront que peu de temps à l’apprentissage. Ils insisteront pour l’utiliser le plus vite possible. »
            </td>
        </tr>
        <tr>
            <td>1974</td>
            <td>Ted Nelson:
                <br> <cite>Computer Lib/Dream Machines</cite>, édition revue, 1987, p.9</td>
            <td>
                <ul>
                    <li>Utilisateur naïf</li>
                </ul>
            </td>
            <td>
               « Personne ignorant le fonctionnement d’un ordinateur mais l’utilisant quand même. On appelle « systèmes d’utilisateurs naïfs » les systèmes conçus pour faciliter et clarifier l’utilisation pour ce type de personnes. <br />
Nous sommes tous des utilisateurs naïfs à un moment ou à un autre ; il ne faut pas en avoir honte. Cependant, certaines personnes  dans le domaine de l’informatique pensent le contraire. »
            </td>
        </tr>
        <tr>
            <td>1982</td>
            <td>Steven&nbsp;Lisberger:
                <br> <cite>TRON</cite> 
            </td>
            <td>
                <ul>
                    <li>Divinité</li>
                </ul>
            </td>
            <td>
                « – Croyez-vous en les utilisateurs ?
– Bien sûr. Si je n’ai pas d’utilisateur, qui m’aurait programmé ? »
                <p>(<a href="http://youtu.be/Ng1U4LMZz7Y?t=35s">voir le dialogue sur YouTube</a>)</p>
            </td>
        </tr>
        <tr>
            <td>1983</td>
            <td><cite>TIME Magazine</cite></td>
            <td>
                    <img src="/media/images/time-1983.jpg" height="264" width="211">
            </td>
            <td>
La « personnalité de l’année » est une machine : « Machine de l’année : l’ordinateur emménage »
            </td>
        </tr>
        <tr>
            <td>1993</td>
            <td>Eric&nbsp;S.&nbsp;Raymond: <a href="http://www.catb.org/~esr/jargon/html/S/September-that-never-ended.html">September that never ended</a>, Jargon File</td>
            <td>
                <ul>
                    <li>Néophytes désemparés </li>
                </ul>
            </td>
            <td>
« Le mois de septembre infini : pour la première fois depuis septembre 1993. L’un des rythmes saisonniers de Usenet était constitué de l’arrivée massive, en septembre, de néophytes désemparés qui, en faisant preuve d’un manque flagrant de nétiquette, se sont avérés être de véritables nuisibles. Le phénomène est lié aux jeunes étudiants entrant à l’université, obtenant leur premier compte Internet, et s’y plongeant sans prendre la peine d’apprendre les pratiques acceptables. »
            </td>
        </tr>
        <tr>
            <td>1996</td>
            <td>Eric S. Raymond:
                <br> <a href="http://www.amazon.com/gp/product/0262680920/ref=as_li_ss_tl?ie=UTF8&amp;camp=1789&amp;creative=390957&amp;creativeASIN=0262680920&amp;linkCode=as2&amp;tag=digitfolkl-20">The New Hacker’s Dictionary</a>
            </td>
            <td>
                <ul>
                    <li>hackers = prescripteurs</li>
                    <li>lamers = utilisateurs</li>
                </ul>
            </td>
            <td>
<p><em>Hacker</em> n. [...] Personne appréciant l’exploration des détails des systèmes programmables et les méthodes permettant de pousser leurs capacités au maximum, par opposition à la plupart des utilisateurs, qui préfèrent se contenter d’apprendre le minimum nécessaire. » – p. 223.</p>

<p><em>Lamer</em> n. […] Synonyme de <em>luser</em>, peu usité parmi les hackers, mais répandu chez les <em>warez d00dz</em>, les crackers et les <em>phreakers</em>. Contraire d’<em>élite</em>. Porte les mêmes connotations d’élitisme conscient de soi que l’emploi de <em>luser</em> chez les hackers. » – p. 275.</p>
            </td>
        </tr>
        <tr>
            <td>2006</td>
            <td><cite>TIME Magazine</cite></td>
            <td>YOU</td>
            <td>
                <a href="http://art.teleportacia.org/observation/papier-mache.html">
                    <img src="/media/images/time-2006.jpg" height="264" width="198">
                </a>
            </td>
        </tr>
        <tr>
            <td>2008</td>
            <td>Don Norman:
                <br>Allocution à <a href="http://www.youtube.com/watch?v=WgJcUHC3qJ8">UX Week 2008</a>
            </td>
            <td>Personnes</td>
            <td>« Je préfère parler de personnes. »</td>
        </tr>
        <tr>
            <td>2009</td>
            <td>Sir&nbsp;Tim&nbsp;Berners-Lee:
                <br> <a href="http://www.ted.com/talks/tim_berners_lee_on_the_next_web.html">The Next Web</a>,
                <br>TED Talk</td>
            <td>Eux</td>
            <td>
                « Il y a vingt ans […] j’ai inventé le World Wide Web. »
            </td>
        </tr>
        <tr>
            <td>2012</td>
            <td>Jack&nbsp;Dorsey, cofondateur de Twitter:
                <br> <a href="http://jacks.tumblr.com/post/33785796042/lets-reconsider-our-users">Let’s reconsider our “users”</a> 
            </td>
            <td>Client</td>
            <td>
« Si j’emploie une fois encore le mot « utilisateur », <em>collez-moi immédiatement une amende de 140 dollars</em>. »
            </td>
        </tr>
        <tr>
            <td>2013</td>
            <td>Bruce Tognazzini, gérant de Nielsen Normal Group :  
                <a href="http://asktog.com/atc/the-third-user/">The Third User</a> 
            </td>
            <td>Acheteur (<em>buyer</em>)</td>
            <td>
                    <img src="/media/images/buyer-naive-expert_1.png" />
            </td>
        </tr>
    </tbody>
</table>



[^1]: Don Norman, « Why Interfaces Don’t Work », *in* Brenda Laurel (éd.), *The
Art of Human-Computer Interface Design*, Addison-Wesley, 1990, p.218.

[^2]:	 Apple Inc., [Official Apple (New) iPad Trailer][4], 2012.


[^3]:	 Une autre force très vivace derrière l’occultation du terme
« utilisateur » provient des adeptes de la *Gamification*. Ceux-ci préfèrent désigner les utilisateurs sous le nom de « gamers ». Mais ceci est un autre sujet. 

[^4]:	 [Vidéo de l’intervention][5]. Voir aussi l’essai de Norman [« Words
matter »][6], paru en 2006 : « Les psychologues dépersonnalisent les patients qu’ils étudient en les rebaptisant “sujets”. Nous dépersonnalisons les individus que nous étudions en les rebaptisant “utilisateurs”. Ces deux termes sont péjoratifs. Ils nous éloignent de notre mission première : aider autrui. Donnons le pouvoir au peuple, pour redonner un sens à cette expression démodée. Peuple. Êtres humains. Voilà à qui s’adresse vraiment notre discipline. »


[^5]:	 Lev Manovich, [« How do you call a person who is interacting with
digital media? »][7], 2011.


[^6]:	 Emprunt au sous-titre « You May Always Choose None of the Above »,
tiré du chapitre « Choice », *in* Douglas Rushkoff, *Program or be Programmed*,
Soft Skull Press, 2010, p.46.

[^7]:	 « Le film *Tron* (1982) renferme l’appréciation la plus forte et la
définition la plus glorieuse de ce terme. […] La relation entre les
utilisateurs et les programmes est décrite comme très intime et personnelle,
de nature quasi religieuse, entre un créateur attentif et respectueux et une
progéniture responsable et dévouée. » – Olia Lialina et Dragan Espenschied, [Do
you believe in users?][8], *in* Digital Folklore, Merz Akademie, 2009.


[^8]:	 Vannevar Bush, « As we may think », version illustrée, *Life Magazine*, septembre 1945, [fac-similé][9])


[^9]:	 Thierry Bardini, *Bootstrapping : Douglas Engelbart, Coevolution, and
the Origins of Personal Computing*, Stanford University Press, 2000.

[^10]:	 J. C. R. Licklider, [« Man-Computer Symbiosis, IRE Transactions on Human
Factors »][10] *in* *Electronics*, vol. HFE-1, p.4-11, 1960.


[^11]:	 Alan Kay, [« Personal Dynamic Media »[11], 1977, *in* Noah Wardrip-Fruin et Nick Montfort (éds.), The New Media Reader, MIT Press, 2003.



[^12]: 	 Voir Douglas K. Smith et Robert C. Alexander, [*Fumbling The
Future: How Xerox Invented, then Ignored, the First Personal Computer*][12],
iUniverse, 1999, p.110. 


[^13]:	 Alan Cooper, Robert Reimann, David Cronin, About Face 3: The Essentials of Interaction Design, Wiley, 2007, p.45.

[^14]:	 Ted Nelson, *Computer Lib/Dream Machines*, édition revue, Tempus Books/Microsoft Press, 1987, p.9.

[^15]:	 « L’informatique a toujours été une affaire personnelle. Je veux dire
par-là que si vous ne vous y plongiez pas intensément, parfois avec toutes les
fibres de votre corps en éveil, ce que vous ne vous serviez pas d’un
ordinateur, vous n’étiez qu’un simple utilisateur. » Scan tiré de *Computer
Lib*, p.3.

[^16]:	 Voir par exemple les trailers pour [Adobe Creative Suite 6][13], 2012.


[^17]:	 Douglas Rushkoff, *op.cit*, p.131.

[^18]:	 Vannevar Bush, « As We May Think », *The
Atlantic Monthly*, juillet 1945, ([version HTML][14])


[^19]:	 Don Norman, “Why Interfaces Don’t Work”, *in* Brenda Laurel (éd.),
*The Art of Human-Computer Interface Design*, Addison-Wesley, 1990, p.218.

[^20]:	 [Retranscription][15], [vidéo][16].


[^21]:	 Outil de dissimulation de prise de contrôle. 

[^22]:	 John von Neumann, Introduction to [« The First Draft Report on the
EDVAC »][17], 1945.


[^23]:	 M. Mitchell Waldrop, *The Dream Machine: J.C.R. Licklider and the
Revolution that Made Computing Personal*, Penguin Books, 2002, p.62.

[^24]:	 Ted Nelson, *op.cit.*, p.37

[^25]:	 Voir Kevin Kelly, *What Technology Wants*, Viking Adult, 2010

[^26]:	 Le Web 2.0 était censé constituer une convergence totale entre les individus et la technologie, mais s’avéra une fois encore favoriser l’aliénation et la mise à part des utilisateurs et des développeurs. Les individus furent chassés des pages d’accueil artisanales pour intégrer les réseaux sociaux. 

[^27]:	 « La politique est un système, assurément complexe, de la même
manière. Si les gens comprennent quelque chose d’aussi compliqué qu’un
ordinateur, ils exigeront une meilleure compréhensionde comprendre davantage
d’autres choses. » – déclaration d’un interlocuteur, discutée dans Sherry
Turkle, *The Second Self: Computers and the Human Spirit*, The MIT Press, 2004, p.163.

[^28]:	 Au lieu d’enseigner la programmation, la plupart des écoles offrant
des cours d’informatique enseignent l’utilisation des programmes. […] Le
problème le plus saillant est que leur approche de l’informatique sera dans sa
totalité appréhendée du point de vue de l’utilisateur. » Douglas Rushkoff,
*op.cit.*, p.130.

[^29]:	 Les systèmes à manipulation directe, tels que le bureau Macintosh,
tente de jeter des ponts par-dessus le fossé de l’interface en représentant
l’univers informatique comme une collection d’objets directement analogues à
des objets du monde réel. Cependant, les fonctionnalités complexe et
foisonnantes des nouvelles applications – qui sont en accord avec les attentes
toujours plus importantes du public en termes de possibilités offertes par un
ordinateur – menace de nous pousser à la limite du bureau métaphorique. Le
pouvoir de l’ordinateur est enfermé derrière une porte sans poignée. » Brenda
Laurel, Computer as Theater, Addison-Wesley Professional, 1993, p.18.





[1]: http://www.youtube.com/watch?v=MokNvbiRqCM&t=3m38s
[2]: http://totalrecallbook.com/storage/As%20We%20May%20Think%20Vannevar%20Bush450910.pdf
[3]: http://sergeydolya.livejournal.com/510565.html
[4]:http://www.youtube.com/watch?v=RQieoqCLWDo
[5]: http://www.youtube.com/watch?v=WgJcUHC3qJ8
[6]: http://www.jnd.org/dn.mss/words_matter_talk_a.html
[7]: http://lab.softwarestudies.com/2011/07/how-do-you-call-person-who-is.html
[8]: http://contemporary-home-computing.org/turing-complete-user/
[9]: http://totalrecallbook.com/storage/As%20We%20May%20Think%20Vannevar%20Bush%20450910.pdf
[10]: http://groups.csail.mit.edu/medg/people/psz/Licklider.html
[11]: http://www.newmediareader.com/book_samples/nmr-26-kay.pdf
[12]: http://books.google.de/books?id=ekSp-S3Uc58C&lpg=PA110&ots=fMmzlCFJAT&pg=PA110
[13]:http://www.adobe.com/products/creativesuite/design-web-premium.html
[14]: http://www.theatlantic.com/magazine/archive/1945/07/as-we-may-think/303881/?single_page=true
[15]: http://joshuawise.com/28c3-transcript#the_coming_war_on_general_computation
[16]: http://www.youtube.com/watch?v=HUEvRyemKSg&feature=youtu.be
[17]:http://www.di.ens.fr/~pouzet/cours/systeme/bib/edvac.pdf


[IMAGE1]: /media/images/personal-computing.png
[IMAGE2]:  /media/images/scientist.png
[IMAGE3]:  /media/images/sergey-dolya.png
[IMAGE4]:  /media/images/mashup.png
</div>
