Type: article
Authors: Florian Cramer
Translator: Kamen Nedev
Title: $(echo echo) echo $(echo): Poética de la línea de comandos
Date: 2007
Original: http://www.digitalartistshandbook.org/node/13
License: CC-BY-NC-SA
Lang: es


<div markdown=true class="original">
Diseño
======

La mayoría de los argumentos a favor del uso de la línea de comandos frente al uso de la interfaz gráfica de usuario (GUI) están lastrados por un platonismo de administrador de sistemas. Una instrucción como «cp test.txt /mnt/disk» no está, en realidad, ni un milímetro más cerca de una hipotética «verdad» de la máquina que arrastrar un icono del archivo.txt con el ratón al símbolo de un disco duro montado. Y, aunque estuviera más cerca de la «verdad» ¿qué ganaríamos con ello?

La línea de comandos es, de por sí, una interfaz de usuario tan abstraída del kernel del sistema operativo como lo es la GUI. Mientras que el aspecto y usabilidad de «escritorio» de la IGU emula objetos de la vida real de un entorno de oficina analógico, la línea de comandos de Unix, BSD, Linux/GNU y Max OS X emula las máquinas de teletipo que servían como terminales de usuario en los primeros ordenadores Unix de principios de los 70. Este legado sigue vivo en la terminología de la «terminal virtual», y el archivo de dispositivo /dev/tty (de «teletipo») en sistemas operativos compatibles con Unix. Por tanto, tanto el uso de entornos gráficos como el de la línea de comandos son medios; capas de mediación en el bucle de retroalimentación cibernético entre humanos y máquinas, y pruebas de la veracidad de la afirmación de McLuhan de que el contenido de un nuevo medio es siempre un medio antiguo.

Ambas interfaces de usuario fueron diseñadas con objetivos diferentes: en el caso del teletipo de la línea de comandos, el de minimizar el esfuerzo y el gasto de papel, mientras que en el caso de la GUI fue el uso de analogías - en un caso ideal - autoexplicativas. La minimización de escritura y gasto de papel tenía la intención de evitar la redundancia, manteniendo la sintaxis y la respuesta de las instrucciones lo más sucintas y eficaces posibles. Por eso «cp» no se escribe «copy», «/usr/bin» no es «/Unix Special Resources/Binaries», y por eso también el resultado de la instrucción de copiar recibe como respuesta una línea en blanco, y por eso la instrucción puede repetirse simplemente pulsando la flecha hacia arriba y el retorno, o por qué volver a escribir «/mnt/disk» se puede evitar escribiendo simplemente «!$».

A su vez, la GUI reinventa el paradigma de los lenguajes universales de signos pictóricos, ideados por primera vez en las utopías educativas del Renacimiento, desde la Ciudad del Sol de Tommaso Campanella hasta el libro escolar ilustrado «Orbis Pictus» de Jan Amos Comenius. Los objetivos de sus diseños eran similares: «usabilidad», operación autoexplicativa a través de diferentes lenguas y culturas humanas, si es necesario a costa de la complejidad o la eficacia. En la operación de copiar un archivo, el acto de arrastrar es, en sentido estricto, redundante. Al significar nada más que la transferencia de «a» a «b», consigue exactamente lo mismo que el espacio entre las palabras - o, en términos técnicos: los argumentos - «test.txt» y «/mnt/disk», pero requiere una operación táctil mucho más complicada que pulsar la barra de espacio. Esta complicación es intencional, ya que la operación simula el común acto de arrastrar un objeto de la vida real a otro sitio. Pero, aun así, la analogía no es completamente intuitiva: en la vida real, arrastrar un objeto no lo copia. Y, con la evolución de las GUI desde Xerox Parc a través del primer Macintosh hacia paradigmas más contemporáneos de barras de herramientas, múltiples escritorios, integración con el navegador web, uno ya no puede sentar un analfabeto digital delante de una GUI y decirle que piense en ella como si fuera un escritorio de la vida real. Al margen de lo acertado de esta clase de analogías, el uso de la GUI es una técnica cultural tan construida y aprendida como lo es escribir instrucciones.

Por tanto, las categorías platónicas de la verdad no pueden evitarse del todo. Si bien la interfaz de línea de comandos también es una simulación - en concreto, la de una conversación telegráfica - sus expresiones alfanuméricas se traducen mucho más fácilmente en la operación numérica del ordenador, y viceversa. El lenguaje escrito puede usarse mucho más fácilmente para usar los ordenadores para aquello para lo que fueron construidos, para automatizar tareas de formateo: la operación «cp \*.txt /mnt/disk», que copia no sólo uno, sino todos los archivos de texto de un directorio a un disco montado sólo puede replicarse en una IGU encontrando, seleccionando y copiando a mano todos los archivos de texto, o usando una función de búsqueda o una función de script como una herramienta añadida. La extensión de la instrucción «for file in \*; do cp $file $file.bak; done» no puede replicarse en una IGU a no ser que esta función haya sido ya programada en ella. En la línea de comandos, «usar» se extiende naturalmente a «programar».

En una perspectiva más amplia, esto quiere decir que las aplicaciones GUI son, típicamente, simulaciones directas de una herramienta analógica: el procesamiento de textos emula las máquinas de escribir, Photoshop el cuarto oscuro de un laboratorio fotográfico, el software de publicación digital una mesa de maquetación, los editores de vídeo un estudio de vídeo, etc. El software sigue anclado en una escaleta de tareas tradicional. Las herramientas equivalentes en la línea de comados - por ejemplo: sed, grep, awk, sort, wc para el procesamiento de textos, ImageMagick para la manipulación de imágenes, groff, TeX, o XML para la maquetación de documentos, ffmpeg o MLT para el procesamiento de vídeo - transforman el proceso de trabajo tradicional de la misma manera que «cp \*.txt» transforma el concepto de copiar un documento. El diseñador Michael Murtaugh, por ejemplo, usa herramientas de línea de comandos para extraer automáticamente imágenes de una colección de archivos de vídeo para generar galerías o composites, un concepto que, sencillamente, excede el paradigma de un editor gráfico de vídeo, con su concepto predeterminado de qué supone la edición de vídeo.

Las implicaciones de esto llegan mucho más lejos de lo que pueda parecer a primera vista. La interfaz de línea de comandos provee funciones, no aplicaciones; métodos, no soluciones, o: nada más que un montón de plug-ins para ser promiscuamente enchufados unos en otros. La aplicación puede construirse, y la solución puede ser inventanda, por los propios usuarios. No es una interfaz empaquetada, o, - tomando prestados los términos de Roland Barthes - una interfaz «legible», sino «escribible». Según la distinción que hace Barthes entre literatura realista frente a literatura experimental, el texto legible se presenta lineal, compuesto suavemente, «como una despensa donde se guardan los significados, apilados, a salvo». [^1] En cambio, al reflejar la «pluralidad de puntos de entrada, la apertura de redes, la infinidad de lenguajes» [^2], el texto escribible intenta «convertir al lector no en consumidor, sino en productor del texto». [^3] Además de la caracterización que hace Umberto Eco de la línea de comandos como una interfaz iconoclásticamente «protestante», y la GUI como idolátricamente «católica», la GUI podría calificarse como Tolstoi, o como Toni Morrison, y la línea de comandos como Gertrude Stein, «Finnegans Wake», o la poesía L.A.N.G.U.A.G.E. de las interfaces informáticas. O, también, el paradigma Lego de un juguete auto-definido frente al paradigma Playmobil de un juguete predefinido.

La ironía está en que el paradigma Lego había sido el objetivo de diseño inicial de Alan Kay para la interfaz gráfica de usuario en Xerox PARC en los años 70. Basado en el lenguaje de programación Smalltalk, y haciendo uso de la programación orientada a objetos, aquella GUI debía permitir a los usuarios conectar sus propias aplicaciones a partir de módulos preexistentes. En sus formas populares en Mac OS, Windows, o KDE/Gnome/XFCE, las GUI nunca cumplieron esa promesa, y en cambio reforzaron la división entre usuarios y desarrolladores. Incluso excepciones marginales del propio sistema de Kay - que sigue vivo en el proyecto «Squeak», y los entornos de programación multimedia gráficos de Miller Puckette «Max» y «Pure Data» manifiestan la limitación de las GUI para operar también como interfaces de programación gráfica, ya que ambos siguen requiriendo programación textual a nivel de la sintaxis base. En términos de programación, la GUI refuerza la separación entre la UI (la interfaz de usuario), y la API (la interfaz de programación de aplicaciones), mientras que en la línea de comandos, la propia UI es la API. Alan Kay reconoce que «no sería sorprendente que el sistema visual fuese menos capaz en este área [el de la programación] que el mecanismo que resuelve oraciones sustantivas en el lenguaje natural. Aunque no sería justo decir que los 'lenguajes basados en iconos no funcionan' sólo porque nadie haya sido capaz de diseñar uno bueno, es probable que la explicación anterior esté cerca de la verdad». [^4]

Mutant
======

     CORE CORE bash bash CORE bash

     There are %d possibilities.  Do you really  
     wish to see them all? (y or n)

     SECONDS  
     SECONDS

     grep hurt mm grep terr mm grep these mm grep eyes grep eyes mm grep hands  
     mm grep terr mm > zz grep hurt mm >> zz grep nobody mm >> zz grep  
     important mm >> zz grep terror mm > z grep hurt mm >> zz grep these mm >>  
     zz grep sexy mm >> zz grep eyes mm >> zz grep terror mm > zz grep hurt mm  
     >> zz grep these mm >> zz grep sexy mm >> zz grep eyes mm >> zz grep sexy  
     mm >> zz grep hurt mm >> zz grep eyes mm grep hurt mm grep hands mm grep  
     terr mm > zz grep these mm >> zz grep nobody mm >> zz prof!

     if [ "x`tput kbs`" != "x" ]; then # We can't do this with "dumb" terminal  
     stty erase `tput kbs`

     DYNAMIC LINKER BUG!!!

Codework de Alan Sondheim, enviado a la lista de correo  «_arc.hive_» el 21 de julio de 2002

En una terminal, las instrucciones y los datos pasan a ser intercambiables. En la instrucción «echo date», «date» es el texto, o datos, que tiene que aparecer como resultado de la instrucción «echo». Pero si el resultado se vuelve a enviar al procesador de la línea de comandos (también conocido como shell) - «echo date -sh» - «date» se ejecuta como una instrucción en sí. Esto quiere decir: se pueden construir instrucciones de líneas de comandos que mezclan datos introducidos, texto, en nuevas instrucciones para ejecutarlas. Al contrario que en las GUI, hay recursividad en las interfaces de usuarios: las instrucciones pueden procesarse a sí mismas. Photoshop, en cambio, puede manipular sus propios diálogos gráficos, pero no puede después ejecutar estas mutaciones. Tal y como dice el programador y administrador de sistemas Thomas Scoville en su artículo de 1998 «The Elements of Style: UNIX As Literature», «Las herramientas de sistema de UNIX son como un kit de Lego para escritores. Tubos y filtros conectan una utilidad a la siguiente, y el texto fluye, invisible, entre ellas. Trabajar con una shell, con derivados de awk o lex, o con la herramienta set, es literalmente como bailar con palabras.» [^6]

En el net.art, «OSS» de jodi es lo más cercano a una hipotética GUI que se devora a sí misma, manipulando con Photoshop sus propios diálogos. El entorno de la línea de comandos de Unix/Linux/GNU es exactamente eso: un procesador de texto gigante en el que cada función - buscar, reemplazar, contar palabras, ordenar líneas - ha sido externalizada a un pequeño programa, cada uno representado por una instrucción de una sola palabra; palabras que pueden procesar palabras tanto como datos [correo electrónico, documentos de texto, páginas web, archivos de configuración, manuales de software, código fuente, por ejemplo] como las palabras en sí. Y, para más «shocks» culturales para la gente que no está acostumbrada a ella: con SSH o Telnet, cada línea de comandos pasa a ser «transparente a la red», esto es, cada instrucción puede ejecutarse tanto localmente como remotamente. «echo date - ssh user@somewhere.org» construye la instrucción en la máquina local, la ejecuta en la máquina remota somewhere.org, pero devuelve el resultado otra vez en la terminal local. Instrucciones y datos no solamente pueden mutar unos en otros, sino que instrucciones y datos en máquinas locales se pueden mezclar con los de máquinas remotas. El hecho de que ARPA - y, más tarde, Internet - fuera diseñado para computación distribuida se hace tangible a nivel microscópico en el espacio entre palabras, de una manera mucho más radical que en paradigmas monolíticos como «subir», o «aplicaciones web».

Con su hibridación de código y datos locales y remotos, la línea de comandos es el sueño húmedo de un poeta electrónico, «codewoker» o net.artista ASCII hecho realidad. Entre las «constricciones» poéticas inventadas por el grupo OULIPO, aquellas que son puramente sintácticas se pueden reproducir fácilmente en la línea de comandos. «POE», un programa diseñado a principios de los 90 por los poetas experimentales austriacos Franz Josef Czernin y Ferdinand Schmatz para asistir a los poetas con el análisis y construcción lingüísticos, acabó siendo, sin ninguna intención, una herramienta de texto Unix para DOS. En 1997, el poeta underground norteamericano ficus strangulensis hizo un llamamiento para la creación de un «sintetizador de texto», que es lo que la línea de comandos Unix realmente es. Por tanto, la «netwurker» mez breeze apunta, como principales influencias culturales en su trabajo net.poético «mezangelle» a «#unix [shelled + otherwise]», junto a «#LaTeX [+ LaTeX2e]», «#perl», «#python» and «#el concepto de ARGS [con un potencial todavía por realizar]». [^7] En la dirección contraria, desarrolladores de C obfuscado, poetas Perl y hackers como jaromil han mutado sus programas en net.poesía experimental.

Las mutaciones y recursiones en la línea de comandos ni son una coincidencia, ni son agujeros de seguridad, sino herramientas que los administradores de sistemas necesitan a diario. Tal y como dice Richard Stallman, fundador del proyecto GNU y primer desarrollador de los programas de línea de comandos GNU, «es un poco paradójico que puedas conseguir definir algo en términos de sí mismo, que esa definición tenga sentido. [...] El hecho de que [...] puedas definir en términos de sí mismo y definirlo bien así, esa es una parte fundamental de la programación.» [^8]

Cuando, como observa Thomas Scoville, el vocabulario de instrucción y sintaxis como la de Unix pasa a ser «algo natural», [^9] también se convierte en lenguaje conversacional, y la sintaxis se convierte en semántica no a través de algún tipo de inteligencia artificial, sino en términos pop culturales, como pasaba con las máquinas de escribir mutantes en la adaptación cinematográfica de «El almuerzo desnudo» que hizo David Cronenberg. Estas máquinas de escribir, literalmente «buggy», quizás sean el icono más potente del texto escribible. Y, aunque el software libre no se limite, de manera alguna, a las terminales - Unix empezó como software privativo - no dejar de ser esa cualidad escritora, y esa deconstrucción de la dicotomía usuario/consumidor, lo que hace que el software libre/de código abierto y la línea de comandos sean íntimos compañeros de cama.

[Este texto deliberadamente reutiliza y muta algunos pasajes de mi ensayo «Exe.cut[up]able Statements», publicado en el catálogo de ars electronica de 2003.]

## Referencias

[Bar75]
    Roland Barthes. S/Z. Hill and Wang, Nueva York, 1975.
   
[Ben97]
    David Bennahum. Interview with Richard Stallman, founder of the free software foundation. MEME, (2.04), 5 1997. http://www.ljudmila.org/nettime/zkp4/21.htm.
   
[Kay90]
    Alan Kay. User Interface: A Personal View. En Brenda Laurel, editor, The Art of Human-Computer Interface Design, págs. 191-207. Addison Wesley, Reading, Massachusetts, 1990.
   
[Sco98]
    Thomas Scoville. The Elements of Style: Unix as Literature, 1998. http://web.meganet.net/yeti/PCarticle.html.

--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

Notas a pie de página:

[^1]: [Bar75], p. 200
[^2]: [Bar75], p. 5
[^3]: [Bar75], p. 4
[^4]: [Kay90], p. 203
[^5]:  Codework de Alan Sondheim, enviado a la lista de correo  «_arc.hive_» el 21 de julio de 2002
[^6]:  Thomas Scoville, The Elements of Style: Unix As Literature, [Sco98]
[^7]:  Inédito en estos momentos, por publicar en la web http://www.cont3xt.net
[^8]: [Ben97]
[^9]: [Sco98], ibid.

</div>
