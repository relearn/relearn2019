Type: article
Authors: Femke Snelting; Alexandre Leray
Title: Where is design?
Date: 2012
License: Free Art Licence
Lang: en


<div markdown=true class="original">
From the ePub definition:    
> "The format is intended as a single format  that publishers and conversion houses can use in-house, as well as for distribution and sale."
 
Maybe we should better start this introduction by asking the question: "Where in a standard can we locate the design of a book?"
 
No, books are not just individually designed paper objects any more. They can be printed into different formats, but can also be electronic books such as ePubs, HTML pages, PDF, etc. In addition to being embodied in different output media, books are now edited in a discontinuous way where source material and modifications may come from different places, at different moments.
 
It means for book designers that their territory of work has moved away from the layout of a single, specific output to the design of "book systems"[^1] that can in turn produce multiple outputs/formats. Potentially, this means designers could become participants in a more complex but interesting situation where they need to negotiate medium and technology with a larger set of actors. The form of the book would actively be defined through an amalgam of processes happening at different moments and places **in the process of making it**, and not just be the unmutable result of historical events.
 
Instead, the state-of-the-art of book design practices is growing into a caricatural division. On one hand the designers allegedly want pixel perfect specificities, always longing for a time when books were still uniquely shaped single-master artefacts. On the other hand we have the publishing entrepreneur who demands generic design for speeding up the production of books by connecting stock management  interactively to content delivery and reflowable output for any device, at any time.
 
Brutal optimization
===================
 
Current standards tell a story of how they are attached to a certain type of production through their particular idea of optimization. Standards and file formats for this type of books are mainly developed and pushed for and by the industry. They reflect a technocratic approach to "books" and force that same approach onto ideas of authorship, distribution and ultimately designers' tools and practices.
 
Ultimately, it means separating  *form* and *content* as a principle to be able to split the container from the content and make form into style. Existing standards have separated the design process into *content* on one side and *styling* on the other; "*semantic* vs *presentation*". In XHTML for instance we have seen a refinement of the specifications to separate the "structure" (templates), the "content" (database) and the "styling" (CSS). While this division has useful implications in many ways, it is also problematic in the sense that it tends to divide labour and format tasks, leading in a weaker/poorer conception of design and authorship.
 
Federated publishing is actually a reality. Strangely, the way designers (can) work seems more formatted than ever, and we believe the technical infrastructure that we see emerging, is part of this problem. Only very rarely does our practice reflect the rich intermingling between form, content and medium that open up. How can we make the fact that design is embedded in chains of dependencies into an asset? 

How can we prevent that design/artistic practice lets itself be split apart from other realities?
 
Graceless degradation
=====================
 
Current standards such as ePub and other flavours of XML have a particular philosophy of optimization. The ability of reflowing text, is basically all that this means. This idea of optimal is rather "economic". It is typically based around one-column layouts; it does not think about relations between page size and resolution, between amount of text per page, margin size, etc. 
 
ePub is promoted having the concept of "graceful degradation" built in, the idea that the system can continue to function in the event of the failure of one or more components. This principle is often contrasted with "naive" systems that are designed in such a way that they would break down if only a single condition would not be met (and it is interesting to see how this mirrors the caricature of designer versus publisher).
 
More problematic than limited understandings of optimization might be the kind of technocratic hierarchies that are built into "fault tolerant" standards and file formats for cross-media publishing. 
 
Take for example moving content between devices that support Full Colour output, and those ones with one-bit output. In the logic of "graceful degradation", the lack of colour can only be seen as a fault, as an absence of colour. But the distinct graphic character of line-art can also be seen as a quality. The display of a "book" designed for line art on a colour device, can actually be considered as a significant degradation. With the idea of degradation comes a technocentric idea of hardware, where more is better, and larger is greater.
 
... graceful optimization?
==========================
 
> "Another criticism of EPUB revolves around the specification's lack of detail on  linking into, between, or within an EPUB book, as well as its lack of a  specification for annotation."
> -- <http://en.wikipedia.org/wiki/EPUB>
 
Current web technologies hint at the fact that much more intelligent ideas of relations between form and device, and maybe even between form, device and content are possible. Even more, that designers are involved in developing work in this area. 
<https://developer.mozilla.org/en-US/docs/CSS/Media_queries>
 
We are looking for a standard for "book systems" that first of all makes space for design. We are not interested in pixel perfectness but in a type of book design that engages with content at all levels (not only cosmetic). We accept that this might demand a rethinking of the full publishing infrastructure.
 
What we mean by contextual design is the understanding that if design is reflowable then its components need to flow together, not only at the level of text characters but at the level of layout as well. It would mean to reconsider the template not as a strict container but as a "context" that generates a set of parameters for attraction and repulsion between its components. An elastic visuality on the level of the template (specifying the  relation between the elements) but also at the level of the standard to open up the possibilities.
 
This work  on standards is an attempt to not work from this split but to understand design, production and publishing as deeply intertwingled, interlaced practices.

 

[^1]: A concept document for a file format or standard. See Ernst Cassirer <http://en.wikipedia.org/wiki/Ernst_Cassirer#Philosophy_of_symbolic_forms> standard for "books" as a symbolic form.
</div>
