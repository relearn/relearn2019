Type: article
Authors: Robin Kinross
Title: Unjustified text and the zero hour
Date: 1994
Book: Unjustified texts — perspectives on typography
Publisher: Walker Art Center
Pages: 286-301
License: All rights reserved


<div markdown=true class="original">
This text is composed in the conditional: this is what I would write, if there was time and space for something more. It is about something very big and general: a principle of design, in fact a more or less eternal problem that presents itself every time you set down or make manifest more than a few words. At the same time, I am interested in some small and marginal specific happenings: a few odd people, struggling with the circumstances in which they found themselves. The context is the “Stunde Null” of around 1945: the “zero hour” of much of continental Europe in ruins.[^1]

Both that long essay and this short one must start with this quotation from a German writing in Los Angeles, California, in 1944.

> *Pro domo nostra.* When during the last war—which like all others, seems peaceful in comparison to its successor—the symphony orchestras of many countries had their vociferous mouths stopped, Stravinsky wrote the *Histoire du Soldat* for a sparse, shock-maimed chamber ensemble. It turned out to be his best score, the only convincing surrealist manifesto, its convulsive, dreamlike compulsion imparting to music an inkling of negative truth. The pre-condition of the piece was poverty: it dismantled official culture so drastically because, denied access to the latter’s material goods, it also escaped the ostentation that is inimical to culture. There is here a pointer for intellectual production after the war, which has left behind in Europe a measure of destruction undreamed of even by the voids in that music. Progress and barbarism are today so matted together in mass culture that only barbaric asceticism towards the latter, and towards progress in technical means, could restore an unbarbaric condition. No work of art, no thought, has a chance of survival, unless it bear within it repudiation of false riches and high-class production, of colour films and television, millionaire’s magazines and Toscanini. The older media, not designed for mass-production, take on a new timeliness: that of exemption and of improvisation. They alone could outflank the united front of trusts and technology. In a world where books have long lost all likeness to books, the real book can no longer be one. If the invention of the printing press inaugurated the bourgeois era, the time is at hand for its repeal by the mimeograph, the only fitting, the unobtrusive means of dissemination.

This comes from Theodor Adorno’s most brilliant, most depressed book, *Minima moralia: reflections from damaged life*, in Edmund Jephcott’s phenomenal translation. [^2]

The shocking closing thought about “printing being replaced by the mimeograph” was, I am sure, inspired by the mode of publication that the Institut für Sozialforschung—the “Frankfurt School” of social-critical thought, then exiled in North America—had begun to adopt, out of necessity. For example, Walter Benjamin’s last piece, written in 1940 in France, the theses 'Über den Begriff der Geschichte' ('on the meaning/concept of history'), was first published in 1942, after Benjamin's death, by the Institut in Los Angeles. By the way, Benjamin's famous statement that 'there is no document of civilization which is not at the same time a document of barbarism'—the idea that Adorno picks up in this passage—occurs in those theses. To publish such a text in Los Angeles in 1942 one had to do it in samizdat: with the informal techniques of office-printing. So the words of Benjamin—the 'last European', for whom American exile was inconceivable—were typewritten onto stencils and spirit-duplicated. I have never seen this book or booklet, but I would bet that the right-hand edge of the text will have been ragged. The characters of the old manual typewriter were of a single width, and word-spaces had this same constant width. So the text had to be set without justification. [^3]

![Figure1-2][Figure1-2]
<figcaption>
1 & 2 Top: unjustified and justified setting of text, in monowidth characters. Set without word breaks and without “tracking”. Bottom: unjustified and justified setting of text, in proportional width characters. Set without word breaks; the justified setting uses “tracking”.
</figcaption>

Figure 1 shows the simple difference between justified and unjustified in single-width character setting. The folly of justification here should be evident. These examples were made on a computer with a DTP program, in which justification is as easy as unjustified setting. But that is a recent luxury. In figure 2, the same difference is shown with fully typographic, proportional-width characters. But even here, you see what may happen with justification, if you are not careful. You have to put the space somewhere: between words, even between letters. And that can make for an unwanted emphasis to words: a drawing-of-attention where you do not want it.

The English word “justify” has legal and theological senses, before its specialized typographic one, and it is the same in other European languages: to prove, to vindicate, to absolve. Justified text has been the norm ever since 1450, when Europe was first blessed with the procustean tools of the setting stick and the printing “forme” or frame. It is text made exact and formal. With unjustified or ragged or what is even sometimes called “free” setting, the spaces between words are equal, so the right edge has to be ragged, lines find their own length. Free, informal, self-determining: the social overtones are clear.

It is a good surmise to say that the increasing acceptance of unjustified text through this century followed from the fact that people became used to seeing it in typewritten text, in mimeography, and then in the small-offset printing that became widespread in Europe after 1945. Here I would want to find out more about this unglamorous and so far unwritten chapter of printing history: the development of office printing. One would have to look at typewriters. For example in 1941, as if in response to a growing need, IBM first put their successful Executive typewriter onto the market. This had characters of various widths. Justification was possible, although it took twice as long as unjustified.

![Figure3][Figure3]
<figcaption>
3 Max Bill, “Über Typografie”, *Schweizer Graphische Mitteilungen*, n°4, 1946. Page size of the magazine is 297×210mm. This reproduction is made from the uncut pages of an offprint.
</figcaption>

In the *Schweizer Graphischs Mitteilungen* of April 1946, the artist and designer Max Bill published an article entitled “ÜberTypografie” (“on typography”). Bill determined the typography of the article (figure 3). It is very cool: lowercase only, 8 point sanserif type printed in grey, unjustified setting without word-breaks, and illustrated by BiIl’s own work. But Bill’s words are not so cool. His starting point was a lecture by the typographer Jan Tschichold, given in Zurich in December 1945. In that lecture Tschichold had for the first time explained his turn away from modernism, of around 1937, towards a kind of enlightened traditional typography Tschichold then replied to Bill in the June issue of the SGM, in an article with the Goethe-like title of 'Glaube und Wirklichkeit' ('belief and reality'). [^4]

![Figure4][Figure4]
<figcaption>
4 Advertisement from: Franz Roh (ed.), *L. Moholy-Nagy: 60 Fotos*, Berlin: Klinkhardt & Biermann, 1930. The text-setting, in which the descriptive text is justified, would almost certainly have been specified by Jan Tschichold.
</figcaption>

This is one of the really fascinating debates in typography. It is of interest here because unjustified text appears in it as one of the markers dividing pre-war and post-war modernism. In the typography of heroic modernism—let us call it that—up to the mid-1930s, although the overall configuration may be asymmetric, columns of text are not set unjustified. It never happened at the Bauhaus, never in Tschichold’s work either. Figure 4 shows a modest sample of Tschichold’s design work from his mature 'new typography’ of the early 1930s: a publisher's advertisement for a book. The point to make here is that one might have expected the paragraph of text to be set unjustified. But Tschichold did not take his asymmetry as far as that.

![Figure5][Figure5]
<figcaption>
5 An example of Max Bill’s typography, shown in his 'Über Typografie'.
Bill notes in his caption: “machine-setting without rules, clearly arranged, avoidance of unnecessary gaps between word and number”.
Page size of original item: 230×155mm.
</figcaption>

In his article, Bill makes some play with the need to accept machine typesetting. He mentions in some of the captions to the reproduced examples of his work that the type was machine-set. Although Tschichold suggests that this is a glorification of something banal (machine production), in fact it is of interest if the whole of some of these pieces were set on a machine: because that would have required rather careful and clear instructions from the designer. Tschichold referred to Bill disparagingly as “an artist”, but if he could specify such things, then he was more than just an artist (figure 5).

Bill does not say this, but the implication of his article is that unjustified setting goes together with machine setting. ln fact it doesn’t. In the technology of that time—Monotype and Linotype—justified was as easy to produce as unjustified. Unjustified was easier in hand setting, as Tschichold pointed out.

This exchange of 1946 is full of very heavy accusations. Both participants had good reasons to feel that an awful lot was at stake in such apparently simple a thing as how you set text. Tschichold had been taken into custody in Munich in 1933, as a “cultural bolshevik”, and had then emigrated to Switzerland. Meanwhile Bill had been and remained a utopian socialist and a strong anti-fascist. Their argument was unresolved. But Max Bill’s text and his design work stood at the start of what was to become known as 'Swiss typography', over which he presided as one of the godfathers. From the late 1950s to the late 1960s, Swiss typography was really the hegemonic typography of design-conscious, modernizing Europe.

In March 1946, Max Bill received a letter from a typographer in London: Anthony Froshaug, then twenty-five years old, who had resisted call-up in the war. Froshaug’s mother was English, but his father was Norwegian. I suppose this was one factor in his feeling of not fitting well into English society; at least not well enough to join up in its army, not even to fight in a war against fascism. Nevertheless, he was inspired by a kind of utopian-anarchist-modernist hope. Froshaug was then running a cross-disciplinary small-publishing venture, Isomorph Ltd, and he hoped to entice Bill onto the list. The letter is quite smarmy: 'I have been very pleased and interested to see some of your typographical and exhibition work reproduced in various magazines over the last few years—not forgetting the Roth book. I see from the Swiss lists that we may one day hope to see copies of your book on Maillart, and other productions. I have also been informed by the Director of the Amsterdam Museum of Modern Art that you are starting the publication of a review in collaboration with Le Corbusier and others.’ [^5]

![Figure6][Figure6]
<figcaption>
6 Cover design by Willem Sandberg. *Open Oog*, n°1, 1946. Grey and red printing, 258×190mm.
</figcaption>

I could go on to describe the sporadic contact that developed between these two men. It led to an invitation to Froshaug to teach typography at the experiment in reconstruction that Max Bill became involved with: the Hochschule für Gestaltung Ulm. But I want to break off and explain this last reference in Froshaug’s letter. It is to Willem Sandberg, the director of the Stedelijk Museum in Amsterdam, who was just then collaborating on a new magazine with the title *Open Oog*: 'open eye'. The first of only two numbers is dated September 1946. Its cover (figure 6) is a very typical example of Sandberg's graphic work. The editorial group consisted of the designer Wim Brusse (who may have had some hand in the magazine's design), the art historian H.L.C. Jaffé, the architects J.P. Kloos, Gerrit Rietveld, and Mart Stam, and the designer and curator Willem Sandberg. The outer ring of associates did indeed include Max Bill and Le Corbusier.

![Figure7][Figure7]
<figcaption>
7 Editorial statement from *Open Oog*, n°1.
</figcaption>

*Open Oog* is a document of the pre-war avant-garde in Europe at that post-war moment: trying to regroup, to face the conditions of desolation, of zero, and see what they could do. Figure 7 shows the facing pages of the editorial statement. It is set unjustified, and in fact with lines broken according to sense, where possible. This statement recalls the 'period of creative intoxication’ before the war: 'New forms for new men and women… Typography abandons symmetric layout and assumes direction and movement; tries to guide the eye across the page. (Symmetry is equilibrium, stagnation.) Articles of use are produced exclusively with a view to their being used.’ The statement ends as follows: '1945. Now the storm is passing. Hitler is dead; but the tatters of his world image still conceal themselves in the thoughts of the misguided. Rare indeed are those who, standing wholly in the present, can assume the responsibilities and implications and face the naked truth of this grand and fiery epoch. Shall the War of 1939-1945 herald a new age? We pause, and scan the horizon of time—with open eye.’

Willem Sandberg was active in the Dutch resistance movement during the war, helping with sabotage activities and with illegal printing. He spent the last two years of the war in hiding. Some experimental typography from that time showed him trying out unjustified text: the way of setting that then became habitual with him. It is in Sandberg’s typography, I think more than any other designer's, that one sees the social meanings that can be found in unjustified setting. Sandberg's is a typography of openness, of imperfection, of informality, of equality, and so of dialogue. I still find it very moving. Although, having said that, one could make a whole series of typographer's qualifications: about word-breaks, hyphenation zones, and multi-column setting.

Sandberg did write a short defence of unjustified setting, published in 1952 in the English magazine *Typographica*, edited by Herbert Spencer.[^6] And in that year Spencer published his book *Design in business printing*, which includes arguments for unjustified, on purely pragmatic grounds. Here, with more time and space, one could make a detour into the English-British situation.

![Figure8][Figure8]
<figcaption>
8 Stefan Themerson, *Bayamus*, Editions Poetry London, 1949. 216×135mm.
</figcaption>
But I want to go back to Anthony Froshaug and to another person perching just then in the margins of English life. This was Stefan Themerson, a Polish writer, whose itinerary of exile from Warsaw had been Paris (1937) and then London in 1941. Themerson and Froshaug met in 1944, and began quite an intense dialogue over the next five years or so. A book by Themerson on experimental film was one of the many never-to-appear titles announced by Froshaug’s publishing imprint. Themerson and his wife, the painter Franciszka Themerson, went on to start their own publishing venture, the Gaberbocchus Press: in Chelsea, then Maida Vale, but absolutely outside the English literary scene. The strand of the Froshaug-Themerson dialogue and collaboration I want to touch on here is their work on what Themerson named ‘semantic poetry’. One of the examples in Themerson’s novel *Bayamus* shows this idea at work (figure 8). The 'Semantic sonata’, a long poem by Themerson to be printed by Froshaug, would have been the most concentrated expression of their collaboration. But it never appeared in this form. A prospectus was published in 1950.

![Figure9][Figure9]
<figcaption>
9 Cover of a folder designed by Max Bill, reproduced in his “Über Typografie”. Size of original item: 297×210mm.
</figcaption>

Semantic poetry as an attempt to strip language down, and at the same time to embody—or body forth—meaning: typographically. Lines had to be ragged, of course, but they were broken according to sense, and, further, internal vertical alignments were used. Max Bill sometimes used this principle too. In one of the pieces shown in his 1946 article, you can see elements configured around a central axis, but not symmetrically (figure 9). The placing of title lines, in the three language variants, is determined by the word “Architektur/​architecture”. In such things one can see the attempt to get beyond the merely formal: to reach meaning.

For Themerson, who was learning English, and Froshaug, who was helping him, “semantic” setting was partly just an outcome of the process of looking up words in several dictionaries (Polish, French, and English): they would write alternatives directly above one another. Partly also it was a response to the conditions of that time. As Themerson remembered later: “It was a refusal to be taken away from reality, a refusal to be taken for a ride, that made me "devise" (not to say "burst into") semantic poetry … at a time when political demagogues of all sorts were using oratory devices stolen from poets…” [^7]

Themerson is an unclassifiable writer. He sometimes fell in with English positivism, though he was too funny and also too serious to be trapped by it. Certainly that attitude of stripping away the metaphysical rubbish is of that moment: one sees it also, for example, in Susan Stebbing’s two war-time Pelican books, *Thinking to some purpose* and *Philosophy and the physicists*, in which she used the weapons of analytical philosophy to see off contemporary political and scientific rhetoric. [^8]

The reference to Anthony Froshaug can be rounded off by looking at the first page of a conspectus of his work as a printer, published in 1951 (see p.280 above). It is set unjustified, of course. There is a brief statement of what he was up to: going back to pre-industrial conditions and to the margin, but with a sophisticated awareness: “Equipment is deliberately minimal and simple in mechanical design: rather than behaviourist engines, it tends towards workshop tools & machine-tools enjoying maximal degrees of freedom, within which one individual may solve new problems in configuration.” Yes, it is as Adorno proposed: “The older media, not design for mass-production, take on a new timeliness: that of exemption and improvisation.”

The anti-metaphysical attitude I have been describing, as well as the results it produced, makes a clear contrast with the typographic and poetic avant-garde of around 1914: Futurism in Russia and Italy, Vorticism, Dada. It is true that those earlier people did try to shape text in response to meaning. But the meaning, the spirit, was quite different. The work of 1945 is cool, quiet, exact, without illusions. The inconceivable worst had actually happened (the death camps, the atomic bomb). One could only pause, strip away, to find out what really was there. Then there might be a chance of some decent reconstruction.

I have tried to point to the strong political mix of that time: despair and hope. There is even a grain of hope in the Adorno of 1944. I think we tend to forget this politics, especially in looking at what happened to the movement of reconstruction twenty years or so on from 1945. For example, there is a common perception of the Hochschule für Gestaltung Ulm as a sort of dried-out technocratic laboratory. But it had its origins in the reconstruction movement, and it was specifically a memorial to the “Weiße Rose” resistance group in Munich. Anthony Froshaug, who taught there, once described the HfG as an anarchist dream, and I think that description suggests a truth.

To sum up: I do think it is interesting and significant that it was just at that moment, around 1945, and not earlier, that the ancient principle of unjustified setting was rediscovered. This was done by a handful of quite marginal people, as well as by the pervasive presence of the typewriter. It would be interesting to trace how the experiments of 1945 got taken into the broad stream of reconstruction and then prosperity, over the twenty or so years that followed.

*Information Design Journal*, vol. 7, n°3, 1994

> This was a lecture given at the conference on “Design & Reconstruction in Postwar Europe”, held at the Victoria & Albert Museum, London, January 1994; the text was then published in IDJ, largely as spoken.


[^1]: The fact that we have to use the German term is significant: the
British managed to avoid it. Some weeks after this lecture was given,
the sense of the “Stunde Null” in 1945 was very well explained by Neal
Ascherson in his *Independent on Sunday* column (13 February 1994):
> 'The cities stood in ruins or were burning. The roads were jammed
> with lost human beings, men in every imaginable uniform and families
> in rags, pressing this way and that as the Allied aircraft plunged down
> on them with rockets and cannon. Abandoned trains stood in rainy
> fields, their locomotives still smoking, corpses humped across rails already turning crimson with rust. No newspapers or radio remained
> to give news or instructions. The roofless factories were silent; the
> shops gutted. Bandit gangs, some of them still in concentration-camp
> clothes, ranged the land murdering and plundering. The Seven Seals
> had been opened.
> This condition, which spread far beyond Germany, is called by the
> Germans “Stunde Null". That means much more than “zero hour". It
> means the moment at which the world has ended—but also the moment at which the next world is conceived.'

[^2]: Theodor Adorno, *Minima moralia: reflections from damaged life*, New
Left Books, pp. 50-1. [Original publication: Frankfurt a.M: Suhrkamp,
1951.] *Minima moralia* is a sequence of short, aphoristic meditations,
divided into three parts. This passage, section 30, comes from the first
part, dated 1944.

[^3]: The term “unjustified” is used here as the one most widely understood
by lay people. Among several alternatives, “fixed word space setting”, is
a more exact description. The topic has been thoroughly discussed by
Paul Stiff in “The end of the line: a survey of unjustified typography” (*Information Design Journal*, vol. 8, n°2, 1996, pp. 125-52).

[^4]: These articles by Max Bill and Jan Tschichold have now been published in English in *Typography Papers*, n°4, 2000.

[^5]: Letter dated 9.3.1946, now published in full in: *Anthony Froshaug: Documents of a life*, Hyphen Press, 2000, p. 143.

[^6]: W.J.H.B. Sandberg, “Must line-length be uniform?”, *Typographica* (first-series), n°5, [1952], pp. 30-1.

[^7]: Stefan Themerson, *On semantic poetry*, Gaberbocchus, 1975, p. 16.

[^8]: Susan Stebbing, *Thinking to some purpose*, and *Philosophy and the physicists*, Harmondsworth: Penguin Books, 1939 and 1945 respectively.


[Figure1-2]: /media/images/kinross-figure1-2.jpg.gif
[Figure3]: /media/images/kinross-figure3.jpg.gif
[Figure4]: /media/images/kinross-figure4.jpg.gif
[Figure5]: /media/images/kinross-figure5.jpg.gif
[Figure6]: /media/images/kinross-figure6.jpg.gif
[Figure7]: /media/images/kinross-figure7.jpg.gif
[Figure8]: /media/images/kinross-figure8.jpg.gif
[Figure9]: /media/images/kinross-figure9.jpg.gif
</div>
