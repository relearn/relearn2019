Title: Une myriade d'outils de composition 
Date: 2012
Authors: Alexandre Leray; Stéphanie Vilayphiou

Si le logiciel est un object fonctionnel, c'est aussi un objet culturel qui incarne la vision de son créateur sur la façon dont les choses doivent se faire. Par ailleur, cette vision est toujours inscrite dans une lignée d'outils et de pratiques antérieurs.

Lorsqu'on démarre un logiciel de mise en page, on engage un dialogue avec 50 ans d'histoire moderne de l'informatique, et avec plus de 500 ans de l'histoire de l'imprimerie, s'étandant à l'histoire de l'écriture et du signe. Même si des gourous moidernistes comme Jan Tschicold et Joost Hochuli on prechés des règles de compositions universelles, la pratique du design montre que celles-ci ne peuvent jamais être valide pour chaque type de livres, de culture ou même de contenu. Les détails tels que comment effectuer des césures, ou comment les blocs de textes sont construits sont tous des résultats de décisions spécifiques culturelles et contextuelles. Les textes dans cette section reflètent donc sur cette question: comment les aspects sociaux et techniques influencent et donnent-ils sens à la manière dont quelqu'un met en page et compose du texte?

*Ordinateur et composition automatique* de Maurice Girod explique comment la composition automatique de texte (à travers les césures et la justification) ne peut-être valide pour chaque language, et même pour les écrits de différents éditeurs publiants dans le même language.

ALors que Girod parle de justification de texte par le prisme de la technologie et du language, Robin Kinross, dans *Unjustified text and the zero hour*, retrace son histoire à travers innovation technologique et les contextes sociaux avec une approche culturelle.

Le texte de Alexandre Leray et Femke Snelting *Books with and Attitude* parle du design d'un contenu pour de multiple supports. Il critique l'appauvrissement qui résulte de la décision visant à un affichage correct qui correspond au plus petit dénominateur commun. Plutôt que d'adopter une approche qui vise à éliminer les différence entre les dispositifs et produire une expérience homogène, les auteurs appellent à un design qui considère la spécificité comme une opportunité pour une "optimisation gracieuse".


* [*Ordinateur et composition automatique*](ordinateur-et-composition-automatique.html)
* [*Unjustified text and the zero hour*](unjustified-text-and-the-zero-hour.html)
* [*Books with an Attitude*](books-with-an-attitude.html)
* [*TeX and daughters: an interview with Jacques André*]() (working title)
