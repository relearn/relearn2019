Title: The Making of the Standards
Lang: en
Date: 2012
Authors: Alexandre Leray; Stéphanie Vilayphiou


> For those who labor long and hard to craft good and just standards, as well as for those who have suffered from their absence. On the one hand, the fight against the tyranny of structurelessness. On the other, the fallacy of one size fits all.
> 
> <footer>—Lampland,  Martha, and Susan Leigh Star. 2009.  Standards and their stories: how  quantifying, classifying, and  formalizing practices shape everyday  life. Ithaca: Cornell University  Press.</footer>

Making standards is not only a technical job. It is a social process in  which power, negotiation, coercion and compromises come into play.  Standards both discriminate and refine, include and exclude. Whether de-facto or negotiated, how do standards affect graphic designers? What makes a standard successful or not? This chapter contains three clusters of texts which address these questions.

The first one (Jacquerye, [Unicode Inc.](unicodes.html)) deals with Unicode, the unified standard aiming to describe all characters in use on Earth. The texts discuss the implicit hierarchy between central and peripheral languages, and how the very structure of Unicode classification mirrors an established geopolitics. At the same time Jacquerye presents collective typographical projects trying to compensate this imbalance, within this system, and honor the richness of the world languages.

The second cluster ([Froshaug](on-typography.html), [Open Source Publishing](opening-the-black-box-of-printing.html), [de Montrond](un-code-de-la-mise-en-page.html)) investigates the implicit and explicit rules and standards that regulate the relationships between designers and printers. The texts evaluate the weight of the print infrastructure and the relationship between quality and access, as well as the importance of a common vocabulary between the different parties involved.

From different and converging perspectives, the third cluster ([Arnaud](w3c-go-home.html), [Pilgrim](how-did-we-get-here.html), [www-talk](proposed-new-tag-img.html), [Schrijver](who-makes-standards.html)) documents how the HTML language is debated and defined, influencing the way the web is shaped. It gives a historical account of how the `<img>` tag and the HTML5 specification came to life and the various tensions and conflicts revolving around the negotiated standards for the web. The contributions insist in different ways on the active role taken by people who care about the web as a creative space.

Content
-------

* [Unicodes](unicodes.html)
* [Archive of Notices of Non-Approval]()
* [On Typography](on-typography.html)
* [Opening the black box of printing](opening-the-black-box-of-printing.html)
* [Un code de la mise en pages](un-code-de-la-mise-en-page.html)
* [W3C go Home! (c’est le HTML qu’on assassine)](w3c-go-home.html)
* [How did we get here ?](how-did-we-get-here.html)
* [Proposed New Tag: IMG](proposed-new-tag-img.html)
* [Who Makes Standards](who-makes-standards.html)
