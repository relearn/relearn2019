Type: article
Authors: Martin Arnaud
Title: W3C go Home! (c’est le HTML qu’on assassine)
Date: 2003
Original: http://www.uzine.net/article1979.html
License: CC-BY
Lang: fr

<div markdown=true class="original">
![(IMG)][2] 

Une nouvelle sorte d’individus pollue actuellement les forums de discussion de la planète ; on ne connaît pas le nom de leurs chefs mais, par une forme étonnante de génération spontanée, ils se sont répandus partout et passent leur temps à en faire perdre à tous ceux qui s’intéressent à ce vieux monument qu’est le HTML. Pire : ils pourraient bien dégoûter tout ceux qui voudraient aujourd’hui « s’y mettre ».

Cette secte nouvelle porte plusieurs noms : les adorateurs du *W3C Validator*, les ayatollahs du XHTML ou, plus communément : « les ceusses qui sont pas foutus de monter un site Web potable alors ils font chier les autres »...

Leur arme : le [W3C Validator][1], un outil qui devrait permettre aux webmestres de progresser à leur rythme dans l’apprentissage du HTML, mais que ses adorateurs extrémistes brandissent comme le diktat suprême d’une cause sacrée.

Leur bible : les « recommandations » du même W3C. Là où tout être normalement constitué sait ce qu’est une « recommandation », eux comprennent « exhortation sauvage au strict respect d’une norme absolue ».

<marquee>
![(GIF)][2] 

**Cette page n’est pas W3C compliant**
</marquee>

Si l’on voulait bien les croire, le HTML à papa serait déjà mort, supplanté par une nouvelle race de langage, le XHTML, qui lui-même devrait bientôt muter vers sa forme ultime, quittant la gangue de sa chrysalide obsolète dans un processus dénommé « Modularisation du XHTML ». Hosanna, mes frères, il n’y a de Dieu que Dieu, et XML est son nom, et son fils est modulaire.

# L’affreux HTML n’avait pas que des défauts

On voudrait donc totalement tuer l’ancêtre HTML, au motif qu’il serait, comment dire... à chier. Pourtant notre antique langage n’avait pas que des défauts...

## Le HTML était, avant tout, *un langage simple* ; simplifié, même.

Il a été conçu pour être utilisé, sur le Web, à la place d’un autre ancêtre, le SGML (*Standard Generalized Markup Language*). Le principe du SGML est de décrire la structure d’un document selon une DTD (*Document Type Definition*) qui définit les éléments logiques autorisés (on obtient ainsi des fichiers documentaires dotés d’une *structure logique*) ; ensuite, on passe dans une moulinette pour, à partir de règles de description de mise en pages, créer le résultat final, tel que distribué au lecteur. En SGML on a indiqué que telle partie du texte était un intertitre (la DTD autorise l’existence d’éléments logiques définis comme étant des intertitres), on a indiqué dans une définition de style que les intertitres sont présentés avec un certain espacement avant et après, centré au milieu de la colonne de texte, dans telle police de caractère ; le document final présente donc le texte des intertitres, extrait du document SGML, dans cette police et à cette position. Le SGML, pourtant extrêmement puissant et complet, n’est pas si *generalized* que ça, puisque quasiment personne ne l’utilise pour produire des documents destinés au grand public (à part quelques secteurs de l’édition très spécifiques, tels que la production de dictionnaires). Pourquoi ? Parce que c’est imbitable. La documentation de l’outil de PAO FrameMaker SGML doit faire dans les cinq kilos et plus de mille pages ; au point qu’elle ne comporte pas de numéros de pages pour ne pas effrayer l’utilisateur. Il s’agit pourtant d’un logiciel Wysiwyg...


![(JPEG)][4] 
<figcaption>
**Le HTML c’est pas sorcier**   
Dans les cabarets de Berlin, un couple d’hacktivistes hermaphrodites milite pour un retour au HTML prolétarien.
</figcaption>

Donc, à l’origine, on rompt avec le SGML, on en extrait quelques balises rudimentaires, la façon dont chaque balise est interprétée est elle-même rudimentaire et directement intégrée dans les logiciels clients (navigateurs Web). Résultat : c’est facile (et plutôt amusant) à apprendre, on fait des progrès très rapidement, on n’a pas besoin d’un treuil hydraulique pour soulever la documentation, et ça tourne sur des machines peu puissantes.

Nous verrons que le XHTML devrait évoluer, sous l’impulsion du W3C, vers un retour aux sources, non du HTML, mais du SGML. On lui souhaite plus de succès auprès des millions de gens qui font déjà leur site à leur sauce HTML, que FrameMaker SGML n’en a rencontré auprès des utilisateurs de la PAO.

## Le HTML est *un langage tolérant*, qui accepte les erreurs.

Dès ses origines, le HTML tolère les erreurs de ceux qui le codent. Cela pour deux raisons fondamentales, objectives, qui ont défini ses principes de base.

###   *Compatibilités ascendante et descendante*

Un document HTML réalisé à une certaine époque pour une certaine génération de butineurs reste consultable plusieurs années plus tard avec les butineurs plus récents. C’est la compatibilité ascendante : vous pouvez visiter un site conçu en 1995 pour être affiché par Mosaïc avec la toute dernière version de Mozilla.

À l’inverse, un document HTML récent doit pouvoir être lu avec un logiciel plus ancien. Cela est vrai dans une large mesure (nous reviendrons sur les points particuliers). Si vous développez un site en vérifiant votre résultat avec Mozilla, il sera, du point de vue du HTML, consultable avec Lynx (client Web en mode texte). Pour imaginer l’exploit que cela représente, essayez simplement d’ouvrir un document produit par Word 2002 avec Word 97.

###    *Interopérabilité*

Un document HTML ne doit pas être lié à un logiciel ou à une machine, mais consultable sur toutes les machines avec tous les logiciels remplissant un certain nombre de conditions minimales. Par exemple, la présente page s’affiche de manière très graphique sous Mozilla ou Microsoft Explorer, mais peut être consultée en mode texte (Lynx) sur une vieille machine.

Ces deux exigences (qui font partie de la définition initiale du HTML) sont atteintes grâce à une règle simple : *le navigateur se contente d’ignorer ce qu’il ne comprend pas*. Si l’on utilise des balises dernier cri dans une page Web, elles seront purement et simplement ignorées par les logiciels qui ne les comprennent pas ; de cette façon l’information contenue dans la page est tout de même parfaitement disponible.

De fait, le HTML est un langage extrêmement tolérant, puisque si le webmestre commet une erreur, s’il expérimente une nouvelle balise qui n’est pas comprise par le butineur, rien de catastrophique ne se produit : la page s’affiche tout de même et son ordinateur ne l’insulte pas.

Cela, par exemple, à l’inverse d’une imprimante PostScript qui s’arrête en indiquant « erreur PostScript » ; ou la compilation d’un fichier TEX qui signale toutes les erreurs une par une.

Cette tolérance est sans aucun doute l’une des raisons du succès du HTML : elle permet d’apprendre sans se faire insulter par sa machine (ce qui rebute normalement tout non-informaticien), en autorisant la diffusion de documents contenant des erreurs de codage, sans que personne n’en souffre. En HTML, on peut « coder comme un porc » (parce qu’on apprend, parce qu’on veut produire très vite...), la page reste consultable et l’information accessible

Elle favorise l’apprentissage par *patchs* successifs : on enrichit progressivement ses pages en fonction de ses nouveaux progrès, sans jamais être arrêté dans sa courbe de progression par une erreur qui bloquerait l’affichage (comme, par exemple, l’oubli de déclarer une variable dans un langage de programmation interrompt la compilation).

## Le HTML est *une norme d’usage*.

Le HTML n’a pas attendu la création d’un organisme de normalisation comme le W3C pour exister et se développer. Il est nettement plus ancien.

Est-il donc possible que le HTML ait rencontré un succès mondial auprès de millions de webmestres, alors qu’aucune autorité supérieure n’en assurait la définition et la normalisation préalable ? Comment qu’on faisait ?

On faisait simplement : on essayait les nouvelles balises, et on regardait sur quelques butineurs différents ce que ça donnait, et on choisissait les solutions qui, à la fois, rendaient service (page plus jolie) et restaient consultables sans trop de restrictions visuelles sur un butineur qui ne les comprenaient pas. Sachant que, longtemps, Netscape a été le seul butineur utilisé par les usagers du Web, cela consistait à tester les nouvelles balises introduites à chaque version, et à attendre un peu avant que cette version soit suffisamment répandue pour faire un usage intensif des nouvelles balises. Puis est venu Microsoft Explorer, mais le principe est resté le même. : on teste les bidules, on échange des trucs, et on regarde comment ça marche avec différents butineurs.

Aujourd’hui, malgré les « recommandations », la situation n’a pas changé : on ne peut exploiter ces « recommandations » qu’à partir du moment où elles sont utilisées par les principaux butineurs, ceux qui sont réellement utilisés par les utilisateurs (comprendre : MSIE à plus de 90%...). Il a fallu attendre la disparition quasi complète de Netscape 4 du paysage avant d’utiliser intensivement les feuilles de style, car l’implémentation des CSS dans ce logiciel (encore très répandu dans sa dernière version avant Mozilla) était totalement buguée ; utiliser les premières recommandations avec plein de feuilles de style était le meilleur moyen de présenter des pages immondes aux utilisateurs. Aujourd’hui, il faut composer avec MSIE (nettement moins irrespectueux des normes qu’on veut bien le dire, mais qui, tout de même, bloque l’utilisation réelle de plusieurs recommandations importantes).

Cette caractéristique de *norme d’usage* n’a pas produit que des catastrophes (elle n’a, en particulier, pas produit la catastrophe des catastrophes, mille fois annoncée, qui aurait mené à l’existence d’un HTML « ouvert » et d’un HTML privatisé par Microsoft). Elle a, mine de rien, conduit chaque webmestre à systématiquement intégrer dans son apprentissage du HTML les notions de compatibilités ascendante et descendante et d’interopérabilité. Si l’application des recommandations facilite, techniquement, l’application de ces notions, à l’inverse une approche extrêmement restrictive revient à priver les webmestres bidouilleurs de l’acquisition de réflexes indispensables (vérifier systématiquement la compatibilité et l’interopérabilité à son propre niveau).

# Les défauts « scandaleux » du HTML

La courte histoire du HTML a eu son lot de difficultés, qui ont énormément fait hurler les puristes successifs (parce qu’avant les normes du HTML 1.1 méga-strict, il y avait déjà des puristes). Ces difficultés semblent l’une des principales motivations à la création du W3C et à une vision extrêmement obtuse de l’application des normes.

## **Les balises exotiques du HTML**

La guéguerre entre Netscape et Microsoft, à l’époque où les deux logiciels coexistaient réellement (MSIE n’avait pas totalement écrasé le marché), a conduit à l’émergence de quelques balises HTML propres à chaque logiciel. Ces balises ont fait couler beaucoup d’encre. Pourtant, rétrospectivement, on cherche encore où est la catastrophe annoncée.

###   `<blink>`

La fameuse balise <blink> est la plus ancienne de ces balises exotiques (Netscape 3 ?). Elle a provoqué nombre de crises de nerfs parmi les ayatollahs de l’époque. Elle se contente de faire <blink>clignoter</blink> du texte.

À part la faute de goût (c’est tarte et laid), elle ne provoque pourtant rigoureusement aucune incompatibilité. Au pire, le texte ne clignote pas... La belle affaire.

###    `<bgsound>`

Cette balise made in Microsoft Explorer permettait de faire jouer du son lors de la visite d’une page Web. Là encore, ça n’est pas catastrophique : si elle n’est pas comprise, le son n’est tout bonnement pas joué ; le son n’étant jamais indispensable à la visite d’une page Web, son absence n’est pas désastreuse. Supplantée immédiatement, de toute façon, par d’autres méthodes.

###    `<marquee>`

La balise `<marquee>`, également une création Microsoft, permettait de créer des bandeaux de texte défilant horizontalement. Balise aujourd’hui totalement oubliée, alors que, curieusement, elle fonctionne toujours sur les dernières versions de Microsoft Explorer et même sous Mozilla.

Au pire, le texte s’affiche mais ne se déplace pas. Encore un scandale inutile.

###    `<multicol>`

Cette balise Netscape a totalement disparu. C’est amusant, parce qu’il n’en existe pas d’alternative simple aujourd’hui alors qu’elle pouvait avoir quelques usages pratiques. Il s’agissait simplement d’afficher du texte sur plusieurs colonnes, très simplement.

Utilisée sur un long texte, il s’agit évidemment d’une faute impardonnable nuisant directement à la lisibilité d’une page (le multicolonnage d’un texte sur le Web est catastrophique : le bas des colonnes n’est pas forcément sur le même écran, verticalement, que le haut de la colonne où il faut reprendre la suite de la lecture). Mais à petites doses, c’était très pratique (par exemple pour présenter des listes `<li>` équilibrées).

Et toujours rien de catastrophique : sans l’affichage en plusieurs colonnes, le texte s’affiche tout de même, dans une seule colonne. On peut imaginer pire en matière de rupture sauvage avec l’interopérabilité du HTML.

###   *et al.*

Il y a eu quelques autres balises très spécifiques, mais elles ont fait couler peu d’encre. Notons que la pire (<comment>), de Microsoft, destinée à remplacer les habituels commentaires HTML (celle-ci provoquant réellement des incompatibilités d’affichage graves), n’a jamais été réellement utilisée par les webmestres (qui ne sont tout de même pas totalement des imbéciles).

Ainsi, du côté des balises exotiques, qui ont justifié de très nombreux discours sur l’explosion du Web, la perte de la compatibilité, la disparition de l’interopérabilité, il s’agissait essentiellement de détails sans réelle gravité. Soit parce qu’elle ne nuisait pas directement à l’interopérabilité, soit parce que, lorsqu’elles le faisaient, elles n’étaient tout bonnement pas utilisées (l’aspect *norme d’usage* du HTML utilisé par des gens qui échangent entre eux des conseils d’utilisation).

##  **Les véritables problèmes du webmestre**

Certaines grosses innovations du HTML ont cependant provoqué des difficultés plus importantes.

###    Les tables de mise en page

Netscape 3, en affinant les possibilités de Netscape 2, a introduit la possibilité de créer des tableaux (<table>) dont on pouvait contrôler précisément l’apparence. Rapidement, il s’est agit de créer des tableaux « invisibles » (dont on ne voit pas les bordures) pour définir la construction graphique des pages. C’est, par exemple, la présentation « en colonnes » : à gauche une colonne de navigation dans le site, au centre un grande colonne affichant l’information principale.

C’est encore, aujourd’hui, l’un des aspects du HTML les plus critiqués, et auquel il faudrait substituer l’utilisation des feuilles de style.

Sauf que...   
- cette façon de procéder est désormais unifiée dans tous les butineurs ; les affichages sont désormais identiques d’un logiciel à l’autre (la principale difficulté n’était pas de gérer l’absence des tableaux dans certains logiciels - ils ont rapidement été présents partout - mais des affichages et des possibilités très différents) ;   
- le contrôle des tableaux avec les feuilles de style, depuis le HTML 4, permet de simplifier et unifier leur présentation tout en retrouvant un code « léger » ; la disparition de Netscape 4, notoirement bugué de ce côté, simplifiant encore les choses ;   
- la mise en pages en tableaux ne provoque aucune incompatibilité, malgré tout ce qui s’est dit, avec les logiciels en mode texte (Lynx) ;   
- dans l’apprentissage du HTML, cette façon de procéder vient progressivement, après l’apprentissage des balises d’enrichissement typographique ; cela n’est certes pas évident, mais cela n’a rien à voir avec le difficulté conceptuelle de tout passer en feuilles de style ; comparativement, les tableaux sont très simples à comprendre et à mettre en place (on affiche de temps en temps les bordures - border="1" - et on *patche* son tableau ; à l’inverse, il faut concevoir la mise en forme avec les feuilles de style dès le début, et non par l’ajout de patchs successifs ; le résultat est peut-être plus propre, mais il y a là un niveau conceptuel supplémentaire qui introduit un saut énorme dans la courbe d’apprentissage des utilisateurs du HTML).

###    Les frames

Le multi-fenêtrage a, lui aussi, provoqué de nombreuses crises de nerf. Considéré comme une hérésie, il conduisait à rendre ses pages totalement incompatibles avec la génération de butineurs précédente (en gros : Mosaïc). Sauf que, là encore, le problème n’était pas dans la définition de la balise elle-même, mais dans l’usage que certains en ont fait : dans la page de définition des fenêtres, la balise <frameset> permettait d’inclure le code spécifique pour la navigation sans les frames.

En réalité, les frames ont rendu d’énormes services à tous ceux qui géraient des sites « à la main », statiques. Le rejet des frames concernait ceux qui, à l’époque, pouvaient bidouiller leur serveur, et utiliser les CGI pour construire des pages dynamiques.

L’intérêt des frames est, en effet, de pouvoir gérer la navigation dans un fichier séparé unique, présent sur chaque page du site. L’ajout d’une nouvelle page d’information dans un site ne nécessitait, pour gérer la navigation, que la modification d’un seul fichier. Précédemment, ceux qui géraient leurs sites « à la main » devaient modifier plusieurs fichiers pour assurer la mise à jour du système de navigation de leur site.

Aujourd’hui, les frames sont plus ou moins rendues obsolètes par les outils permettant de créer des sites dynamiques, mêmes extrêmement simples (le code PHP pour inclure, sans fioritures, un fichier affichant une navigation commune dans une page HTML de contenu, est très accessible).

Mais ces frames, si décriées, ont assuré à tous ceux qui n’étaient pas techniciens (parce que programmer ses pages en Perl, il fallait être sévèrement burné), qui ne disposaient pas d’un serveur personnel pour héberger son site, et/ou qui ne payaient pas un hébergement professionnel autorisant la manipulation des CGI, la possibilité de réaliser des sites de taille conséquente sans être poussé au suicide à chaque mise à jour du site.

On notait déjà la différence de jugement : d’un côté des utilisateurs sans réelles compétences techniques, qui se forment sur le tas et créent des sites plutôt riches avec les outils du bord, et de l’autre des techniciens disposant de la formation et de l’accès aux machines, critiquant violemment la seule solution permettant de palier les limitations imposées au grand public.

###   Javascript

En réalité, la principale incompatibilité qui rendait dingues les webmestres ne venait pas du HTML lui-même, mais du langage Javascript, destiné à permettre un peu d’animation du côté du client.

En particulier : le Javascript sortait systématiquement bugué (à mort) chez Netscape, le Javascript de Microsoft Explorer était plus ou moins incompatible avec celui de Netscape, et celui de Mozilla est encore une autre version du langage.

Lorsque le HTML 4 a tendu vers le DHTML, où les premières versions de Mozilla cotoyaient les dernières évolutions de Netscape 4 et l’impérial Microsoft Explorer, les choses les plus simples vous poussaient à ouvrir une fenêtre et à sauter en bas de l’immeuble.

Mauvaise nouvelle : la situation n’a pas changé, et le W3C ne participe pas à la définition du Javascript. Quant aux nouveaux butineurs censés respecter à la lettre les normes, ils n’améliorent pas toujours la situation de ce côté (bugs, insuffisances, évolutions ; saut qualitatif important du Javascript entre les versions 1.2 et 1.3 de Mozilla ; butineurs qui utilisent des méthodes différentes pour se faire identifier par le serveur...).

# XHTML, sauveur de l’humanité

Pour pallier tous ces prétendus problèmes monstrueux du HTML, il faudrait donc passer à une utilisation totalement stricte du XHTML (et, bientôt, carrément faire du XML/XSLT).

##  **XHTML, une norme claire**

Le XHTML aurait l’immense avantage, grâce à la définition centralisée de normes, d’être beaucoup plus clair. Inutile désormais de se compliquer avec des implémentations spécifiques du HTML, il suffit de consulter la norme unique du XHTML pour que ça fonctionne illico.

Première étape : choisir sa version du XHTML.

Et là, bon courage. Voici, texto, [ce que nous explique le W3C][5] :

> XHTML 1.0 is the first step and the HTML Working Group is busy on the next. XHTML 1.0 reformulates HTML as an XML application. This makes it easier to process and easier to maintain. XHTML 1.0 borrows elements and attributes from W3C’s earlier work on HTML 4, and can be interpreted by existing browsers, by following a few simple guidelines. This allows you to start using XHTML now ! You can roll over your old HTML documents into XHTML using an Open Source HTML Tidy utility. This tool also cleans up markup errors, removes clutter and prettifies the markup making it easier to maintain.
> 
> **Three "flavors" of XHTML 1.0 :**
> 
> XHTML 1.0 is specified in three "flavors". You specify which of these variants you are using by inserting a line at the beginning of the document. For example, the HTML for this document starts with a line which says that it is using XHTML 1.0 Strict. Thus, if you want to validate the document, the tool used knows which variant you are using. Each variant has its own DTD - Document Type Definition - which sets out the rules and regulations for using HTML in a succinct and definitive manner.
> 
> *   **XHTML 1.0 Strict **- Use this when you want really clean structural mark-up, free of any markup associated with layout. Use this together with W3C’s Cascading Style Sheet language (CSS) to get the font, color, and layout effects you want.
> *   **XHTML 1.0 Transitional** - Many people writing Web pages for the general public to access might want to use this flavor of XHTML 1.0. The idea is to take advantage of XHTML features including style sheets but nonetheless to make small adjustments to your markup for the benefit of those viewing your pages with older browsers which can’t understand style sheets. These include using the body element with bgcolor, text and link attributes.
> *   **XHTML 1.0 Frameset** - Use this when you want to use Frames to partition the browser window into two or more frames.
> 
> The complete XHTML 1.0 specification is available in English in several formats, including HTML, PostScript and PDF. See also the list of translations produced by volunteers. 

Trois versions différentes, c’est chouettos, non ? Mais tout cela est déjà dépassé, la prochaine étape est prête, il s’agit du XHTML 1.1 :

> **XHTML 1.1 - Module-based XHTML**
> 
> This Recommendation defines a new XHTML document type that is based upon the module framework and modules defined in Modularization of XHTML. The purpose of this document type is to serve as the basis for future extended XHTML ’family’ document types, and to provide a consistent, forward-looking document type cleanly separated from the deprecated, legacy functionality of HTML 4 that was brought forward into the XHTML 1.0 document types.
> 
> This document type is essentially a reformulation of XHTML 1.0 Strict using XHTML Modules. This means that many facilities available in other XHTML Family document types (e.g., XHTML Frames) are not available in this document type. These other facilities are available through modules defined in Modularization of XHTML, and document authors are free to define document types based upon XHTML 1.1 that use these facilities (see Modularization of XHTML for information on creating new document types). 

Evidemment, les vieilles documentations qui vous expliquent pas à pas comment ajouter la balise <html>, puis <body>, puis des paragraphes, puis du gras et de l’italique, puis des images, deviennent soudainement totalement ringardes comparées à ce déferlement de novlangue pour diplômés en informatique. (Et je n’exagère pas : il s’agit du texte de présentation du HTML, celle immédiatement accessible lorsqu’on suit le lien « HTML » depuis la page d’accueil du W3C ; il ne s’agit pas d’une page réservée aux cracks du XML.)

Si vous pensiez vous en sortir en restant sagement au HTML 4, détrompez-vous :   

- il n’existe pas de HTML 4, mais du HTML 4.01 ;
- d’après le W3C Validator, vous devez choisir entre le HTML 4.01 Strict, Transitional ou Frameset ;
- essayez de trouver rapidement les différences entre ces trois versions dans la page de [spécification du HTML 4.01][6] du W3C.

À l’évidence, le XHTML n’est pas destiné à être utilisé et appris par le grand public (celui-là même qui a appris le HTML depuis des années, assurant au Web sa nature d’outil d’autopublication et de partage).

##  **La perte de compatibilité et de souplesse**

Tenez-vous bien : pour prétendre rendre le HTML plus compatible avec tout, la notion de compatibilité descendante est abandonnée : le terme le plus présent dans les documentations du W3C est : « *deprecated* ». L’autre terme est « *compliant* ».

Une balise ancienne peut désormais être définie comme « deprecated » (obsolète). La rupture de principe est flagrante : la compatibilité descendante n’existe plus, d’une version à l’autre du HTML il faudrait abandonner certaines balises, sauf à se manger un méchant message d’insulte de la part du W3C Validator.

La syntaxe de vos pages doit, dans la même logique, être « compliant » (conforme). Autre rupture : là où les balises HTML non comprises par un butineur étaient purement et simplement ignorées, il faudrait croire désormais que le *parser* XML va prendre feu en cas d’erreur. La souplesse qui rendait le HTML si facile à utiliser devrait donc être remplacée par un langage intransigeant n’offrant aucune tolérance à l’erreur.

L’apprentissage du XHTML seraient donc celui d’un langage rigide, complexe et n’acceptant pas les bidouilles. La construction des pages par patchs successifs, qui est la méthode simple pour apprendre soi-même le HTML, est bloquée.

##  **L’accessibilité**

Dans la nouvelle théologie des adorateurs du dieu W3C, l’argument le plus courant justifiant l’abandon du HTML est de faciliter l’accès aux pages Web aux non-voyants. Soyons clair : c’est une tarte à la crème.

Les pages HTML, mêmes complexes, étaient déjà accessibles, pour peu que le webmestre s’en donnait la peine. Et l’adoption du XHTML, avec une stricte séparation des contenus et des feuilles de style de l’affichage, ne résout pas grand-chose. De la même façon que certains usages du HTML rendaient la lecture difficile en mode texte, il est tout à fait possible de fabriquer des pages en XHTML strict pénibles à consulter par un non-voyant.

Deux exemples classiques le démontrent.

###    La mise en page en tableaux

Construire graphiquement sa page en tableaux constituerait un obstacle infranchissable pour une consultation en mode texte. À l’inverse, tout passer en feuilles de style résoudrait le problème.

Qu’est-ce qui cloche réellement dans la mise en page en tableaux ? Il s’agit de l’habitude qui consiste à farcir la colonne de gauche d’une foultitude d’informations relativement accessoires pour la navigation, *puis* à coder le texte à droite. Cela se fait ainsi :


<table>
<tbody><tr>
<td>40<br> lignes<br> inutiles<br> à gauche</td>
<td><b>Le texte réellement utile</b></td>
</tr>
</tbody></table>

    <table>
    <tr>
    <td>40<br /> lignes</br /> inutiles<br /> à gauche</td>
    <td><b>Le texte réellement utile</b></td>
    </tr>
    </table>

Ce qui, sur un butineur en mode texte, donne l’affichage&nbsp;:


> 40    
> lignes
> inutiles    
> à gauche    
> **Le texte réellement utile**



L’accès à l’information essentielle (le texte de droite) est rendu pénible par le fait qu’il faut se cogner, auparavant, le lecture des 40 lignes inutiles de la colonne de gauche.

Il faudrait donc passer en feuilles de style, ce qui permettrait de coder dans la page d’abord les informations essentielles, puis les éléments accessoires après. De cette façon, dans un butineur en mode texte, l’information serait hiérarchisée de manière plus évidente et l’accès à l’information vitale rendu immédiat.

Mais :

- on peut déjà réaliser cela avec les tables ;
- le problème est rigoureusement le même en XHTML : si on ne conçoit pas sa page en « inversant » la suite du code de son affichage à l’écran (l’information accessoire, qui apparaît au début grâce aux feuilles de style, apparaît en réalité après dans la programmation des pages et donc dans l’affichage en mode texte), la question n’est pas du tout réglée par la magie du respect des normes.

Voici par exemple le même tableau, codé toujours avec les `<table>`,


<table>
<tbody><tr>
<td></td>
<td rowspan="2"><b>Le texte réellement utile</b></td>
</tr>
<tr>
<td>40<br> lignes<br> inutiles<br> à gauche</td>
</tr>
</tbody></table>


codé ainsi&nbsp;:

    <table>
    <tr>
    <td></td>
    <td rowspan="2"><b>Le texte réellement utile</b></td>
    </tr>
    <tr>
    <td>40<br /> lignes</br /> inutiles<br /> à gauche</td>
    </tr>
    </table>

>**Le texte réellement utile**    
> 40    
> lignes    
> inutiles    
> à gauche


On obtient bien l’inversion nécessaire à une présentation adaptée au mode texte, où l’information essentielle est immédiatement accessible.

L’amateur de XHTML strict, s’il code toujours selon la logique « de gauche à droite, de haut en bas » (naturelle), commettra la même erreur, avec du code pourtant « compliant » :

<form method="get" action="/"><div><textarea dir="ltr" class="spip_cadre" rows="1" cols="40" readonly="readonly" id="itsalltext_generated_id__3">&lt;div class="colonne-gauche"&gt;40 lignes inutiles&lt;/div&gt;
&lt;div class="colonne-droite"&gt;Le texte réellement utile&lt;/div&gt;</textarea><img src="chrome://itsalltext/locale/gumdrop.png" title="It's All Text!" style="cursor: pointer ! important; display: none ! important; position: absolute ! important; padding: 0px ! important; margin: 0px ! important; border: medium none ! important; width: 28px ! important; height: 14px ! important; opacity: 0.0152174 ! important; left: 805px ! important; top: 299px ! important;"></div></form>

Il s’agit bien de XHTML, tout aussi pénible à consulter en mode texte que les tableaux du HTML.

###    L’utilisation des enrichissements typographiques pour structurer la page

Dès Netscape 2, affinée par Netscape 3, l’utilisation de la balise <font…> a permis à de nombreux webmestres de suggérer la structure de leur document sans plus utiliser les codes `<h1>`, `<h2>`... qui définissent bien la structure. Pour afficher un titre, certains ont utilisé l’indication de tailles de caractères plutôt que l’indication permettant de structurer le document (`<h1>` ne se contente pas de mettre le texte en grande taille et en gras, mais aussi indique qu’il s’agit d’un titre de premier niveau).

Du coup, avec un butineur en mode texte, rien n’indique que tel élément est un titre, parce que l’information n’est plus présente dans le code.

![(JPEG)][7] 
<figcaption>
**Validator ? Moi jamais !**   
Pour Monique, le XHTML tel qu’on veut désormais l’imposer détruit totalement cette courbe d’apprentissage très progressive. "J’espère l’éviter à mes enfants", ajoute-t-elle.
</figcaption>

Cependant, le XHTML, même « strict », n’implique pas que les webmestres vont conserver des éléments clairs de structuration. Il suffit de coder des feuilles de style définissant l’enrichissement typographique sans mention de la structure classique des documents (coder un CSS « .montitre » et ne pas coder les titres avec `<h1>`).

Ce qui a permis d’obtenir à la fois le contrôle typographique personnalisé et l’utilisation des balises de structuration, c’est l’introduction des CSS dans le HTML 4.

Ici encore, ça n’est pas la norme elle-même qui est en cause, appliquée de manière plus ou moins stricte, mais l’usage qu’en font les webmestres. On peut faire du HTML - et même, du HTML pas *compliant* - très joli et qui respecte la structure du document pour une navigation en mode texte, et du XHTML respectant scrupuleusement les spécifications de la norme, mais pénible à visiter en mode texte.

*Remarque perfide.* On pourrait aussi signaler que poster deux cent fois le même message inutile sur les listes de diffusion nuit largement plus à l’accès à l’information par les malvoyants, dont la vitesse de lecture est nettement ralentie, que des pages qui se consultent parfaitement sous Lynx sans respecter la dernière recommandation à la mode...

##  **Interopérabilité**

On se contentera de demander en quoi un système basé sur des parsers intolérants avec des versions du langage perdant la compatibilité avec les versions précédentes serait plus « interopérable » qu’un langage souple et tolérant.

La nécessité réelle d’un système séparant le fond et la forme tout en présentant l’information structurée au public est développée dans le paragraphe suivant.

##    **Séparer le fond et la forme**

Le XHTML revient, *non pas parce qu’il utilise les recommandations chipoteuses du W3C Validator, mais uniquement lorsqu’il est utilisé d’une manière très spécifique*, au principe de balises de structuration logique associées à des tables de mise en pages. Autrement dit, il facilite la stricte séparation du fond et de la forme.

L’évolution annoncée (à mon avis totalement mort-née) poussera cette logique encore plus loin : du XML contenant non seulement la structure logique des pages, mais du site lui-même ; une DTD personnalisée définira la liste des éléments logiques autorisés ; un ensemble de règles de mise en page (XSLT stylesheet) permettra ensuite de présenter à partir du fichier XML les documents au public. Du HTML on sera passé au XHTML puis [au couple XML/XSLT][8] [[1][9]].

Sauf que : on s’en fout.

![(JPEG)][10] 
<figcaption>
**Un problème de société**   
Caroline (à gauche sur la photo) : Dès Netscape 2, affinée par Netscape 3, l’utilisation de la balise <font…> a permis à de nombreux webmestres de suggérer la structure de leur document sans plus utiliser les codes `<h1>`, `<h2>`... qui définissent bien la structure.
</figcaption>

Quelle est l’utilité réelle de séparer totalement le fond de la forme sur des pages Web présentées au public ? Quelle est l’utilité réelle consistant à pouvoir extraire de mes pages une information structurée ? Au passage, est-ce que j’aurais raté la nouvelle loi abolissant totalement les droits d’auteurs sur le réseau ?

Dans la pratique, l’échange d’informations complètes et structurées se fait sur les informations que l’on accepte d’échanger, et à la condition que les structures soient invariantes d’un site à l’autre (ce qui n’a rigoureusement aucune raison d’être le cas - et tant mieux !). Le besoin réel est donc extrêmement restreint.

Surtout, c’est déjà le cas sur tous les sites utilisant des systèmes de publication de sites dynamiques : l’information est déjà stockée sous une forme structurée dans une base de données, et elle est extraite pour créer les pages fournies aux visiteurs. Inutile, alors, de s’astreindre à une stricte séparation du fond et du contenu sur les pages affichées publiquement, alors qu’on peut fournir par ailleurs des pages extrêmement simplifiées graphiquement (et même : aucun graphisme). C’est par exemple le cas des fichiers de *backend* fabriqués par ces systèmes : on peut bien faire des sites graphiquement exubérants mélangeant allègrement le fond et la forme, l’extraction et l’échange d’informations se fait par une autre page, effectivement dans un format XML, mais sans s’enquiquiner à faire de la mise en forme graphique. (La sauvegarde et l’exportation des contenus des bases de données grâce au format XML, et par exemple en passant par SOAP, semblent prometteuses ; mais elles ne concernent en rien le public qui réalise des sites Web, ni ne requiert d’abandonner le HTML.)

Par ailleurs, l’exploitation d’une même source de contenu pour la distribuer sur différents supports mériterait un autre article : il est à la mode de prétendre que l’évolution logicielle mènera à créer automatiquement, à partir d’une même source, plusieurs supports (presse, audiovisuel, multimédia...). Je vous livre mon avis sans développer : c’est une vue de l’esprit destinée uniquement à vendre de nouvelles versions des logiciels (par exemple, si vous coupez l’image de la télé, ça ne vous fait pas une émission de radio...).

# Avant tout : privilégier l’apprentissage

L’avantage fondamental du HTML est non seulement sa relative simplicité, mais surtout sa courbe continue d’apprentissage. On commence simplement et, par l’autoformation, on finit par atteindre un très bon niveau de compétence sans avoir rencontré de difficultés conceptuelles bloquant l’apprentissage.

Classiquement, on procède par étapes :

1.  les balises fondamentales et simples : `<html>`, `<body>`..., `<p>`, `<b>`, `<i>`... ; dès ces rudiments il est possible de publier de l’information ;
2.  rapidement on apprend les balises supplémentaires permettant, en gros, de tout faire (tableaux à l’intérieur des textes, listes...) ; dans le même temps on fait joujou avec un nombre de plus en plus conséquent d’attributs ;
3.  une étape est franchie lorsqu’on comprend le principe des tableaux de mise en page (tableaux invisibles permettant de créer des alignements de colonnes) ; il y a là un saut dans l’apprentissage du HTML, mais tenant plus d’une utilisation graphique que de l’apprentissage de nouvelles balises ;
4.  progressivement on affine et simplifie son code avec des feuilles de style (HTML 4) ;
5.  à la longue on parvient à séparer plus nettement les feuilles de style et le contenu, ce qui effectivement permet d’obtenir un code plus « propre », plus léger...

Très souvent, au cours de cette progression d’apprentissage, on se met à bidouiller en JavaScript, on s’initie au PHP...

Le point essentiel, ici, est l’aspect très linéaire de la progression, dans la logique des *patchs* : on commence par les balises les plus rudimentaires, et on enrichit progressivement le savoir par l’ajout progressif de nouveaux « trucs ». Seuls le passage aux tableaux de mise en page et aux classes de feuille de style représentent des ruptures conceptuelles dans cette progression, mais elles sont amenées comme réponses à des besoins réels issus de sa propre expérience (donc beaucoup plus facile à comprendre, et avec une forte motivation pour faire l’effort de leur apprentissage).

À l’inverse, le XHTML tel qu’on veut désormais l’imposer détruit totalement cette courbe d’apprentissage très progressive. Il faut, dès le début, attaquer par la dernière étape : apprendre *ab initio* les caractéristiques des feuilles de style (sans être passé par l’expérience personnelle de la conversion de besoins graphiques réels - bordures, espacements... - qui en donne naturellement le sens) et comprendre dès le début l’abstraction que sont les feuilles de style séparées.

Il n’y a donc plus de méthode « naturelle » d’apprentissage pour un système tel que le XHTML « strict ». Gros avantage évidemment : seuls les professionnels de la profession qui sont allés à l’école pourront réaliser des sites Web.

Ce qui nous ramène à la justification habituelle de l’application stricte et rigide des normes : interdire l’accès aux nouveaux entrants en affichant un alibi de qualité.

 

[[1][11]] Piotrr me fait remarquer que, après la vague d’intégristes du XHTML strict, on pourra alors se farcir les ayatollahs du XSLT, lesquels prétendront évidemment que c’est beaucoup plus propre - et vach’tement simple.

 

 

 

Signalons le cas pathologique d’une autre recommandation du W3C : [MathML][13]. Il s’agit d’une norme permettant, théoriquement, d’afficher des formules mathématiques sur le Web.

Il s’agit d’une norme déjà très ancienne. Elle n’est implémentée correctement sur aucun butineur utilisable par le grand public (Amaya peut difficilement être considéré comme un logiciel utilisable). Pourtant, la norme MathML 2 vient de sortir. L’espoir fait vivre.

La qualité et la solidité du langage sont cependant au rendez-vous. On peut coder une formule assez simple ainsi :

    <math xmlns="http://www.w3.org/1998/Math/MathML">
      <mrow>
        <msup>
          <mfenced open="[" close="]">
            <mrow>
              <mi>a</mi>
              <mo>+</mo>
              <mi>b</mi>
            </mrow>
          </mfenced>
          <mn>260</mn>
        </msup>
        <mo>+</mo>
        <msub>
          <mfenced open="{" close="}">
            <mrow>
              <mi>a</mi>
              <mo>+</mo>
              <mi>b</mi>
            </mrow>
          </mfenced>
          <mi>i</mi>
        </msub>
      </mrow>
    </math>

ce qui donne (en gros) :

    [*a* + *b*]260 + {*a* + *b*} *i*

Signalons que cette formule se code, avec l’antique TeX :

Voilà qui définit assez bien le futur du XHTML : un langage de description conçu pour l’échange d’informations structurées par des logiciels. Coder à la main sans faire exploser son *parser* XML est inconcevable.

 [1]: http://validator.w3.org/
 [2]: /media/images/arton1979.gif
 [4]: /media/images/PERSO196.jpg
 [5]: http://www.w3c.org/MarkUp/
 [6]: http://www.w3.org/TR/html401/
 [7]: /media/images/PERSO108.jpg
 [8]: http://www.w3.org/Style/XSL/
 [9]: #nb1 "[1] Piotrr me fait remarquer que, après la vague d'intégristes du XHTML (...)"
 [10]: /media/images/PERSO174.jpg
 [11]: #nh1
 [13]: http://www.w3c.org/Math/
 </div>
