Type: book
Authors: Anthony Froshaug
Title: On Typography
Date: 2000
Book: Anthony Froshaug. Typography & texts
Publisher: Hyphen Press
Pages 107-110
Original: http://www.hyphenpress.co.uk/journal/2009/10/19/on_typography
License: All rights reserved
Lang: en

<!--
Untitled manuscript written in 1947, previously unpublished.

The manuscript is written in ink on handmade paper: the work of a scribe, exploring modern ideas with old means of production. Though presumably a fair copy of previous drafts, it is nevertheless a virtuoso piece of handwork, but done quickly. It is headed ‘10 11 July 1947 1am’, and concludes ‘5am’.

This is one of a number of writings on these themes from this time. AF had by then started to print in London, before setting up as a full-time printer in Cornwall.


Based on the work of earlier inscription cutters & scribes, the first primers attempted to produce exact facsimiles of manuscript books; but facsimiles capable of production in greater quantity and with more speed & accuracy. Both the aesthetic & epistemological patterns of the fifteenth-century book were expropriated by the lettercutters & typefounders & compositors & pressmen. In the related technics of papermaking & inkmaking & binding, the new printers eventually assumed prominence as customers, though not without t.he eventual reaction of a variant of Gresham’s Law.

But early standards were high: due not only to the demands of any competitor in a field where the object to be displaced is lovely, but also to the inherent human desire, then not discouraged by society, to do responsible work as well as possible.

As is general when an already formulated entity, such as a book, is first mass produced, the object to be made is attempted & considered as a complete entity; not examined nor modified except so far as the limitations of the technic require. The book has therefore not changed or developed to any degree since the invention of priming: the technic reached its finest achievements in the first hundred years of its development, and the later history of printing has been a history of technical development in machinery and sociological depression in aesthetics. As a geological fault reveals the history & biology of the earth as a fixed pseudomorph, the book is a cultural pseudomorph of the society & aesthetics of its environment.

But in this matrix, the world of Gutenberg & Coster & Faust & Schoefler, obtained certain beliefs & visual patterns & solutions which were not less primary for being implicit & unformulated.The book, as it has been modified over five hundred years, is based on these conventions, themselves imitated from the antecedent manuscript.

Before inorganic records were made, human thought was communicated from mouth to ear to brain to mouth, individual to individual. Speaker and hearer were participators in a process of mutual interaction and conscious & unconscious modification of ideas: the creator was the community, the created thing organic, subject to mutation from generation to generation.

The introduction of inscribed & written records made possible the identification of he originator: his ideas became inanimate concepts, petrified at birth; their records anonymous; their recipient, the reader, his own interpreter. Anonymity & ankylosis gave the creator the possibility of eternal life; mass production by printing gave him the possibility of simultaneous location in many places. But it took from him the possibility of interpretation & collaboration.

The reader his interpreter implies a system of symbols able to excite in his senses the ideas of another; implies the establishment of an isomorphic expression for ideas & speech. But the alphabet can only express ideas insofar as they are apt to expression in words as printed or written.

Each letter of the alphabet is a complete entity, just as is each number from 0 to 9. But while 2 + 1 + 20 differs from 3 + 1 + 20 by one unit, ‘bat’ differs from ‘cat’ in a way which has nothing to do with c being located one place after b on the scale a to z. A word is a complete whole, a gestalt, as a constellation is something more than or other than the sum of its component stars. A sentence or phrase is categorized into an entity by punctuation marks, a paragraph by other conventions.

Each letter also of a word is assembled according to a certain convention: in Europe left is temporally antecedent to right. Left is past, right is future. The same for top and bottom of the page: above is before, below is after.

One or two conventions for tone of voice, and conventions for orthography & sentence construction are also established: exclamation mark & question mark & symbols of emphasis. And inherent in all these conventions, the aesthetic form which has arisen as the solution required to complete the configuration or gestalt in a way satisfactory to the structure of the human mind.

These conventions were established and forgotten long before the invention of printing: they were however the problem. Well adapted & acceptable for books in which component words were neutral in feeling and used as counters in word-play, they were clearly not suitable where sound was as important as meaning, as in all literature. Nor were they suitable for the expression of certain scientific ideas, particularly in mathematics.

When conventions are unsuitable to the formulation of ideas, either the idea or the convention is changed or rejected. Blake was obliged to introduce a parallel isomorphic concomitant to his poems, in the form of pictures; and obliged to develop his own technic to produce his books. Apollinaire was obliged to reject traditional stanza forms and the left-to-right top-to-bottom conventions in order to express his concepts. Not only poets: also scientists.With the introduction ofthe concept of zero and its symbolic formulation was implied the gestalt of positional notation and the rejection of and-summation.The figures 1, 2, 3 meant entirely different ideas if assembled into the differing sequences 123, 132, 213, 231, 312, 321. There was more similarity between 101 and 109, although their component digits differ, than between 101 and 011. The former pair are approximately equal on the scale of hundreds, each a little over; the latter pair have no pretension to equality. With tabular matter, the left-to-right convention was destroyed: while a list of words may be arranged from a left-hand margin, the list of figures is disposed to the left and to the right of a zero position.

Although subject to these attacks, the book has generally remained as first conceived five hundred years ago. As a sponge soaks up water, and a word an emotional content to which it was not designed, so these conventions have absorbed the new concepts rather than the new concepts determining new conventions & new solutions, But new solutions are required: the fact that books on mathematics & balance sheets & tabular matter generally are so ugly means that the good gestalt, the beautiful equation, the required solution, is not employed. The book is a pro-crustean bed,

It is not only that the basic concepts of the book have not been examined: the history of society & industrial organization for the last five hundred years have contributed. The emphasis on mass-production & speed in printing has, in our acquisitive society, demanded the accelerating concretion of production into more rigid & specialized & centralized patterns.The division of labour, absence of responsibility for the finished product or its original conception implies that the artisan who works under such organization is totally split.The printer is now many men, many separate & codified trades: the man is factory & home, work & play, getting & spending. Work implies one work, highly specialized, developing but one side of the personality & ability and suppressing & atrophying & perverting all others into expression as aggression & war & the problem of leisure. In the archetype of our society, no man is a responsible or creative individual; every man is trained from birth by environmental pressure not to wish to be responsible or creative.

The emphasis on technical & mechanical development has changed the tool, powered and controlled by the individual, into the machine, powered by exterior energy and controlled only within the limits of freedom given by its designer. The machine is not so much the extension as the concretion of the hand.The hand may be trained to be as creative as the mind with Which it is symbiotic; each machine is designed as a solution to a problem. Even in its most highly developed forms, such as ENIAC (the ‘electronic brain’), the machine is based on principles of behaviourism; the symptom of the mind is its capacity of configuri-1tion.The mind produces gestalten, the machine and-summations. 

For the new problems of configuration & conventions which are good solutions, tl1e highest mechanical degree of freedom is required. For the tabulation of known or discoverable results to problems, Babbage’s calculating machine, itself printing its own results, not subject to human error in transcription, is the prototype: for creative activity, the mind & hand & tool & machine-tool are better adapted than automatics & telemechanics. It might be considered possible to solve the problems by giving more power to the bastard typographer. But himself schizoid, short-haired, ill-cultured, irresponsible & mad, typical assimilator of the machine, he is obliged to act as an authoritarian, imposing his own superego on others and unable to carry out the work he designs. Solutions rarely arise except by contact with & sensual experience of the material.

Workshop organization is therefore required: the freedom of hand-setting, simplicity of equipment, responsibility & self-discipline in the artisan. Hand composition & press-work, rather than machine keyboard & machine-minding, may limit output, but a society interested only in output does not require or demand many copies of experimental work. Such work will not only be attempts at solutions to problems in gestalt psychology: the poet & scientist also demand new solutions. Society is so split that the number of copies of fundamental & original work must be few; in any case this schizoid environment generally holds the new solution to be seditious & pornographic where it is not totally obscure.
-->
