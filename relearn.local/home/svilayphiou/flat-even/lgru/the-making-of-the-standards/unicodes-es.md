Type: inproceedings
Authors: Denis Jacquerye
Translator: Kamen Nedev
Title: Unicodes
Date: 2012
Original: http://aa.lgru.net/pages/LGRUdenisJacquerye/
License: CC-BY
Lang: es

about='http://sound.constantvzw.org/LGRU/02-24-12_Denis-Moyogo-Jacquerye.ogg'

<div markdown=true class="original">
00:00:03,911 -->

Así que mi presentación es sobre la lucha de algunas personas por usar tipografía en sus idiomas, especialmente en el caso de tipografía digital. Porque este universo de la tipografía digital se compone de un conjunto de elementos bastante complejo.

00:00:37,520 -->

Así que una de las cosas básicas que hace la gente cuando quiere usar su propio idioma, acaban con este tipo de problemas que tenemos aquí, cuando ciertas letras se ven, otras no, y a veces no se corresponden con la fuente. Porque una fuente tiene una de las letras que necesitan, pero otras no. Como, por ejemplo, aquí, esta fuente tiene la letra en mayúscula, pero no en minúscula. Así que los usuarios realmente no saben cómo apañarse con esto, se limitan a probar fuentes diferentes, y, si son valientes, van a la red y aprenden a quejarse por ello. Así que muy a menudo, terminan quejándose a los desarrolladores - me refiero a los diseñadores de tipografía, o ingenieros. Y esta gente resuelve los problemas lo mejor que sabe. Pero a veces es bastante difícil averiguar cómo resolver estos problemas. Añadir letras que faltan es bastante fácil, pero....

00:01:59,759 -->

Y, luego, a veces, hay unos requisitos de idiomas que son muy complejos. Por ejemplo, aquí, en polaco, tenemos el ogonek, que es como una pequeña cola, que indica que una vocal es nasalizada. De hecho, la mayoría de fuentes tienen esa letra, y, en algunos idiomas, la gente está acostumbrada a tener esta pequeña cola centrada, y es muy raro ver una fuente que la tenga. Así que cuando los diseñadores tipográficos se enfrentan a este problema, no tienen más opción que seguir una tradición u otra, y si quieren seguir un camino, han perdido a esta gente. También hay problemas en cómo quieres mantener el espaciado de manera diferente, porque aquí tenemos un apilamiento de diferentes acentos - mejor dicho, diacríticos, o acentos diacríticos. Porque apilarlos hasta esta altura a veces se mete en la línea anterior, así que a menudo tienes que buscar una solución para relajar una línea, y, luego, en algunos idiomas, en lugar de apilarlos, acaban colocándolos uno al lado del otro. Que es otro momento en el que tienes que tomar una decisión.

00:03:55,467 -->

Pero, en general, estas cosas se basan en cómo los ordenadores representan la tipografía. Solíamos tener codificaciones sencillas, en las que teníamos ASCII, el alfabeto latino occidental básico, y cada letra se representaba en bytes. Teníamos los bytes que representaban una letra, y la letra se presentaría en fuentes distintas, con estilos diferentes, y podían satisfacer los requisitos de gente diferente, y luego hicieron un montón de otras codificaciones, porque había muchos requisitos, y es bastante difícil meter todo esto en ASCII.

00:04:55,127 -->

Así que, a menudo, empezarían con ASCII, y luego añadirían requisitos específicos, pero terminaron con un montón de estándares diferentes, debido a todas las necesidades diferentes. Así que un byte único tendría significados diferentes, y cada uno de estos significados se representaría de manera diferente en tipografía. Pero muy a menudo, cuando ves páginas web antiguas, muy a menudo ves que están usando codificaciones antiguas. Si tu navegador no usa la codificación correcta, tendrías un montón de sinsentido en la pantalla, debido al caos de las codificaciones. Así que, a finales de los 80, empezaron a pensar en estos problemas, y en los 90 empezaron a trabajar en Unicode, un montón de empresas se juntaron y trabajaron en un único estándar unificador que sería compatible con todos esos otros estándares y los que estén por venir.

00:06:12,541 -->

En Unicode, todo está bastante bien definido, tienes un punto universal que representar, para identificar una letra, y luego esta letra se puede representar con diferentes glifos en función de la fuente o estilo seleccionados. Así que, con esta plataforma, cuando necesitas que una letra se represente correctamente, tienes que buscar este punto en un editor de tipografía y cambiar la forma de la letra, y puedes representarla bien. Y luego, a veces, simplemente, no hay un punto que represente la letra, y esto pasa porque todavía no ha sido añadida, no estaba en ningún estándar previo, o a nadie le había hecho falta antes, o la gente que la necesitaba sólo usaba la imprenta tradicional y tipos de plomo.

00:07:33,909 -->

Así que, para ello, tienes que empezar a tratar con Unicode, con la organización en sí. Tienen varios canales de comunicación, como la lista de correo, y recientemente han abierto un foro donde puedes hacer preguntas sobre las letras que necesites, porque puede que no consigas encontrarlas.

00:07:59,175 -->

Supongo que el mapa de letras os es familiar. Muy a menudo, tenemos aplicaciones con las que acceder a todas las letras que existen en Unicode, o aquellas que están disponibles en la fuente que estemos usando. Así que es bastante difícil encontrar lo que necesitamos, la mayoría de las veces es... porque todo está organizado siguiendo un juego de reglas muy restrictivo. Así que la mayoría de las veces las letras están ordenadas tal y como están ordenadas dentro de Unicode, o sea, siguiendo el orden de sus puntos de código: así que la A mayúscula es 41, y luego B es 42, etc. Luego, cuando más avanzas, más lejos vas en los bloques y tablas de Unicode. Y allí hay muchos sistemas de escritura diferentes. Pero, también, ya que Unicode se está expandiendo de manera orgánica, se trabaja primero en un script, luego en otro, luego vuelven al primero y añaden cosas, así que las cosas no están en un orden lógico o práctico. Como, por ejemplo, aquí, el Latin Básico llega hasta aquí, y aquí tenemos Latin Extendido A, Latin Extendido B, C, y D. Pero están bastante lejos uno de otro en Unicode, y cada uno de ellos está organizado de manera diferente, aquí tenéis una letra mayúscula a solas, y aquí tenéis una mayúscula y una minúscula. Así que, si sabes qué letra quieres usar, a veces puede que sólo encuentres la versión en mayúscula, y luego tienes que seguir buscando la versión en minúscula, dependiendo de la aplicación que estés usando, puede que encuentres la información enseguida, mirad aquí, la minúscula es tan difícil de encontrar, está en Fonética Internacional.

00:10:56,931 -->

Básicamente, cuando tienes una letra que, simplemente, no encuentras, preguntas en la lista de correo o en los foros, y la gente te puede decir si merecerá la pena que se incluyera en Unicode o no. Y luego, si estás muy motivado, puedes intentar satisfacer los criterios para inclusión, pero, para la inclusión propiamente dicha, también tiene que haber una propuesta formal, tienen una plantilla con preguntas que puedes responder, y también tienes que proporcionar pruebas de que las letras que quieres añadir se usan realmente, o de cómo se usarían. Estos criterios son bastante complicados porque... tienes que asegurarte de que la letra no sea una variante glífica, o sea, que no sea lo mismo representado de otra manera. O simplemente una forma diferente con la misma idea, que se seguiría usando más o menos de la misma manera. Y luego también tienes que demostrar que no sea una letra que ya exista, porque a veces simplemente no sabes si es una variante de otra letra, o a veces ellos quieren ponérselo más fácil y dicen que es una variante aunque tú no lo consideres así.
Tienes que asegurarte de que no sea solamente una ligadura, porque a veces las ligaduras se usan como una letra única, pero no lo son, y a veces sí lo son. Tienes que proporcionar una fuente con la letra para que ellos puedan usarla en su documentación.

00:13:08,048 -->

FS: ¿Cuánto se suele tardar en hacer eso, normalmente?

DJ: Supongo que depende, porque a veces, si proporcionas suficientes pruebas y lo explicas bien, lo aceptan directamente, pero muy a menudo te piden que hagas revisiones de las propuestas, y a veces las rechazan directamente porque no se ajustan a los criterios. De hecho, estos criterios han cambiado un poco en el pasado, porque al principio, en Unicode, tenías... Empezaron con el Latin Básico, y luego empezaron a añadir letras especiales que la gente usaba: aquí por ejemplo tenéis el alfabeto fonético internacional, pero también están las letras acentuadas... Porque al principio se dieron cuenta de que esas letras se usaban en otras codificaciones, y sólo quieren ser compatibles con todo lo ya preexistente, así que las añaden. Y también, ya se habían dado cuenta de que ya estaban aquellas letras acentuadas de otras codificaciones, así que añaden las que saben que se están usando, aunque todavía no estén codificadas. Y es por es que tenemos cosas como nomenclaturas diferentes, porque ellos seguían procedimientos diferentes al principio, en lugar de tener un procedimiento único, como ahora. Así que añadieron un montón de letras latinas con marcas que se usaban, por ejemplo, en transcripciones. Así que, si, por ejemplo, estás transcribiendo sánscrito, usarías algunas de las letras aquí. Luego, en un momento dado se dieron cuenta de que esa lista de letras acentuadas se iba a hacer inmensa, y de que tenía que haber una manera más inteligente de hacerlo. Y se dieron cuenta de que, en realidad, podían usar sólo partes de esas letras, porque pueden descomponerlas en, por ejemplo, la letra base y las marcas que vayas añadiendo. Así que ahora hay compatibilidad entre (), así que aquí tenéis una letra única que se puede descomponer canónicamente en la b minúscula y los dos puntos arriba. Y aquí tenéis la letra para el punto de arriba; está en el bloque de marcas diacríticas. Y aquí tenéis todas las marcas diacríticas que ellos consideraron útiles en un momento dado. En ese momento, cuando se dieron cuenta de que acabarían con miles de letras acentuadas, vieron que así tendríamos todas las posibilidades a nuestro alcance, y que a partir de ahora, dirían que si querías una letra acentuada que no haya sido codificada, que simplemente uses las partes correspondientes para representarla. Y en el 96, alguna gente que hablaba Yoruba, un dialecto oral de Nigeria, hicieron una propuesta para añadir letras con diacríticos que necesitaban, y Unicode se limitó a rechazar la propuesta porque ya tenían esa otra forma de componerlas. 

00:16:56,327 -->

FS: ¿Así que los elementos que necesitaban no estaban todavía en la caja de herramientas?

DJ: Sí, los elementos estaban, pero no el... Estas partes de codificación están allí, lo que quiere decir que las letras se pueden representar con Unicode, pero luego el software no lo manejaba bien, así que para los hablantes Yoruba tenía sentido que se codificara en Unicode.

FS: Así que ¿podías escribirlos, pero, claro, tendrías que escribir dos letras?

DJ: Sí, ese es el problema principal. Cómo insertas las letras... Porque a menudo muchos teclados están basados en codificaciones antiguas, en las que tienes letras acentuadas como letras únicas, así que cuando quieres tener una secuencia de varias letras, lo que tienes que hacer es escribir más, o tener un mapa de teclado especial que te permita mapear varias letras en un solo mapa de teclado. Y esto es técnicamente posible, pero es un proceso muy lento si quieres tener todas las posibilidades. Podrías tener un mapa de teclado muy común, de modo que los desarrolladores terminarían añadiéndolo a los mapas de teclado de las aplicaciones que estén desarrollando. Pero luego hay gente con necesidades diferentes, así que hay que volver a hacer el mismo esfuerzo para poder tener disponibles esas secuencias.

00:18:33,250 -->

Dentro de Unicode hay mucha documentación, pero es bastante difícil encontrarla cuando acabas de empezar, y es bastante técnica.  De hecho, la mayor parte de la documentación está en un libro que publican con cada nueva versión. Y ese libro tiene varios capítulos que describen cómo funciona Unicode, y cómo deberían funcionar juntas las letras, qué propiedades tienen. Y todas las diferencias relevantes entre formas de escritura. También tienen casos especiales, en los que intentan cubrir aquellas necesidades que no se han satisfecho, o propuestas que han sido rechazadas. Así que, por ejemplo, tienen varios ejemplos en el libro de Unicode: en algunos sistemas de transcripción tienen esta secuencia de letras, o ligadura: es una T y una S con un enlace de ligadura, y luego un punto encima. Así que la ligadura implica que T y S son pronunciadas a la vez, y el punto de arriba quiere decir que... eh... bueno, tiene otro significado. (Risas). ¡Pero significa algo! Pero, debido a cómo funcionan las letras en Unicode, las aplicaciones lo reordenan todo cada vez que lo introduces, y la ligadura se desplaza detrás del punto. Así que siempre tenéis esta representación, porque tenéis la T en el lugar donde debería estar el punto, y luego debería estar la ligadura y luego la S. Pero la T va primero, el punto está encima de la T, la ligadura va encima de todo, y luego la S se coloca al lado de la T. Y explican cómo hay que insertar la T, la ligadura, y luego la marca diacrítica especial que impide que todo se desordene, y luego añades el punto, y luego puedes hacer la S. Este tipo de uso es fantástico, porque ofrece una solución, pero es muy difícil porque tenéis que insertar cinco letras en lugar de, bueno... cuatro. (Risas).  Aun así, la mayoría de las librerías que renderizan fuentes no lo manejan bien, y la mayoría de las fuentes ni siquiera lo tienen previsto. Unicode hace más cosas: debido a esta separación entre acentos y letras, puedes normalizar cómo se ordenan las cosas. Así que tenéis... Esta secuencia de letras que se pueden reordenar en uno previamente compuesto con un circunflejo, o con lo que sea que uséis para combinar marcas en el orden normalizado. Y todas estas cosas tienen que ser manejadas de alguna manera en las librerías, en las aplicaciones o en las fuentes. Y luego...

00:22:40,479 -->

La propia documentación de Unicode no es prescriptiva, lo que quiere decir que las formas de los glifos no están determinadas. Así que tenéis margen de maniobra para usar el estilo que queráis, o el estilo que vuestro público objetivo quiere. 
Por ejemplo, tenemos diferentes () y diferentes (). Así que Unicode sólo tiene una forma, y es la tarea del diseñador de fuentes el de tener formas diferentes. Unicode tampoco trata de glifos, lo que realmente trata es cómo se representa la información, cómo se visualiza. Aquí, por ejemplo, tenéis dos letras que se visualizan como una ligadura: debido a codificaciones anteriores, en realidad están codificados como una única letra. Pero, incluso en un caso nuevo, Unicode probablemente no propondría la ligadura como una letra única. Y así están las cosas con la compatibilidad, aquí tenéis dos maneras distintas de representar la () circunfleja, y la librería de renderización de fuentes debería ser capaz de posicionar las cosas de forma idéntica, por si sola o con la información proporcionada por la fuente. Y, también, debería realizar un posicionamiento idéntico, relativo, con letras que sólo pueden ser compuestas.

00:24:29,398 -->

Así que toda esta información está allí, en esa esquina. Es muy raro encontrar fuentes que usen esta información para satisfacer las necesidades de las personas que necesitan estas características específicas. Una de las maneras de implementar todas estas características es con True Type, OpenType, y también con algunas alternativas como Graphite que son un subconjunto de las fuentes True Type OpenType. Pero, entonces, necesitarás que tus aplicaciones sean compatibles con Graphite. Así que el único estándar real es True Type OpenType. Está bastante bien documentado, y es muy técnico, porque nos permite hacer muchas cosas para muchas formas de escritura diferentes. Pero el problema que tiene es que sus actualizaciones son lentas, así que si hay un error en la especificación OpenType en curso, tardan bastante en corregirla y que la corrección se vea en tu aplicación. Es bastante flexible, pero algo muy importante es que tiene su propio sistema de codificación de distintas escrituras, lo que quiere decir que hay idiomas identificados que no pueden identificarse en OpenType, así que no puedes... Una de las características de OpenType es... Es algo atrevido decirlo así, pero... Si estoy usando polaco, quiero esta forma, y si estoy usando navajo, quiero esta otra forma. Esto es fantástico, porque entonces puedes hacer la misma fuente tanto para gente hablantes de polaco como para hablantes de navajo, y ellos no tienen que preocuparse de cambiar fuentes, siempre y cuando denominen claramente lo que están haciendo con los idiomas que están usando. Pero, luego, con idiomas que no, con idiomas que no tienen...
Porque en OpenType, simplemente, no tienes esta opción. Porque este otro estándar tiene codificaciones para todos los idiomas, lo cual es normal para un estándar, pero OpenType decidió hacerse su propio estándar, que es diferente, y que no tiene una relación (). Así que es todo muy frustrante, porque tenemos Unicode, y, como podéis ver aquí, podemos encontrar todas las letras en Unicode, pero no está organizado de una manera práctica: tienes que buscar mucho entre las tablas para encontrar las letras usadas por un idioma, y luego tienes que averiguar cómo usarlas en la práctica.

00:27:38,586 -->

Es un caso de una verdadera falta de concienciación dentro de la comunidad de diseño tipográfico. Porque incluso cuando añadieron todas las letras que podamos necesitar, no especificaron su posicionamiento, así que aquí en este ejemplo... Cuando la combináis con un circunflejo, simplemente no se posiciona bien. Y esto ocurre porque muchos diseñadores tipográficos siguen trabajando con la mentalidad de las codificaciones de antes, donde había un códido por una letra acentuada. Y es demasiado difícil. A veces, piensan que con sólo seguir los bloques de Unicode, las tablas de letras Unicode, ya es suficiente. Pero luego tienes problemas en los que, como aquí al principio, la mayúscula está en un bloque y la minúscula en otro. Y luego, se centran en un sólo bloque, no trabajan en el otro bloque porque no lo consideran necesario, y aun así, tenemos dos versiones de la misma letra allí, así que tendría sentido tenerlas en el mismo bloque.

00:29:01,425 -->

Es difícil porque hay muy poca conexión entre el mundo Unicode y la gente que trabaja en OpenType y en cómo se dirige OpenType, y luego están los diseñadores tipográficos, y luego los usuarios.

00:29:38,888 -->

PM: Hay algo que... El principio de la presentación resulta sorprendente, porque indicaste los puntos de codificación de las letras, todas tus letras venían subtituladas por sus puntos de codificación... Pero ¿por qué no usaste sus nombres? En parte, esa es la belleza de Unicode, el nombrarlo todo, cada caracter.

DJ: Bueno, en realidad... Los nombres son bastante largos, y los puntos de codificación son ¿más cortos? (Risas.). Y, por otro lado, algo muy () de Unicode es el hecho de que... Para () tienen esa política según la cual puedes cambiar los nombres de las letras, y tienen una lista de errata en la que se dieron cuenta, «Mierda, no teníamos que haber nombrado esto así, aquí hay un nombre que sí tiene sentido, y el nombre real es un error.»

FS: ¿Puedes darnos el enlace a ese documento?

DJ: ¡Sí!

FS: Pierre se refiere al hecho de ver en las tablas de letras que cada glifo también tiene una descripción. Y estas descripciones son a veces tan abstractas y tan poéticas que fue un punto de arranque para OSP el intentar reimaginar qué tipo de formas se corresponderían a la descripción. Así que «punto de combinación arriba» es la descripción textual de un punto de codificación. Pero, por supuesto, hay miles así, así que hacen unos trucos de gimnasia de los más fantástico para intentar...

DJ: Acaban de añadir un montón de mierda en la última versión...

(Risas).

DJ: Así que esto es la propuesta... Y tienen algo así como un párrafo entero explicando la cosa y tienen todas esas letras... Y luego tienes la descripción en los datos de Unicode, el cómo puedes representar la información sobre las letras. Y luego, normalmente, tienes algunos ejemplos de muestra...

00:32:34,179 -->

NM: Y cuando la gente llega a un proyecto como DéjàVu, tienen que entender todo esto para empezar a contribuir. ¿Cómo tiene lugar este proceso de formación o de educación o de aprendizaje?

DJ: Bueno, normalmente, la gente se interesa por aquello que conoce. Tienen esa necesidad específica, y se dan cuenta de que pueden añadirla a DéjàVu, así que aprenden a jugar con FontForge y... Pasado un tiempo, lo que han hecho es lo suficientemente bueno para que podamos usarlo. Hay gente que da con la fuente y empieza a añadir cosas que no conoce. Por ejemplo, teníamos Ben trabajando en árabe, y la mayoría de las veces él iba trazando cosas y pidiendo feedback en la lista de correo, y luego tuvimos feedback, cambiamos algunas cosas y lo publicamos, y tuvimos más feedback (risas) porque había más gente quejándose. Así que, en buena medida, se trata de ir dibujando a base de los recursos que puedas encontrar. Muchas veces, el trabajo se base en otras tipografías. Pero, a veces, te limitas a copiar errores de otras tipografías... Así que, al final, lo único útil es el feedback de los usuarios, porque al menos es algo con lo que puedas contar. Cuando la gente está usando algo, probándolo, entonces sabes que puedes mejorarlo.

NM: ¿Cuánto saben? ¿Cuánto necesitan saber de Unicode, por ejemplo, para poder empezar?

DJ: Sólo necesitan saber dónde va una letra, qué punto de codificación. Y, luego, con el resto de características, intentamos ayudar todo lo que podamos, pero nos damos cuenta de que falta algo. FontForge es bastante complejo, porque es compatible con muchas de las características de OpenType. Es sólo que es muy complicado añadirlas. Así que, en realidad, ellos no tienen que pelearse con ello... Hay gente que pregunta cómo puede usar esas características de OpenType, y nosotros lo hacemos por ellos, o lo hacemos y luego les explicamos cómo, o, simplemente, no sabemos cómo hacerlo.  (Risas). ...Que es lo que ocurre, probablemente, en la mayoría de los casos.

PM: ¿Estás usando archivos con características, o la interfaz de FontForge?

DJ: Ah, sólo la interfaz. Supongo que usar archivos sería más fácil, pero, a veces... De hecho, algunos de nosotros usamos scripts, así que en lugar de un archivo, uso scripts, porque te permite hacer... Te permite... ¿¿?? Después de un tiempo, así que es mejor hacer scripts. Supongo que también se podría usar un script para generar un archivo de características que también funcione.
</div>
