Type: article
Authors: Mailing list www-talk
Title: Proposed New Tag: IMG
Date: 1993
Original: http://1997.webhistory.org/www.lists/www-talk.1993q1/0182.html
License: Copyright © 2003 The World Wide Web History Project and Arcady Press
Lang: en


<div markdown=true class="original">
## Thu, 25 Feb 93 21:09:02 -0800   
Marc Andreessen (marca@ncsa.uiuc.edu)

I'd like to propose a new, optional HTML tag:

IMG

Required argument is `SRC="url"`.

This names a bitmap or pixmap file for the browser to attempt to pull
over the network and interpret as an image, to be embedded in the text
at the point of the tag's occurrence.

An example is:

`<IMG SRC="file://foobar.com/foo/bar/blargh.xbm">`

(There is no closing tag; this is just a standalone tag.)

This tag can be embedded in an anchor like anything else; when that
happens, it becomes an icon that's sensitive to activation just like a
regular text anchor.

Browsers should be afforded flexibility as to which image formats they
support. Xbm and Xpm are good ones to support, for example. If a
browser cannot interpret a given format, it can do whatever it wants
instead (X Mosaic will pop up a default bitmap as a placeholder).

This is required functionality for X Mosaic; we have this working, and
we'll at least be using it internally. I'm certainly open to
suggestions as to how this should be handled within HTML; if you have
a better idea than what I'm presenting now, please let me know. I
know this is hazy wrt image format, but I don't see an alternative
than to just say ``let the browser do what it can'' and wait for the
perfect solution to come along (MIME, someday, maybe).

Let me know what you think.........

Cheers,    
Marc

--    
Marc Andreessen    
Software Development Group    
National Center for Supercomputing Applications    
marca@ncsa.uiuc.edu




## Thu, 25 Feb 1993 23:13 PDT
Tony Johnson (TONYJ@scs.slac.stanford.edu)

>I'd like to propose a new, optional HTML tag:
>
> IMG
>
>Required argument is SRC="url".
>
>This names a bitmap or pixmap file for the browser to attempt to pull
>over the network and interpret as an image, to be embedded in the text
>at the point of the tag's occurrence.
>
>An example is:
>
> `<IMG SRC="file://foobar.com/foo/bar/blargh.xbm">`
>
>(There is no closing tag; this is just a standalone tag.)

I have something very similar in Midas 2.0 (in use here at SLAC, and due for
public release any week now), except that all the names are different, and it
has an extra argument `NAME="name"`. It has almost exactly the same
functionality as your proposed IMG tag. e.g.

`<ICON name="NoEntry" href="http://note/foo/bar/NoEntry.xbm">`

The idea of the name parameter was to allow the browser to have a set of
"built in" images. If the name matches a "built in" image it would use that
instead of having to go out and fetch the image. The name could also act as a
hint for "line mode" browsers as to what kind of a symbol to put in place of
the image.

I don't much care about the parameter or tag names, but it would be sensible
if we used the same things. I don't much care for abbreviations, ie why not
`IMAGE=` and `SOURCE=`. I somewhat prefer `ICON` since it imlies that the `IMAGE`
should be smallish, but maybe ICON is an overloaded word?

>Browsers should be afforded flexibility as to which image formats they
>support. Xbm and Xpm are good ones to support, for example. If a
>browser cannot interpret a given format, it can do whatever it wants
>instead (X Mosaic will pop up a default bitmap as a placeholder).

I was proposing to use the file extension (.xbm above) to tag what format the
image was in, but with the intention that in future, when HTTP2 comes along,
the same format negotiation technique would be used to access images.

Xbm and Xpm sound resonable to me, but do they sound reasonable for non-X
based applications??

While we are on the subject of new tags, I have another, somewhat similar tag,
which I would like to support in Midas 2.0. In principle it is:

`<INCLUDE HREF="...">`

The intention here would be that the second document is to be included into
the first document at the place where the tag occured. In principle the
referenced document could be anything, but the main purpose was to allow
images (in this case arbitrary sized) to be embedded into documents. Again the
intention would be that when HTTP2 comes along the format of the included
document would be up for separate negotiation.

An alternative I was considering was:

`<A HREF="..." INCLUDE>See photo</A>`

I don't much like adding more functionality to the `<A>` tag, but the idea here
is to maintain compatibility with browsers that can not honour the `INCLUDE`
parameter. The intention is that browsers which do understand `INCLUDE`, replace
the anchor text (in this case "See photo") with the included document
(picture), while older or dumber browsers ignore the `INCLUDE` tag completely.

Comments?????

Tony






## Fri, 26 Feb 93 14:04:55 +0100
Tim Berners-Lee (timbl@www3.cern.ch)

I had imagined that figues would be reprented as

`<a name=fig1 href="fghjkdfghj" REL="EMBED, PRESENT">Figure </a>`

where the relation ship values mean

`EMBED` Embed this here when presenting it
`PRESENT` Present this whenever the source document
is presented

Note that you can have various combinations of these, and if
the browser doesn't support either one, it doesn't break.

A see that using this as a method for selectable icons means nesting
anchors. Hmmm. But I hadn't wanted a special tag.
Annother way would be to declare an entity with `SYSTEM` attribute
giving the URL and then invoke that entity, but that would mean an
extendable entity dictionary which is not currently needed.


## Fri, 26 Feb 1993 10:07:34 -0500
Jim Davis (davis@dri.cornell.edu)


I like the functionality of Marc's proposal. (I am indifferent
to the syntax, so long as we get a standard.) While I daresay
that HTPP2 and Mime will oneday achieve the same power and more,
I like the idea of getting it soon.

Note that Tim's proposal

`<a name=fig1 href="fghjkdfghj" REL="EMBED, PRESENT">Figure </a>`

is less general, since it can only be used inside anchors. (Maybe
I don't understand it, though... do you have to click on the
anchor, or does `PRESENT` mean that it is "automatically" clicked?
If so, what happens to the text between the `A` and `/A` ?

As I understand it, I can use Marc A's scheme for inline pictures
(which might be big) or icons in anchors.

As for specifics of Marc `A`'s proposal, two comments

1) why have `SRC` instead of `HREF`?

2) It would be nice if there was a way to specify the content type,
e.g.

`<IMG HREF="http://nsa.gov/pub/sounds/gorby.au" CONTENT-TYPE=audio/basic>`

But I am completely willing to live with the requirement that I specify
the content type by file extension. It's good enough for me.


## Fri, 26 Feb 93 13:29:45 -0800
Marc Andreessen (marca@ncsa.uiuc.edu)


Jim Davis writes:
> As for specifics of Marc `A`'s proposal, two comments
>
> 1) why have `SRC` instead of `HREF`?

Because I wanted to avoid overloading `HREF` -- doesn't really make
sense in this context, I don't think.

> 2) It would be nice if there was a way to specify the content type,
> e.g.
>
> `<IMG HREF="http://nsa.gov/pub/sounds/gorby.au" CONTENT-TYPE=audio/basic>`
>
> But I am completely willing to live with the requirement that I specify
> the content type by file extension. It's good enough for me.

Actually, in Mosaic this will be possible by doing something like:

`<A HREF="http://nsa.gov/pub/sounds/gorby.au"><IMG SRC="myimage.xbm></A>`

Simple.....

Marc




## Fri, 26 Feb 93 18:12:45 +0100
Tim Berners-Lee (timbl@www3.cern.ch)


> Date: Fri, 26 Feb 1993 10:07:34 -0500
> From: Jim Davis <davis@dri.cornell.edu>
>

> I like the functionality of Marc's proposal. (I am indifferent
> to the syntax, so long as we get a standard.) While I daresay
> that HTPP2 and Mime will oneday achieve the same power and more,
> I like the idea of getting it soon.
>

> Note that Tim's proposal
>

> `<a name=fig1 href="fghjkdfghj" REL="EMBED, PRESENT">Figure </a>`
>

> is less general, since it can only be used inside anchors.

No ... it is just that the thing is seen as a type of link rather
than something totally new. Note that in the Microsoft world the
work "Link" always mean "embed the thing referred to here" --
this explains much of the confusion when explaining W3 to windows
users.

If you consider that often the reader (rather than the author) might
want to chose which figues are expanded inline rather than put in a
separate window, and also that the reader (rather tham the author)
might want to chose whether related things are PRESENTed
automatically or not, then the use of `<A>` with switches looks
more appropriate. As Tony points out, an advantage is that it will
be handled relatively well by browsers which can't do it or
old ones which don't understand it.

> (Maybe
> I don't understand it, though... do you have to click on the
> anchor, or does `PRESENT` mean that it is "automatically" clicked?

You didn't quite but you do now. yes ... `PRESENT` means
`CONSIDER-THIS-CLICKED`.
> If so, what happens to the text between the `A` and `/A` ?

It is replaced by the object because of the `EMBED` relationship.

> As I understand it, I can use Marc `A`'s scheme for inline pictures
> (which might be big) or icons in anchors.

If you allow an image, then suppose we also allow some content which
includes anchors with x,y coordinates within the image. Then the
document can intercept mouse clicks and allow hypergraphics

I don't want to change HTML now if I can help it, until it has gone
to RFC track

Tim


## Fri, 26 Feb 93 13:32:09 -0800
Marc Andreessen (marca@ncsa.uiuc.edu)


Tim Berners-Lee writes:
> I don't want to change HTML now if I can help it, until it has gone
> to RFC track

I absolutely agree in all cases -- my purpose in suggesting IMG is
that things are reaching the point where some browsers are going to be
implementing this feature somehow, even if it's not standard, just
because it's the logical next step, and it would be great to have
consistency from the beginning -- so that when HTML2 comes along,
we're all still in lockstep.....

Cheers,    
Marc



## Fri, 26 Feb 93 10:28:38 PST
Jay C. Weber (weber@eitech.com)


While images are at the top of my list of desired medium types in a
WWW browser, I don't think we should add idiosyncratic hooks for media
one at a time. Whatever happened to the enthusiasm for using the
MIME typing mechanism? I made a concrete proposal a few months ago,
where HREFs can point to other parts in a MIME multipart (and thereby
to an "external-body"), and I've seen a similar idea recently regarding
embedding media clips in a "simplemail" format. Don't we want to
take on something like this in HTML2?

Jay    
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Jay C. Weber weber@eitech.com    
Enterprise Integration Technologies weber@cis.stanford.edu   
459 Hamilton Avenue, Suite ##100 (415)617-8002   
Palo Alto, CA 94301





## Fri, 26 Feb 93 14:00:12 PST
Jay C. Weber (weber@eitech.com)


> From marca@wintermute.ncsa.uiuc.edu Fri Feb 26 11:12:42 1993
>
> Yup. BTW, someone mentioned ``what happened to MIME'' -- this isn't a
> substitute for the upcoming use of MIME as a standard document
> mechanism; this provides a necessary and simple implementation of
> functionality that's needed independently from MIME.

Wait a minute -- let's temporarily forget about MIME, if it clouds the
issue. My objection was to the discussion of "how are we going to
support embedded images" rather than "how are we going to support
embedded objections in various media".

Otherwise, next week someone is going to suggest 'lets put in a new
tag `<AUD SRC="file://foobar.com/foo/bar/blargh.snd">`' for audio.
There shouldn't be much cost in going with something that generalizes.

Jay   
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Jay C. Weber weber@eitech.com   
Enterprise Integration Technologies weber@cis.stanford.edu    
459 Hamilton Avenue, Suite ##100 (415)617-8002   
Palo Alto, CA 94301




## Fri, 26 Feb 93 17:37:29 -0800
Pei Y. Wei (wei@xcf.berkeley.edu)


To: marca@ncsa.uiuc.edu, timbl@nxoc01.cern.ch
Subject: Re: proposed new tag: IMG
Cc: davis@dri.cornell.edu

> > Tim Berners-Lee writes:
> > I don't want to change HTML now if I can help it, until it has gone
> > to RFC track.
>
> Marc wrote:
> I absolutely agree in all cases -- my purpose in suggesting IMG is
> that things are reaching the point where some browsers are going to be
> implementing this feature somehow, even if it's not standard, just
> because it's the logical next step, and it would be great to have
> consistency from the beginning -- so that when HTML2 comes along,
> we're all still in lockstep.....

Yup, yup... Since this is becoming an issue, I might as well mention
that viola too has a number of bitmap tags, but for a DTD other than HTML,
because it'd be a mess to mix such things with the current HTML. At this
point, HTML should stay stable.

Yup, we obviously need to agree upon the namings, etc, from the start,
or it's gonna be a sincere pain to converge later... I don't have any
particular preference for the names/syntax, as long as there is some
accounting for the format used (not by file extension, but preferably
by using MIME format identifiers).

-Pei



## Sat, 27 Feb 93 16:49:48 MET
Tim Berners-Lee (timbl)


Ok, so for HTML2 let's have something for inclusion.
I don't see any reason to limit it to images. Like
it could be more text or whatever.

SGML does provide an official way of doing this folks
and even if Dan C ain't here to round us up we maybe
ought to stick to the track. It might have some useful
spinnoffs like people assuming that any `SYSTEM` attribute,
instead of being a "system defined" path for an
entity, would be a URI.

How about
`<!ENTITY ICON6 SYSTEM "http://blah..">&ICON6;`

Tim BL



## Mon, 1 Mar 93 9:29:50 GMT
Dave_Raggett (dsr@hplb.hpl.hp.com)


> While images are at the top of my list of desired medium types in a
> WWW browser, I don't think we should add idiosyncratic hooks for media
> one at a time. Whatever happened to the enthusiasm for using the
> MIME typing mechanism? I made a concrete proposal a few months ago,
> where HREFs can point to other parts in a MIME multipart (and thereby
> to an "external-body"), and I've seen a similar idea recently regarding
> embedding media clips in a "simplemail" format. Don't we want to
> take on something like this in HTML2?

True indeed! I want to consider a whole range of possible image/line art
types, along with the possibility of format negotiation. Tim's note
on supporting clickable areas within images is also important

Dave Raggett, HPLabs



## Mon, 1 Mar 93 13:05:08 -0800
Marc Andreessen (marca@ncsa.uiuc.edu)


Dave_Raggett writes:
> True indeed! I want to consider a whole range of possible image/line
> art types, along with the possibility of format negotiation. Tim's
> note on supporting clickable areas within images is also important

Actually, maybe we should think about a general-purpose procedural
graphics language within which we can embed arbitrary hyperlinks
attached to icons, images, or text, or anything. Has anyone else seen
Intermedia's capabilities wrt this? It's one of their most impressive
capabilities, actually.

Something like a cross between PostScript and CGM might work...
actually, maybe we should just use one or the other, and add the
extensions we need for the links. Also we'd want to make sure that
it's completely editable.

Cheers,   
Marc

--   
Marc Andreessen   
Software Development Group   
National Center for Supercomputing Applications   
marca@ncsa.uiuc.edu



## Mon, 1 Mar 1993 14:55:57 PST
Bill Janssen (janssen@parc.xerox.com)


Excerpts from ext.WorldWideWeb: 1-Mar-93 Re: proposed new tag: IMG Marc
Andreessen@ncsa.uiu (884)

> Actually, maybe we should think about a general-purpose procedural
> graphics language within which we can embed arbitrary hyperlinks
> attached to icons, images, or text, or anything. Has anyone else seen
> Intermedia's capabilities wrt this? It's one of their most impressive
> capabilities, actually.

Other systems to look at which have this (fairly valuable) notion are
Andrew and Slate. Andrew is built with \_insets\_, each of which has some
interesting type, such as text, bitmap, drawing, animation, message,
spreadsheet, etc. The notion of arbitrary recursive embedding is
present, so that an inset of any kind can be embedded in any other kind
which supports embedding. For example, an inset can be embedded at any
point in the text of the text widget, or in any rectangular area in the
drawing widget, or in any cell of the spreadsheet. Each ``embedding''
consists of some direct information which specifies the display area of
the embedded information, and a pointer to the actual data object
(actually, in most current usage the embedded data object is directly
contained, but references do in fact work).

This business about embedding the display information is interesting, as
it seems to contradict the standard separation of data and display so
pervasive in Andrew. In Andrew text, embedded insets are thought of as
single characters in rather strange fonts, so the rectangular area
information tells the text display widget the size of the ``character''.
Similarly for embedded insets in the drawing widget.

Bill


## Mon, 1 Mar 93 15:21:36 -0500
Thomas A. Fine (fine@cis.ohio-state.edu)


Here's my opinion.

The best way to do images in WWW is by using MIME. I'm sure postscript
is already a supported subtype in MIME, and it deals very nicely with
mixing text and graphics.

But it isn't clickable, you say? Yes your right. I suspect there is
already an answer to this in display postscript. Even if there isn't
the addition to standard postscript is trivial. Define an anchor
command which specifies the URL and uses the current path as a closed
region for the button. Since postscript deals so well with paths, this
makes arbitrary button shapes trivial.

Let's call this specially modified postscript HTPS. Until HTPS becomes
some sort of standard, every copy of HTPS should include a piece of code
that asks the interpreter if it knows about the anchor command, and if
it doesn't it will substitute in a null command.

As far as implementation, this is easy, though not necessarily trivial.
Ghostscript already provides the ability to bind arbitrary functions to
mouse events. It shouldn't be too horrible to set up anchor to keep
a table of button paths, and have mouse clicks call a function that looks
up what button they are in.

Let me point out that it wouldn't be hard to make such a system deal
with html too. Many people already have code that converts html to
postscript.

tom


## Tue, 2 Mar 1993 23:25:51 +0100
Tim Berners-Lee (timbl@dxcern.cern.ch)


HTTP2 allows a document to contain any type which
the user has said he can handle, not just registered MIME
types. So one can experiment. Yes I think there is a case
for postscript with hypertext.    
I don't know whether display postcript has enough.
I know Adobe are trying to establish their own postscript-based
"PDF" which will have links, and be readable by their
proprietory brand of viewers.

I thought that a generic overlaying language for anchors
(Hytime based?) would allow the hypertext and the graphics/video
standards to evolve separately, which would help both.

Let the `IMG` tag be `INCLUDE` and let it refer to an arbitrary
document type. Or `EMBED` if `INCLUDE` sounds like a cpp include
which people will expect to provide SGML source code to be
parsed inline -- not what was intended.

Tim BL


## Fri, 12 Mar 93 22:32:12 -0800
Marc Andreessen (marca@ncsa.uiuc.edu)


Back to the inlined image thread again -- I'm getting close to
releasing Mosaic v0.10, which will support inlined GIF and XBM
images/bitmaps, as mentioned previously. So I just reread the
exchanges we had a while back, and have a few comments...

Tim Berners-Lee writes:
> Let the `IMG` tag be `INCLUDE` and let it refer to an arbitrary document
> type. Or `EMBED` if `INCLUDE` sounds like a cpp include which people
> will expect to provide SGML source code to be parsed inline -- not
> what was intended.

We're not prepared to support `INCLUDE`/`EMBED` at this point; it raises a
number of nasty issues that are quite separate from the idea of
inlined images. For example, what happens if one EMBEDS a document
that in turn EMBEDS the first document? Oops. Aside from this, I'm
not sure I see the point in allowing arbitrary `EMBED`'s for things like
chunks of texts: this is a hypertext system, after all, and it ought
to be possible to get the functional effect of an `EMBED` by using an
ordinary link. Right?

So, we're probably going to go with `<IMG SRC="url">` (not `ICON`, since
not all inlined images can be meaningfully called icons). For the
time being, inlined images won't be explicitly content-type'd; down
the road, we plan to support that (along with the general adaptation
of MIME). Actually, the image reading routines we're currently using
figure out the image format on the fly, so the filename extension
won't even be significant.

Cheers,   
Marc

--   
Marc Andreessen   
Software Development Group   
National Center for Supercomputing Applications   
marca@ncsa.uiuc.edu



## Sat, 13 Mar 1993 11:56:26 +0100
Guido van Rossum (Guido.van.Rossum@cwi.nl)


>We're not prepared to support `INCLUDE`/`EMBED` at this point; it raises a
>number of nasty issues that are quite separate from the idea of
>inlined images. For example, what happens if one EMBEDS a document
>that in turn EMBEDS the first document? Oops.

I would consider this an error of the author that needs to be detected
to protect the browser. It only requires maintaining a stack of
nested `EMBEDS`.

>Aside from this, I'm
>not sure I see the point in allowing arbitrary `EMBED`'s for things like
>chunks of texts: this is a hypertext system, after all, and it ought
>to be possible to get the functional effect of an `EMBED` by using an
>ordinary link. Right?

Some other hypertext systems do this, in a sense: in Guide there are,
apart from real `GOTO` stype hyperlinks, also "folds" (I think they are
called) which are sort of embedded documents that you can open and
close. The advantage in certain situations is that opening a fold
retains more context than following a link. It feels like using an
outline processor, which is rather pleasant (for certain kinds of
information).

--Guido van Rossum, CWI, Amsterdam `<Guido.van.Rossum@cwi.nl>`

PS. I keep hearing about supporting MIME. Is there consensus on the
form this should take? (I remeber getting in an unpleasant fight with
Dan Connolly about this once -- BTW is he still with us?)
</div>
