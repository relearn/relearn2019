from __future__ import division
from flat import gray, shape, document
from math import pi, cos, sin
from random import normalvariate, random

SIZE, STEPS = 320, 5
cx = cy = SIZE/2
r = min(cx, cy)/STEPS

point = shape().nostroke().fill(gray(0))
boundary = shape().width(0.12)
sigma = document(SIZE, SIZE, 'mm')
page = sigma.addpage()

for i in range(500000):
    angle = 2*pi*random()
    radius = normalvariate(0, 1)*r
    x = cos(angle)*radius + cx
    y = sin(angle)*radius + cy
    page.place(point.circle(x, y, 0.09))

for i in range(3, 4):
    page.place(boundary.circle(cx, cy, r*i))

sigma.pdf('sigma.pdf')