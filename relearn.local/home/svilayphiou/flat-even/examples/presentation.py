from random import choice, random, randint
from flat import gray, rgb, cmyk, spot, diffuse, font, text, mesh, tree, \
    moveto, quadto, curveto, shape, strike, image, scene, group, document, view




d = document(200, 240, 'mm')




# 1 - intro (x xy xyz)

p = d.addpage()
black = gray(0)
thick = shape().stroke(black).width(8)
coordinates = [
    (10, 10, 30, 60), (10, 60, 30, 10),
    (40, 10, 60, 60), (40, 60, 60, 10),
    (70, 10, 80, 35), (80, 35, 90, 10), (80, 35, 80, 60),
    (100, 10, 120, 60), (100, 60, 120, 10),
    (130, 10, 140, 35), (140, 35, 150, 10), (140, 35, 140, 60),
    (160, 10, 180, 10), (160, 60, 180, 10), (160, 60, 180, 60),
    (10, 110, 10, 160), (10, 110, 30, 110), (10, 135, 30, 135),
    (40, 110, 40, 160), (40, 160, 60, 160),
    (80, 110, 90, 160), (70, 160, 80, 110), (74, 140, 86, 140),
    (100, 110, 120, 110), (110, 160, 110, 110)]

for cs in coordinates:
    p.place(thick.line(*[c + random() * 6 - 3 for c in cs]))




# 2 - shapes

p = d.addpage()
for i in range(10):
    s = shape().stroke(black).width(random() * 10).cap('round')
    p.place(s.path(
        moveto(
            random() * 180, random() * 220),
        quadto(
            random() * 180, random() * 220,
            random() * 180, random() * 220)).elevated()).position(10, 10)




# 3 - colors

p = d.addpage()
red1 = rgb(255, 0, 0)
red2 = cmyk(0, 100, 100, 0)
red3 = spot('Pantone 1795', cmyk(0, 96, 90, 2))
s = shape().nostroke().fill(choice((red1, red2, red3)))

p.place(s.rect(10, 10, 180, 220))




# 4 - fonts, kerning, svg

p = d.addpage()
black = rgb(0, 0, 0)
f1 = font.open('src/Lato-Medium.ttf')
f2 = font.open('src/LatoLatinMed-Regular.ttf')
s1 = strike(f1).size(120, 140).color(black)
s2 = strike(f2).size(120, 140).color(black)
p.place(text(
    s1.paragraph(u'VLTAVA'),
    s2.paragraph(u'VLTAVA'))).position(10, 10)
for k in f1.kerning:
    k.clear()
p.svg('presentation-4.svg')




# 5 - fonts, chain

p = d.addpage()
with open('src/metamorphosis.txt') as f:
    rows = iter(f.read().split('\n'))
    title, author = next(rows), next(rows)
    size, leading, columns = choice((
        (9.1, 12.0, 3),
        (14.8, 18.0, 2),
        (24.0, 28.0, 1)))
    regular = font.open('src/Vollkorn-Regular.ttf')
    bold = font.open('src/Vollkorn-Bold.ttf')
    plain = strike(regular).size(size, leading)
    marked = strike(bold).size(size, leading)
    step = 185 // columns
    t = p.place(text(
        plain.paragraph(author),
        marked.paragraph(title),
        *map(plain.paragraph, rows))).frame(10, 10, step - 5, 220)
    for i in range(1, columns):
        t = p.chain(t).frame(10 + step * i, 10, step - 5, 220)




# 6 - fonts, rasterize

p = d.addpage()
q = document(200, 240, 'mm').addpage()
f = font.open('src/SourceCodePro-Semibold.otf')
s = strike(f).size(16, 20.2)
glyphs = ' '.join(unichr(code) for code, index in f.charmap.items())
q.place(s.outlines(glyphs)).frame(10, 10, 180, 230)

p.place(q.image(72)).fitwidth(200)




# 7 - scene

p = d.addpage()
vertices, faces = [None], []
for line in open('src/bunny.obj', 'r'):
    if not line.startswith('#'):
        parts = line.split()
        if parts:
            if parts[0] == 'v':
                vertices.append((
                    float(parts[1]),
                    float(parts[2]),
                    float(parts[3])))
            elif parts[0] == 'f':
                faces.append((
                    int(parts[1]),
                    int(parts[2]),
                    int(parts[3])))
s = scene()
dark = diffuse((0.2, 0.2, 0.2))
light = diffuse((0.7, 0.7, 0.7))
dim = diffuse((0.7, 0.7, 0.7), (100.0, 100.0, 100.0))
bright = diffuse((0.7, 0.7, 0.7), (1000.0, 1000.0, 1000.0))
s.camera((-0.05, 0.28, 0.25), (-0.04, 0.102, 0.02), 35)
s.environment((0.0906, 0.0943, 0.1151), (0.1, 0.09, 0.07))
s.add(mesh(*(
    (vertices[a], vertices[b], vertices[c]) for a, b, c in faces)), light)
s.add(mesh(
    ((-900, 0.034, -900), (900, 0.034, -900), (900, 0.034, 900)),
    ((-900, 0.034, -900), (900, 0.034, 900), (-900, 0.034, 900))), dark)
s.add(mesh(
    ((150, 100, 0), (150, 200, 0), (150, 200, -100)),
    ((150, 100, 0), (150, 200, -100), (150, 100, -100))), bright)
s.add(mesh(
    ((-150, 100, 0), (-150, 200, 0), (-150, 200, 100)),
    ((-150, 100, 0), (-150, 200, -100), (-150, 100, 100))), dim)

# i = s.render(180, 220, 10).tonemapped()
# p.place(i).position(10, 10).fitwidth(180)




# 8 - image

p = d.addpage()
i = image.open('src/david.png')
i1 = i.copy()
i2 = i.copy()
i3 = i.copy()
i4 = i.copy()
i1.scale(0.25)
i2.blur(20)
i3.dither()
i4.invert().flip(True, False)

p.place(i1).position(10, 10).fitwidth(85)
p.place(i2).position(105, 10).fitwidth(85)
p.place(i3).position(10, 123).fitwidth(85)
p.place(i4).position(105, 123).fitwidth(85)




# 9 - tree

api = tree((
    'flat', (
        'document', (
            'page', (
                'placedimage', (
                    'image',),
                'placedshape', (
                    'line', (
                        'shape', (
                            'stroke', (
                                'gray',
                                'ga',
                                'rgb',
                                'rgba',
                                'cmyk',
                                'spot',
                                'devicen',
                                'overprint'),
                            'fill ...')),
                    'polyline ...',
                    'polygon ...',
                    'rect ...',
                    'circle ...',
                    'ellipse ...',
                    'path', (
                        'shape ...',
                        'commands', (
                            'moveto',
                            'lineto',
                            'quadto',
                            'curveto',
                            'closepath')),),
                'placedtext', (
                    'text', (
                        'paragraph', (
                            'span', (
                                'strike', (
                                    'font',
                                    'textcolor ...'),
                                'string')))),
                'placedoutlines', (
                    'outlines ...',),
                'placedgroup', (
                    'group',)),),
        'scene', (
            'mesh',
            'diffuse'),
        'raw',
        'tree', (
            'tree',),
        'view', (
            'flat',))))

p = d.addpage()
black = rgb(0, 0, 0)
blue = rgb(25, 51, 229)
bold = font.open('src/Inconsolata-Bold.ttf')
drawing = shape().stroke(blue).width(0.8)
caption = strike(bold).size(6, 8).color(black)
space = caption.width(' ') / p.k
half = caption.ascender() * 0.7 / p.k
api.flip().frame(10, 10, 160, 140)
for node in api.nodes():
    x2, y2 = node.x, node.y
    p.place(caption.text(node.item)).position(x2, y2 - half)
    if node.parent:
        x0, y0 = node.parent.x, node.parent.y
        x0 += space * len(node.parent.item) + space
        x2 -= space
        p.place(drawing.path(
            moveto(x0, y0),
            curveto(
                (x0 + x2) * 0.5, (y0 + y2) * 0.5,
                (x0 + x2) * 0.5, y2,
                x2, y2)))




# 10 - diagrams

def plot(data, labelx, sublabelx, labely, sublabely, labelz, sublabelz):
    
    width, height = 130, 130
    magnitude = 1.0
    depth = 2.0 ** 0.5 * 0.5
    antidiagonal = False
    left, top, right, bottom = 20, 20, 40, 20
    distance = 5.0
    
    sublabelz %= magnitude
    
    black = rgb(0, 0, 0)
    regular = font.open('src/Montserrat-Regular.ttf')
    bold = font.open('src/Montserrat-Bold.ttf')
    thin = shape().stroke(black).width(0.25)
    thick = shape().stroke(black).width(1.25)
    label = strike(regular).size(10, 12).color(black)
    labelbold = strike(bold).size(10, 12).color(black)
    g = group()
    
    m, n = len(data), len(data[0])
    w, h = n - 1.0, m - 1.0
    k = width / (w + depth * h)
    dx, dy = 0, height - h * k
    norm = (height - h*depth*k) / (magnitude*k)
    magnitude *= norm
    dx += left
    dy += top
    
    assert norm > 0, 'Diagram height too small.'
    
    if antidiagonal:
        points = [[((x+y*depth)*k, (h-y*depth-data[y][x]*norm)*k)
            for x in range(n)] for y in range(m)]
    else:
        points = [[((x+(h-y)*depth)*k, (h-y*depth-data[y][x]*norm)*k)
            for x in range(n)] for y in range(m)]
    
    for y in range(1, m - 1):
        coordinates = []
        for x in range(n):
            coordinates.extend(points[y][x])
        g.place(thick.polyline(*coordinates)).position(dx, dy)
    for x in range(1, n - 1):
        coordinates = []
        for y in range(m):
            coordinates.extend(points[y][x])
        g.place(thick.polyline(*coordinates)).position(dx, dy)
    
    coordinates = []
    for x in range(0, n - 1):
        coordinates.extend(points[0][x])
    for y in range(0, m - 1):
        coordinates.extend(points[y][n - 1])
    for x in reversed(range(1, n)):
        coordinates.extend(points[m - 1][x])
    for y in reversed(range(1, m)):
        coordinates.extend(points[y][0])
    g.place(thick.polygon(*coordinates)).position(dx, dy)
    
    x0, y0 = 0, h*k
    x1, y1 = w*k, h*(1-depth)*k
    x2, y2 = (w+h*depth)*k, (h*(1-depth)-magnitude)*k
    x3, y3 = h*depth*k, (h-magnitude)*k
    if antidiagonal:
        g.place(thin.polygon(x0, y0, x1, y0, x2, y1, x2, y2, x3, y2, x0, y3)).position(dx, dy)
        g.place(thin.polyline(x0, y3, x1, y3, x1, y0)).position(dx, dy)
        g.place(thin.line(x1, y3, x2, y2)).position(dx, dy)
        g.place(thin.polyline(x3, y2, x3, y1, x2, y1)).position(dx, dy)
        g.place(thin.line(x0, y0, x3, y1)).position(dx, dy)
        ax, ay = (x0+x1)*0.5, y0
        bx, by = x2, (y1+y2)*0.5
        cx, cy = (x1+x2)*0.5, (y0+y1)*0.5
        dist = distance - labelbold.leading*0.1 / p.k
    else:
        g.place(thin.polygon(x3, y0, x2, y0, x2, y3, x1, y2, x0, y2, x0, y1)).position(dx, dy)
        g.place(thin.polyline(x3, y0, x3, y3, x2, y3)).position(dx, dy)
        g.place(thin.line(x0, y2, x3, y3)).position(dx, dy)
        g.place(thin.polyline(x0, y1, x1, y1, x1, y2)).position(dx, dy)
        g.place(thin.line(x1, y1, x2, y0)).position(dx, dy)
        ax, ay = (x2+x3)*0.5, y0
        bx, by = x2, (y0+y3)*0.5
        cx, cy = (x1+x2)*0.5, (y2+y3)*0.5
        dist = -distance
    
    ex = labelbold.width(labelx)*0.5 / p.k
    fx, fy = labelbold.textsize*0.1 / p.k, labelbold.leading*0.5 / p.k
    gy = labelbold.ascender() / p.k
    g.place(text(
        labelbold.paragraph(labelx),
        label.paragraph(sublabelx))).position(ax+dx - ex, ay+distance+dy)
    g.place(text(
        labelbold.paragraph(labely),
        label.paragraph(sublabely))).position(cx+distance+dx, cy+dist+dy - gy)
    g.place(text(
        labelbold.paragraph(labelz),
        label.paragraph(sublabelz))).position(bx+distance+dx + fx, by+dy - fy)
    
    return g

p = d.addpage()
from json import load
names = 'naive', 'dynamic', 'shortest', 'binary', 'linear', 'divide'
with open('src/%s.json' % choice(names)) as f:
    j = load(f)
    g = plot(
        j['data'],
        j['labelx'], j['sublabelx'],
        j['labely'], j['sublabely'],
        j['labelz'], j['sublabelz'])
    p.place(g)




# 11 - the end

p = d.addpage()

blue = rgb(0, 0, 255)
regular = font.open('src/Montserrat-Regular.ttf')
bold = font.open('src/Montserrat-Bold.ttf')
head = strike(bold).size(28, 100).color(black)
link = strike(regular).size(28, 100).color(blue)

p.place(text(
    head.paragraph('Xxyxyz'),
    head.paragraph('Flat'),
    link.paragraph('http://xxyxyz.org/flat'))).position(10, 10)




view(d.pdf())



