from flat import font, text, strike, document, view
import codecs

def layout(author, title, paragraphs):
    fonte = font.open('/home/svilayphiou/fonts/OSP/EnVogue/EnVogue/EnVogueBold.otf')
    body = strike(fonte).size(12, 16)
    headline = strike(fonte).size(25, 16)
    pieces = [
        body.paragraph(author),
        headline.paragraph(title),
        body.paragraph('')]
    pieces.extend(body.paragraph(p) for p in paragraphs)
    doc = document(148, 210, 'mm')
    page = doc.addpage()
    block = page.place(text(pieces))
    while block.frame(18, 21, 114, 167).overflow():
        page = doc.addpage()
        block = page.chain(block)
    return doc

fichier = codecs.open("drawing-curved.md", "r", "utf-8")
lignes_brutes = fichier.readlines()
fichier.close()

lignes = []

for l in lignes_brutes:
    print l
    lignes.append(l.replace("\n", "").replace("/"," /".replace(";", " ;")))

doc = layout('Pierre Huyghebaert', 'Drawing Curved', lignes)

doc.pdf('layout.pdf')
