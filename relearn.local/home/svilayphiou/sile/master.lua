local book = SILE.require("classes/book")
local bringhurst = book { id = "bringhurst" }

bringhurst:defineMaster({
        id = "right", firstContentFrame = "topcol",
        frames = {
            topcol = {
            top = "2cm",
            left = "1cm",
            right = "13cm",
            height="5cm",
            --width = "30%pw",
            next="leftcol"
            },
            leftcol = {
            top = "top(topcol) + height(topcol)",
            left = "1cm",
            right = "10cm",
            height="7cm",
            --width = "30%pw",
            next="rightcol"
            },
            rightcol = {
            top = "top(topcol) + height(topcol)",
            left = "right(leftcol) + 5mm",
            width = "3cm",
            height="7cm",
            --width = "30%pw",
            },
        }},
        {
        id = "left", firstContentFrame = "leftcol",
        frames = {
            leftcol = {
            top = "2cm",
            left = "1cm",
            height="10cm",
            width = "3cm",
            next="rightcol"
        },
            rightcol = {
            top = "10cm",
            left = "right(leftcol) + 5mm",
            height="10cm",
            right = "10cm",
            --width = "30%pw",
        },
    }
}
)
--book:loadPackage("twoside", { oddPageMaster = "right", evenPageMaster = "right" });
--bringhurst:mirrorMaster("right", "left")

return bringhurst
