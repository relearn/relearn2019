#!/bin/sh

import RPi.GPIO as GPIO
from time import sleep

NUM_SOLENOIDS = 2
NUM_STEPS = 16

ON_PERIOD = 0.05
SLEEP = 0.1
pins = [24,23]
pattern = [
[1,0,0,0,1,1,1,0,1,0,1,1,1,0,0,0],
[0,1,0,1,0,1,1,1,0,0,0,1,1,1,0,1],
[1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0],
[0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1]
]


def setup():
    GPIO.setmode(GPIO.BCM)
    for i in pins:
        GPIO.setup(i, GPIO.OUT)
        GPIO.output(i, GPIO.HIGH)
        sleep(ON_PERIOD)
        GPIO.output(i, GPIO.LOW)
        sleep(0.4)


def activate(vals):
    for i in range(NUM_SOLENOIDS):
        if vals[i] == 1:
            GPIO.output(pins[i], GPIO.HIGH)
        else:
            GPIO.output(pins[i], GPIO.LOW)
    sleep(ON_PERIOD)
    for i in range(NUM_SOLENOIDS):
        GPIO.output(pins[i], GPIO.LOW)


setup()

while(True):
    for i in range(NUM_STEPS):
        vals = []
        for j in range(NUM_SOLENOIDS):
            vals += [pattern[j][i]]
        activate(vals)
        sleep(SLEEP)

GPIO.cleanup()
